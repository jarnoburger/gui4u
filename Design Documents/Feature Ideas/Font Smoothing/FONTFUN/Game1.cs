using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace FONTFUN
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont font;
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 720;
            graphics.ApplyChanges();
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            font = Content.Load<SpriteFont>("texture");
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        private void DrawString(string text,Vector2 position,Color colr,float scale,Color? border,int BorderSize)
        {
            if (border != null)
            {
                spriteBatch.DrawString(font, text, position - new Vector2(BorderSize, 0), (Color)border, 0, Vector2.Zero, scale, SpriteEffects.None, 1f);
                spriteBatch.DrawString(font, text, position - new Vector2(0, BorderSize), (Color)border, 0, Vector2.Zero, scale, SpriteEffects.None, 1f);
                spriteBatch.DrawString(font, text, position + new Vector2(BorderSize, 0), (Color)border, 0, Vector2.Zero, scale, SpriteEffects.None, 1f);
                spriteBatch.DrawString(font, text, position + new Vector2(0, BorderSize), (Color)border, 0, Vector2.Zero, scale, SpriteEffects.None, 1f);

            }
            spriteBatch.DrawString(font, text, position, colr, 0, Vector2.Zero, scale, SpriteEffects.None, 1f);


        }
        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            spriteBatch.Begin();

            DrawString("Hello There", new Vector2(50, 50), Color.White, 0.5f, null,0);

            DrawString("epic win", new Vector2(150,250), Color.White, 0.25f, Color.DarkBlue,3);
            DrawString("BIG", new Vector2(50, 450), Color.White, 1f, null,0);
            DrawString("Small", new Vector2(450, 650), Color.White, 0.1f, null,0);
            spriteBatch.End();
            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
    }
}
