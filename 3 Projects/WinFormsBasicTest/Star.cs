﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using WinFormAdapter;

namespace WinFormsBasicTest
{
    public class Star : IDisposable
    {
        #region Declare

        private Bitmap _bitmap;
        private Graphics _graphics;
        private readonly Game _game;

        #endregion Declare

        #region Initialize

        public Star(Game game)
        {
            _game = game;
        }

        #endregion Initialize

        #region Monogame

        public void LoadContent()
        {
            var path = Path.GetDirectoryName(Application.ExecutablePath);
            if (path == null) throw new Exception("Could not get executable path");
            path = Path.Combine(path, "Content");
            path = Path.Combine(path, "Textures");
            path = Path.Combine(path, "galaxy.jpg");
            _bitmap = new Bitmap(path);
        }

        public void Update()
        {
            _graphics = _game.CurrentGraphics;
        }

        public void Draw()
        {
            _graphics = _game.CurrentGraphics;
            _graphics.DrawImage(_bitmap, 0, 0);
        }

        #endregion Monogame

        #region Implementation of IDisposable

        // Flag: Has Dispose already been called?
        private bool _disposed = false;

        // Public implementation of Dispose pattern callable by consumers.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern.
        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                // Free any other managed objects here.
                //
                _bitmap.Dispose();
            }

            // Free any unmanaged objects here.
            //
            _disposed = true;
        }

        #endregion Implementation of IDisposable
    }
}