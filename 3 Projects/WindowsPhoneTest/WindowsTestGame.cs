namespace WindowsPhoneTest
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;
    using Microsoft.Xna.Framework.Input;

    using GameTime = GUI4UFramework.Management.GameTime;

    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class WindowsTestGame : Game
    {
        private readonly GraphicsDeviceManager graphics;

        private readonly ContentManager contentManager;

        private SpriteBatch spriteBatch;

        private WindowsTestScript windowsTestScript;

        private Star star;

        private Camera camera;

        private GameTime currentTime;

        public WindowsTestGame()
        {
            this.graphics = new GraphicsDeviceManager(this);
            this.graphics.PreparingDeviceSettings += this.OnPreparingDeviceSettings;

            this.contentManager = new ContentManager(this.Services, "Content");
            this.Content.RootDirectory = "Content";
            this.Window.Title = "GUI4U";
            this.graphics.PreferredBackBufferWidth = 1280;
            this.graphics.PreferredBackBufferHeight = 800;
            this.graphics.PreferMultiSampling = true;

        }

        private void OnPreparingDeviceSettings(object sender, PreparingDeviceSettingsEventArgs e)
        {
            e.GraphicsDeviceInformation.PresentationParameters.RenderTargetUsage = RenderTargetUsage.PreserveContents;
        }

        public ContentManager ContentManager
        {
            get { return this.contentManager; }
        }

        public SpriteBatch SpriteBatch
        {
            get { return this.spriteBatch; }
        }

        public Star Star {get { return this.star; }}

        public Camera Camera { get { return this.camera; }}

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            this.currentTime = new GameTime();

            // Create a new SpriteBatch, which can be used to draw textures.
            this.spriteBatch = new SpriteBatch(this.GraphicsDevice);

            this.star = new Star(this);
            this.star.Initialize();

            this.camera = new Camera(this);
            this.camera.Initialize();

            this.windowsTestScript = new WindowsTestScript(this);
            this.windowsTestScript.Initialize();

            base.Initialize();

#if WINDOWS_PHONE_APP == false
            this.IsMouseVisible = true;
#endif
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            this.windowsTestScript.LoadContent();

            this.star.LoadContent(this);
            this.camera.LoadContent(this);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            this.star.UnloadContent();
            this.camera.UnloadContent();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
            {
                this.Exit();
            }

            var ks = Keyboard.GetState();
            if (ks.IsKeyDown(Keys.Escape))
            {
                this.Exit();
            }

            this.currentTime.Update(gameTime.TotalGameTime);

            this.star.Update(this.currentTime);
            this.camera.Update(this.currentTime);
            this.windowsTestScript.Update(this.currentTime);
            
            base.Update(gameTime);
        }
        
        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(Microsoft.Xna.Framework.GameTime gameTime)
        {
            this.GraphicsDevice.Clear(Color.SlateGray);

            this.currentTime.Update(gameTime.TotalGameTime);

            this.star.Draw(this.currentTime);
            this.camera.Draw(this.currentTime);
            this.windowsTestScript.Draw(this.currentTime);
            
            base.Draw(gameTime);
        }
    }
}
