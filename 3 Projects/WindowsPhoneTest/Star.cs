﻿namespace WindowsPhoneTest
{
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;

    using GameTime = GUI4UFramework.Management.GameTime;

    public class Star
    {
        #region Declare

        private Model _model;
        private Matrix _localMatrix;
        private readonly Vector3 _position;
        private readonly Vector3 _scale;
        private Vector3 _rotationDegree;
        private readonly WindowsTestGame _game;
        #endregion

        #region Initialize
        public Star(WindowsTestGame game)
        {
            this._game = game;
            this._position = new Vector3(0,0,0);
            this._rotationDegree = new Vector3(0,0,0);
            this._scale = new Vector3(100,100,100);
        }
        #endregion

        #region Monogame
        public void Initialize()
        {
            
        }

        public void LoadContent(Game game)
        {
            this._model = game.Content.Load<Model>("Objects\\star");
        }

        public void UnloadContent()
        {
        }

        public void Update(GameTime gameTime)
        {
            // The time since Update was called last.
            var elapsed = (float)gameTime.TotalGameTime.TotalSeconds * 10;

            this._rotationDegree.X = elapsed %360;
            this._rotationDegree.Y = elapsed %360;
            this._rotationDegree.Z = elapsed %360;
            CalculateLocalMatrix(this._position, this._rotationDegree, this._scale,out this._localMatrix);
            //_localMatrix = Matrix.Identity;
        }

        public void Draw(GameTime gameTime)
        {
            // Copy any parent transforms.
            var transforms = new Matrix[this._model.Bones.Count];
            this._model.CopyAbsoluteBoneTransformsTo(transforms);

            // Draw the model. A model can have multiple meshes, so loop.
            foreach (var mesh in this._model.Meshes)
            {

                // This is where the mesh orientation is set, as well as our camera and projection.
                foreach (var effect in mesh.Effects)
                {
                    var basicEffect = effect as BasicEffect;
                    if (basicEffect == null) continue;
                    basicEffect.EnableDefaultLighting();
                    basicEffect.PreferPerPixelLighting = true;
                    basicEffect.Projection = this._game.Camera.ProjectionMatrix;
                    basicEffect.View = this._game.Camera.ViewMatrix;
                    basicEffect.World = transforms[mesh.ParentBone.Index] * this._localMatrix;
                }
                // Draw the mesh, using the effects set above.
                mesh.Draw();
            }
        }
        #endregion

        public static void CalculateLocalMatrix(Vector3 position, Vector3 rotation, Vector3 scale, out Matrix output)
        {
            // convert rotation to rad
            var xaxis = MathHelper.ToRadians(rotation.X);
            var yaxis = MathHelper.ToRadians(rotation.Y);
            var zaxis = MathHelper.ToRadians(rotation.Z);

            // create the scale,rot,translate matrix
            output = Matrix.CreateScale(scale.X, scale.Y, scale.Z) *
                     Matrix.CreateFromYawPitchRoll(yaxis, xaxis, zaxis) *
                     Matrix.CreateTranslation(position.X, position.Y, position.Z);
        }
    }
}
