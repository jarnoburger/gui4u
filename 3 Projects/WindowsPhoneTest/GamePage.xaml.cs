﻿// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace WindowsPhoneTest
{
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;

    using MonoGame.Framework;

    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class GamePage : SwapChainBackgroundPanel
    {
        public GamePage()
        {
            this.InitializeComponent();
        }

        public GamePage(string launchArguments)
        {
            XamlGame<WindowsTestGame>.Create(launchArguments, Window.Current.CoreWindow, this);
        }
    }
}
