﻿namespace WindowsPhoneTest
{
    using Microsoft.Xna.Framework;

    using GameTime = GUI4UFramework.Management.GameTime;

    public class Camera
    {
        #region Declare
        private readonly Vector3 _position;
        private readonly Vector3 _target;
        private readonly float _fieldOfView;
        private readonly float _nearClipping;
        private readonly float _farClipping;
        private readonly WindowsTestGame _game;
        private Matrix _projectionMatrix;
        private Matrix _viewMatrix;
        #endregion

        #region Initialize
        public Camera(WindowsTestGame game)
        {
            this._game = game;
            this._position = new Vector3(100,50,9000);
            this._target = new Vector3();
            this._fieldOfView = 90;
            this._nearClipping = 1.0f;
            this._farClipping = 10000f;
        }
        #endregion

        #region Monogame
        public void Initialize()
        {
            this._projectionMatrix = this.CreateProjectionMatrix();
            this._viewMatrix = this.CreateViewMatrix();
        }

        public void LoadContent(Game game)
        {
        }

        public void UnloadContent()
        {
        }

        public void Update(GameTime gameTime)
        {
            this._viewMatrix = this.CreateViewMatrix();
        }

        public void Draw(GameTime gameTime)
        {
        }
        #endregion

        #region Properties
        public Matrix ProjectionMatrix { get { return this._projectionMatrix; }}
        public Matrix ViewMatrix { get { return this._viewMatrix; }}
        #endregion

        #region Actions
        private Matrix CreateProjectionMatrix()
        {
            var near = this._nearClipping;
            var far =this._farClipping;
            var fovH = this._fieldOfView;
            var radHorizontal = MathHelper.ToRadians(fovH);
            //var aspectRatio = _aspectRatio;
            var aspectRatio = this._game.GraphicsDevice.DisplayMode.AspectRatio;
            var radVertical = radHorizontal / aspectRatio;

            var projectionMatrix = Matrix.CreatePerspectiveFieldOfView(radVertical * 0.5f, aspectRatio, near, far);
            return projectionMatrix;
        }

        private Matrix CreateViewMatrix()
        {
            var up = new Vector3(0, 1, 0);
            var  viewMatrix = Matrix.CreateLookAt(this._position, this._target, up); // combine it to create a view matrix (where the cam looks at)
            return viewMatrix;
        }
        #endregion
    }
}
