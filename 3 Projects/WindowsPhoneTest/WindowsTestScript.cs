﻿namespace WindowsPhoneTest
{
    using System;

    using GUI4UControls.Containers;
    using GUI4UControls.Dialogs;
    using GUI4UControls.Tests;
    using GUI4UControls.Utility;

    using GUI4UFramework;
    using GUI4UFramework.Graphics;
    using GUI4UFramework.Management;
    using GUI4UFramework.Structural;

    using GUI4UWindows;

    using GUI4UWindowsPhoneAdapter;

    /// <summary>
    /// The script that will test all controls using a 3D-engine.
    /// </summary>
    public class WindowsTestScript 
    {
        /// <summary>
        /// The game the MAIN logic.
        /// </summary>
        private readonly WindowsTestGame game;

        /// <summary>
        /// The manager of the drawn nodes (controls).
        /// </summary>
        private NodeManager manager;

        /// <summary>
        /// The window in the 3d that shows the User Interface stuff.
        /// </summary>
        private Window window;

        /// <summary>
        /// A random number generator.
        /// </summary>
        private Random random;

        /// <summary>
        /// Initializes a new instance of the <see cref="WindowsTestScript"/> class.
        /// </summary>
        /// <param name="game">The game.</param>
        public WindowsTestScript(WindowsTestGame game)
        {
            this.game = game;
            UniqueNameCreator.CreateInstance();
        }

        /// <summary>
        /// Initializes this script.
        /// </summary>
        public void Initialize()
        {
            // create the image manager
            InputManager inputManager = new WindowsInputManager();

            // create the compositor (the drawer of stuff)
            var compositor = new WindowsImageCompositor(this.game);

            // create the node manager (the boss of the GUI-node structure)
            this.manager = new NodeManager(compositor, inputManager);

            // create the window
            var tempTheme = new Theme();
            this.window = new Window("WindowsTestScript")
            {
                Title = "Livecoding.tv",
                Config =
                {
                    PositionX = tempTheme.ControlLargeSpacing,
                    PositionY = tempTheme.ControlLargeSpacing,
                    Width = (tempTheme.ControlLargeSpacing) * 2 + (tempTheme.ControlWidth * 9),
                    Height = 700
                }
            };
            this.window.CreateDefaultMenu();                                  // ****
            this.manager.AddControl(this.window);

            // create stuff in the window
            var menuBuilder = new MenuBuilder(this.manager, this.window);
            menuBuilder.AddWindow(new TestWindowButton("Buttons"));           // ****
            menuBuilder.AddWindow(new TestWindowCheckBoxes("Check-boxes"));   // ****
            menuBuilder.AddWindow(new TestWindowColors("Colors"));            // ****
            menuBuilder.AddWindow(new TestWindowComboBox("Combo-box"));
            menuBuilder.AddWindow(new TestWindowContainers("Containers"));    // ****
            menuBuilder.AddWindow(new TestWindowImages("Images"));            // ****
            menuBuilder.AddWindow(new TestWindowListBox("List-box"));         // ****
            menuBuilder.AddWindow(new TestWindowProgressBar("Progress-bar")); // ****
            menuBuilder.AddWindow(new TestWindowScrollBar("Scrollbar"));      // ****
            menuBuilder.AddWindow(new TestWindowText("Text"));                // ****
            menuBuilder.AddWindow(new TestWindowTreeView("TreeView"));        // ****
            menuBuilder.AddWindow(new DialogError("Error dialog"));           // ****
            menuBuilder.AddWindow(new DialogMessage("Message dialog"));       // ****
            menuBuilder.AddWindow(new DialogOkCancel("OK Cancel dialog"));    // ****
            menuBuilder.AddWindow(new DialogRGBA("RGBA dialog"));             // ****
            menuBuilder.AddWindow(new DialogWarning("Warning dialog"));       // ****
            menuBuilder.AddWindow(new DialogYesNo("YesNo dialog"));           // ****
            menuBuilder.AddWindow(new TestWindowLog("Log"));                  // ****
            menuBuilder.CreateButtonsInControl(this.window);

            this.CreateToolTips();
            this.CreateMouseCursor();

            this.manager.Initialize();
            this.manager.DebugMe();
        }

        /// <summary>
        /// Loads the content. Loads all the stuff that needs to be shown by you !
        /// </summary>
        public void LoadContent()
        {
        }

        /// <summary>
        /// Unloads the content that is been made by you.
        /// </summary>
        public void UnloadContent()
        {
            this.manager.UnloadContent();
        }

        /// <summary>
        /// Updates the content made by you.
        /// Used to move the data in Config into a visual 'State'.
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        public void Update(GameTime gameTime)
        {
            this.manager.Update(gameTime);
        }
        
        /// <summary>
        /// Draws the stuff you made. Uses the data in 'State' for that.
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        public void Draw(GameTime gameTime)
        {
            this.manager.Draw(gameTime);
        }

        /// <summary>
        /// Creates the tool tips for all the buttons.
        /// </summary>
        private void CreateToolTips()
        {
            this.random = new Random((int)DateTime.Now.Ticks);
            var counter = 0;
            foreach (var child in this.window.Children)
            {
                // get a control on the form
                var control = child as Control;
                if (control == null) continue;

               // set the random text to the control
                control.TooltipText = Utility.CreateRandomText(this.random, 10);
                //System.Diagnostics.Debug.WriteLine(control.Name + " Tool-tip-text = " + control.TooltipText);
            
                // create the tool-tip itself
                var tooltip = new Tooltip("Tool-tip-" + counter);
                control.AddControl(tooltip);
                counter++;
            }
        }

        /// <summary>
        /// Creates the mouse cursor.
        /// </summary>
        private void CreateMouseCursor()
        {
            var mouse = new MouseCursor("MyCursor");
            this.manager.AddForegroundControl(mouse);
        }
    }
}
