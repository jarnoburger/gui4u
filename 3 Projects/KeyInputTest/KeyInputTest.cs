﻿using System;
using System.Threading;
using System.Windows.Forms;
using Gui4UFramework.Management;
using GUI4UWindows.Keyboard;

namespace keyInputTester
{
    public partial class KeyInputTest : Form
    {
        #region Declare

        private string _eventMessage;
        private readonly DateTime _timeStarted;
        private readonly GameTime _gameTime;
        private readonly KeyboardReader _keyboardReader;

        #endregion Declare

        #region Initialize

        public KeyInputTest()
        {
            InitializeComponent();

            this._keyboardReader = new KeyboardReader();

            _timeStarted = DateTime.Now;

            _gameTime = new GameTime();
        }

        #endregion Initialize

        #region Message pump

        private void DoTimeCheck()
        {
            var time = DateTime.Now - _timeStarted;
            _gameTime.Update(time);
            this._keyboardReader.UpdateKeyboardInput(_gameTime);
            Thread.Sleep(10);
            CheckForKeyChanges();
            UpdateLabels();
        }

        #endregion Message pump

        private void CheckForKeyChanges()
        {
            var flags = this._keyboardReader.KeySwitches;
            if (flags.Changed == false) return;

            if (flags.ChangeText) _eventMessage = "Event change text";
            if (flags.SelectAll) _eventMessage = "Event select all";
            if (flags.EnterPressed) _eventMessage = "Event enter pressed";
            if (flags.DoCursorLeft) _eventMessage = "Event go cursor left";
            if (flags.DoCursorRight) _eventMessage = "Event go cursor right";
            if (flags.InsertText) _eventMessage = "Event insert text " + flags.TextToInsert;
            if (flags.Backspace) _eventMessage = "Event backspace";
            if (flags.KeyDelete) _eventMessage = "Event key delete";

            flags.Reset();
        }

        private void UpdateLabels()
        {
            Text = _eventMessage;
            label1.Text = string.Format("Last pressed keys : {0}", this._keyboardReader.LastPressedKeys);
            label2.Text = string.Format("Fast repeat keys : {0}", this._keyboardReader.FastRepeatKeys);
            label3.Text = string.Format("ConfigText : {0}", this._keyboardReader.Text);
            label4.Text = string.Format("MousePressed keys : {0}", this._keyboardReader.PressedKeys);
        }
    }
}