﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GameTime = Gui4UFramework.Management.GameTime;

namespace keyInputTester
{
    public class Star
    {
        #region Declare

        private Model _model;
        private Matrix _localMatrix;
        private readonly Vector3 _position;
        private readonly Vector3 _scale;
        private Vector3 _rotationDegree;
        private readonly WindowsTestGame _game;

        #endregion Declare

        #region Initialize

        public Star(WindowsTestGame game)
        {
            _game = game;
            _position = new Vector3(0, 0, 0);
            _rotationDegree = new Vector3(0, 0, 0);
            _scale = new Vector3(10, 10, 10);
        }

        #endregion Initialize

        #region Monogame

        public void Initialize()
        {
        }

        public void LoadContent(Game game)
        {
            _model = game.Content.Load<Model>("Objects\\star");
        }

        public void UnloadContent()
        {
        }

        public void Update(GameTime gameTime)
        {
            // The time since Update was called last.
            var elapsed = (float)gameTime.TotalGameTime.TotalSeconds / 100;

            _rotationDegree.X += elapsed % 35;
            _rotationDegree.Y -= elapsed % 25;
            _rotationDegree.Z += elapsed % 27;
            CalculateLocalMatrix(_position, _rotationDegree, _scale, out _localMatrix);
            //_localMatrix = Matrix.Identity;
        }

        public void Draw(GameTime gameTime)
        {
            // Copy any parent transforms.
            var transforms = new Matrix[_model.Bones.Count];
            _model.CopyAbsoluteBoneTransformsTo(transforms);

            // Draw the model. A model can have multiple meshes, so loop.
            foreach (var mesh in _model.Meshes)
            {
                // This is where the mesh orientation is set, as well as our camera and projection.
                foreach (var effect in mesh.Effects)
                {
                    var basicEffect = effect as BasicEffect;
                    if (basicEffect == null) continue;
                    basicEffect.EnableDefaultLighting();
                    basicEffect.PreferPerPixelLighting = true;

                    basicEffect.Projection = _game.Camera.ProjectionMatrix;
                    basicEffect.View = _game.Camera.ViewMatrix;
                    basicEffect.World = transforms[mesh.ParentBone.Index] * _localMatrix;
                }
                // Draw the mesh, using the effects set above.
                mesh.Draw();
            }
        }

        #endregion Monogame

        public static void CalculateLocalMatrix(Vector3 position, Vector3 rotation, Vector3 scale, out Matrix output)
        {
            // convert rotation to rad
            var xaxis = MathHelper.ToRadians(rotation.X);
            var yaxis = MathHelper.ToRadians(rotation.Y);
            var zaxis = MathHelper.ToRadians(rotation.Z);

            // create the scale,rot,translate matrix
            output = Matrix.CreateScale(scale.X, scale.Y, scale.Z) *
                     Matrix.CreateFromYawPitchRoll(yaxis, xaxis, zaxis) *
                     Matrix.CreateTranslation(position.X, position.Y, position.Z);
        }
    }
}