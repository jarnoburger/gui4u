﻿using System;
using Gui4UFramework.Layout.Enums;
using Gui4UFramework.Management;
using Gui4UFramework.Structural;
using GUI4UControls.Containers;
using GUI4UControls.Text;
using GUI4UWindows;
using GUI4UWindows.Keyboard;

namespace keyInputTester
{
    public class WindowsTestScript
    {
        #region Declare

        private readonly WindowsTestGame _game;
        private NodeManager _manager;
        private Window _window;
        private Label[] _labels;
        private const int LabelCount = 7;
        private KeyboardReader _keyboardReader;
        private string _eventMessage;

        #endregion Declare

        #region Initialize

        public WindowsTestScript(WindowsTestGame game)
        {
            _game = game;
        }

        #endregion Initialize

        #region Monogame

        public void Initialize()
        {
            InputManager inputManager = new WindowsInputManager();

            var compositor = new WindowsImageCompositor(_game);

            _manager = new NodeManager(compositor, inputManager);

            _window = new Window("WindowsTestScript")
            {
                Config = { PositionX = 10, PositionY = 10 }
            };
            _window.Config.Width = 520;
            _window.Config.Height = 735;
            _manager.AddControl(_window);

            _labels = new Label[LabelCount];
            for (var i = 0; i < LabelCount; i++)
            {
                var name = String.Format("Label {0}", i);
                const int positionX = 10;
                var positionY = 10 + i * 40;
                var label = new Label(name)
                {
                    Config =
                    {
                        PositionX = positionX,
                        PositionY = positionY
                    },
                    ConfigHorizontalAlignment = HorizontalAlignment.Left,
                    ConfigVerticalAlignment = VerticalAlignment.Center
                };
                _labels[i] = label;
                _window.AddControl(label);
            }

            _window.Config.Height = 10 + LabelCount * 40;

            _manager.Initialize();
            _manager.DebugMe();
        }

        public void LoadContent()
        {
            _eventMessage = string.Empty;

            this._keyboardReader = new KeyboardReader();
        }

        public void UnloadContent()
        {
            _manager.UnloadContent();
        }

        public void Update(GameTime gameTime)
        {
            this._keyboardReader.UpdateKeyboardInput(gameTime);

            CheckForKeyChanges();
            _manager.Update(gameTime);

            UpdateLabels();
        }

        public void Draw(GameTime gameTime)
        {
            _manager.Draw(gameTime);
        }

        #endregion Monogame

        private void CheckForKeyChanges()
        {
            var flags = this._keyboardReader.KeySwitches;
            if (flags.Changed == false) return;

            if (flags.ChangeText) _eventMessage = "Event change text";
            if (flags.SelectAll) _eventMessage = "Event select all";
            if (flags.EnterPressed) _eventMessage = "Event enter pressed";
            if (flags.DoCursorLeft) _eventMessage = "Event go cursor left";
            if (flags.DoCursorRight) _eventMessage = "Event go cursor right";
            if (flags.InsertText) _eventMessage = "Event insert text " + flags.TextToInsert;
            if (flags.Backspace) _eventMessage = "Event backspace";
            if (flags.KeyDelete) _eventMessage = "Event key delete";

            flags.Reset();
        }

        private void UpdateLabels()
        {
            _labels[0].ConfigText = string.Format("Message : {0}", _eventMessage);
            _labels[1].ConfigText = string.Format("Last pressed keys : {0}", this._keyboardReader.LastPressedKeys);
            _labels[2].ConfigText = string.Format("Fast repeat keys : {0}", this._keyboardReader.FastRepeatKeys);
            _labels[3].ConfigText = string.Format("ConfigText : {0}", this._keyboardReader.Text);
            _labels[4].ConfigText = string.Format("MousePressed keys : {0}", this._keyboardReader.PressedKeys);
        }
    }
}