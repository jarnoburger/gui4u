using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using GameTime = Gui4UFramework.Management.GameTime;

namespace keyInputTester
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class WindowsTestGame : Game
    {
        #region Declare

        private readonly GraphicsDeviceManager _graphics;
        private readonly ContentManager _contentManager;
        private SpriteBatch _spriteBatch;
        private WindowsTestScript _windowsTestScript;
        private Star _star;
        private Camera _camera;
        private GameTime _currentTime;

        #endregion Declare

        #region Initialize

        public WindowsTestGame()
        {
            _graphics = new GraphicsDeviceManager(this);
            _graphics.PreparingDeviceSettings += graphics_PreparingDeviceSettings;

            _contentManager = new ContentManager(Services, "Content");
            Content.RootDirectory = "Content";
            Window.Title = "GUI4U";
            _graphics.PreferredBackBufferWidth = 800;
            _graphics.PreferredBackBufferHeight = 800;
            _graphics.PreferMultiSampling = true;
        }

        private void graphics_PreparingDeviceSettings(object sender, PreparingDeviceSettingsEventArgs e)
        {
            e.GraphicsDeviceInformation.PresentationParameters.RenderTargetUsage = RenderTargetUsage.PreserveContents;
        }

        #endregion Initialize

        #region Public Properties

        public ContentManager ContentManager
        {
            get { return _contentManager; }
        }

        public SpriteBatch SpriteBatch
        {
            get { return _spriteBatch; }
        }

        public Star Star { get { return _star; } }
        public Camera Camera { get { return _camera; } }

        #endregion Public Properties

        #region Monogame

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            _currentTime = new GameTime();

            // TODO: Add your initialization logic here
            // Create a new SpriteBatch, which can be used to draw textures.
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            _star = new Star(this);
            _star.Initialize();

            _camera = new Camera(this);
            _camera.Initialize();

            _windowsTestScript = new WindowsTestScript(this);
            _windowsTestScript.Initialize();

            base.Initialize();

            IsMouseVisible = true;
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            _windowsTestScript.LoadContent();

            _star.LoadContent(this);
            _camera.LoadContent(this);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            _star.UnloadContent();
            _camera.UnloadContent();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                Exit();

            var ks = Keyboard.GetState();
            if (ks.IsKeyDown(Keys.Escape))
                Exit();

            _currentTime.Update(gameTime.TotalGameTime);

            _star.Update(_currentTime);
            _camera.Update(_currentTime);
            _windowsTestScript.Update(_currentTime);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(Microsoft.Xna.Framework.GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.SlateGray);

            _currentTime.Update(gameTime.TotalGameTime);

            _star.Draw(_currentTime);
            _camera.Draw(_currentTime);
            _windowsTestScript.Draw(_currentTime);

            base.Draw(gameTime);
        }

        #endregion Monogame
    }
}