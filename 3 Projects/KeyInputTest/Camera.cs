﻿using Microsoft.Xna.Framework;
using GameTime = Gui4UFramework.Management.GameTime;

namespace keyInputTester
{
    public class Camera
    {
        #region Declare

        private readonly Vector3 _position;
        private readonly Vector3 _target;
        private readonly float _fieldOfView;
        private readonly float _nearClipping;
        private readonly float _farClipping;
        private readonly WindowsTestGame _game;
        private Matrix _projectionMatrix;
        private Matrix _viewMatrix;

        #endregion Declare

        #region Initialize

        public Camera(WindowsTestGame game)
        {
            _game = game;
            _position = new Vector3(100, 50, 9000);
            _target = new Vector3();
            _fieldOfView = 90;
            _nearClipping = 1.0f;
            _farClipping = 10000f;
        }

        #endregion Initialize

        #region Monogame

        public void Initialize()
        {
            _projectionMatrix = CreateProjectionMatrix();
            _viewMatrix = CreateViewMatrix();
        }

        public void LoadContent(Game game)
        {
        }

        public void UnloadContent()
        {
        }

        public void Update(GameTime gameTime)
        {
            _viewMatrix = CreateViewMatrix();
        }

        public void Draw(GameTime gameTime)
        {
        }

        #endregion Monogame

        #region Properties

        public Matrix ProjectionMatrix { get { return _projectionMatrix; } }
        public Matrix ViewMatrix { get { return _viewMatrix; } }

        #endregion Properties

        #region Actions

        private Matrix CreateProjectionMatrix()
        {
            var near = _nearClipping;
            var far = _farClipping;
            var fovH = _fieldOfView;
            var radHorizontal = MathHelper.ToRadians(fovH);
            //var aspectRatio = _aspectRatio;
            var aspectRatio = _game.GraphicsDevice.DisplayMode.AspectRatio;
            var radVertical = radHorizontal / aspectRatio;

            var projectionMatrix = Matrix.CreatePerspectiveFieldOfView(radVertical * 0.5f, aspectRatio, near, far);
            return projectionMatrix;
        }

        private Matrix CreateViewMatrix()
        {
            var up = new Vector3(0, 1, 0);
            var viewMatrix = Matrix.CreateLookAt(_position, _target, up); // combine it to create a view matrix (where the cam looks at)
            return viewMatrix;
        }

        #endregion Actions
    }
}