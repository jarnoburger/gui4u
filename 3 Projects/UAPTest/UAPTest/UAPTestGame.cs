﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using GameTime = Gui4UFramework.Management.GameTime;

namespace UAPTest
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class UapTestGame : Game
    {
        private readonly GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;
        private Star _star;
        private Camera _camera;
        private GameTime _currentTime;
        private UAPTestGameScript _uapTestGameScript;

        public UapTestGame()
        {
            _graphics = new GraphicsDeviceManager(this);
            _graphics.PreparingDeviceSettings += OnPreparingDeviceSettings;

            Content.RootDirectory = "Content";

            Window.Title = "GUI4U";

            this._graphics.PreferredBackBufferWidth = 1280;
            this._graphics.PreferredBackBufferHeight = 800;
            this._graphics.PreferMultiSampling = true;
        }

        private void OnPreparingDeviceSettings(object sender, PreparingDeviceSettingsEventArgs e)
        {
            e.GraphicsDeviceInformation.PresentationParameters.RenderTargetUsage = RenderTargetUsage.PreserveContents;
        }

        public Star Star { get { return this._star; } }

        public Camera Camera { get { return this._camera; } }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            this._currentTime = new GameTime();

            // Create a new SpriteBatch, which can be used to draw textures.
            this._spriteBatch = new SpriteBatch(GraphicsDevice);

            this._star = new Star(this);
            this._star.Initialize();

            this._camera = new Camera(this);
            this._camera.Initialize();

            this._uapTestGameScript = new UAPTestGameScript(this);
            this._uapTestGameScript.Initialize();

            base.Initialize();

#if WINDOWS_PHONE_APP == false
            this.IsMouseVisible = true;
#endif
    }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            this._uapTestGameScript.LoadContent();

            this._star.LoadContent(this);
            this._camera.LoadContent(this);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            this._star.UnloadContent();
            this._camera.UnloadContent();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
            {
                this.Exit();
            }

            var ks = Keyboard.GetState();
            if (ks.IsKeyDown(Keys.Escape))
            {
                this.Exit();
            }

            this._currentTime.Update(gameTime.TotalGameTime);

            this._star.Update(this._currentTime);
            this._camera.Update(this._currentTime);
            this._uapTestGameScript.Update(this._currentTime);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(Microsoft.Xna.Framework.GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.SlateGray);

            this._currentTime.Update(gameTime.TotalGameTime);

            this._star.Draw(this._currentTime);
            this._camera.Draw(this._currentTime);
            this._uapTestGameScript.Draw(this._currentTime);

            base.Draw(gameTime);
        }
    }
}