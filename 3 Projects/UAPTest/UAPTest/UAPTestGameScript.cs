﻿using System;
using Gui4UFramework;
using Gui4UFramework.Graphics;
using Gui4UFramework.Management;
using Gui4UFramework.Structural;
using GUI4UControls.Containers;
using GUI4UControls.Dialogs;
using GUI4UControls.Tests;
using GUI4UControls.Utility;
using UniversalAdapter;

namespace UAPTest
{
    /// <summary>
    /// The script that will test all controls using a 3D-engine.
    /// </summary>
    public class UAPTestGameScript
    {
        /// <summary>
        /// The game the MAIN logic.
        /// </summary>
        private readonly UapTestGame _game;

        /// <summary>
        /// The manager of the drawn nodes (controls).
        /// </summary>
        private NodeManager _manager;

        /// <summary>
        /// The window in the 3d that shows the User Interface stuff.
        /// </summary>
        private Window _window;

        /// <summary>
        /// A random number generator.
        /// </summary>
        private Random _random;

        /// <summary>
        /// Initializes a new instance of the <see cref="UAPTestGameScript"/> class.
        /// </summary>
        /// <param name="game">The game.</param>
        public UAPTestGameScript(UapTestGame game)
        {
            this._game = game;
            UniqueNameCreator.CreateInstance();
        }

        /// <summary>
        /// Initializes this script.
        /// </summary>
        public void Initialize()
        {
            // create the image manager
            InputManager inputManager = new UAPInputManager();

            // create the compositor (the drawer of stuff)
            var compositor = new UAPImageCompositor(this._game);

            // create the node manager (the boss of the GUI-node structure)
            this._manager = new NodeManager(compositor, inputManager);

            // create the window
            var tempTheme = new Theme();
            this._window = new Window("UAPTestGameScript")
            {
                Title = "Livecoding.tv",
                Config =
                {
                    PositionX = 0,
                    PositionY = 0 + tempTheme.ControlHeight,
                }
            };
            this._window.Config.Width = (tempTheme.ControlLargeSpacing) * 2 + (tempTheme.ControlWidth * 9);
            this._window.Config.Height = 700;
            this._window.CreateDefaultMenu();                                  // ****
            this._manager.AddControl(this._window);

            // create stuff in the window
            var menuBuilder = new MenuBuilder(this._manager, this._window);
            menuBuilder.AddWindow(new TestWindowButton("Buttons"));           // ****
            menuBuilder.AddWindow(new TestWindowCheckBoxes("Check-boxes"));   // ****
            menuBuilder.AddWindow(new TestWindowColors("Colors"));            // ****
            menuBuilder.AddWindow(new TestWindowComboBox("Combo-box"));
            menuBuilder.AddWindow(new TestWindowContainers("Containers"));    // ****
            menuBuilder.AddWindow(new TestWindowImages("Images"));            // ****
            menuBuilder.AddWindow(new TestWindowListBox("List-box"));         // ****
            menuBuilder.AddWindow(new TestWindowProgressBar("Progress-bar")); // ****
            menuBuilder.AddWindow(new TestWindowScrollBar("Scrollbar"));      // ****
            menuBuilder.AddWindow(new TestWindowText("Text"));                // ****
            menuBuilder.AddWindow(new TestWindowTreeView("TreeView"));        // ****
            menuBuilder.AddWindow(new DialogError("Error dialog"));           // ****
            menuBuilder.AddWindow(new DialogMessage("Message dialog"));       // ****
            menuBuilder.AddWindow(new DialogOkCancel("OK Cancel dialog"));    // ****
            menuBuilder.AddWindow(new DialogRGBA("RGBA dialog"));             // ****
            menuBuilder.AddWindow(new DialogWarning("Warning dialog"));       // ****
            menuBuilder.AddWindow(new DialogYesNo("YesNo dialog"));           // ****
            menuBuilder.AddWindow(new TestWindowLog("Log"));                  // ****
            menuBuilder.AddWindow(new TestWindowVerticalSpacing("Ver spacer"));
            menuBuilder.AddWindow(new TestWindowHorizontalSpacing("Hor spacer"));
            menuBuilder.AddWindow(new TestWindowAlignmentHorizontal("Align horizontal"));
            menuBuilder.AddWindow(new TestWindowAlignmentVertical("Align vertical"));
            menuBuilder.AddWindow(new TestWindowAutoScale("Auto Scale"));
            menuBuilder.CreateButtonsInControl(this._window);

            this.CreateToolTips();
            this.CreateMouseCursor();

            this._manager.Initialize();
            this._manager.DebugMe();
        }

        /// <summary>
        /// Loads the content. Loads all the stuff that needs to be shown by you !
        /// </summary>
        public void LoadContent()
        {
        }

        /// <summary>
        /// Unloads the content that is been made by you.
        /// </summary>
        public void UnloadContent()
        {
            this._manager.UnloadContent();
        }

        /// <summary>
        /// Updates the content made by you.
        /// Used to move the data in Config into a visual 'State'.
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        public void Update(GameTime gameTime)
        {
            this._manager.Update(gameTime);
        }

        /// <summary>
        /// Draws the stuff you made. Uses the data in 'State' for that.
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        public void Draw(GameTime gameTime)
        {
            this._manager.Draw(gameTime);
        }

        /// <summary>
        /// Creates the tool tips for all the buttons.
        /// </summary>
        private void CreateToolTips()
        {
            this._random = new Random((int)DateTime.Now.Ticks);
            var counter = 0;
            foreach (var child in this._window.Children)
            {
                // get a control on the form
                var control = child as Control;
                if (control == null) continue;

                // set the random text to the control
                control.TooltipText = Utility.CreateRandomText(this._random, 10);
                //System.Diagnostics.Debug.WriteLine(control.Name + " Tool-tip-text = " + control.TooltipText);

                // create the tool-tip itself
                var tooltip = new Tooltip("Tool-tip-" + counter);
                control.AddControl(tooltip);
                counter++;
            }
        }

        /// <summary>
        /// Creates the mouse cursor.
        /// </summary>
        private void CreateMouseCursor()
        {
            var mouse = new MouseCursor("MyCursor");
            this._manager.AddForegroundControl(mouse);
        }
    }
}