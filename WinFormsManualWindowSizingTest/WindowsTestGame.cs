// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WindowsTestGame.cs" company="Jarno Burger">
//   see copyright.txt in the root of this project.
// </copyright>
// <summary>
//   This is the main type for your game
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Gui4UFramework.Management;
using WinFormAdapter;
using WinFormsBasicTest;

namespace WinFormsManualWindowSizingTest
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class WindowsTestGame : Game
    {
        /// <summary>
        /// The test script.
        /// </summary>
        private WindowsTestScript _windowsTestScript;

        /// <summary>
        /// A script that shows stars.
        /// </summary>
        private Star _star;

        /// <summary>
        /// Initializes a new instance of the <see cref="WindowsTestGame"/> class.
        /// </summary>
        public WindowsTestGame()
        {
            this.Width = 1280;
            this.Height = 800;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            this.Text = @"GUI4U";

            base.Initialize();

            this._windowsTestScript = new WindowsTestScript(this);
            this._windowsTestScript.Initialize();

            this._star = new Star(this);

            this.IsMouseVisible = true;
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            this._star.LoadContent();
            this._windowsTestScript.LoadContent();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            this._windowsTestScript.UnloadContent();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            this._star.Update();
            this._windowsTestScript.Update(this.GameTime);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            this._star.Draw();
            this._windowsTestScript.Draw(this.GameTime);

            base.Draw(gameTime);
        }
    }
}