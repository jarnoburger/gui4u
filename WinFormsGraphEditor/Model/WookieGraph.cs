﻿using System;

namespace WinFormsGraphEditor.Model
{
    /// <summary>
    /// Contains all the wookie-items, that are connected to each other
    /// </summary>
    public class WookieGraph
    {
        #region Declare
        private readonly Random _random;
        #endregion

        #region Initialize

        public WookieGraph()
        {
            _random = new Random();
            Items = new WookieItemCollection();

            for (var i = 0; i < 10; i++)
            { 
                CreateTestItem();
            }

        }
        #endregion

        private void CreateTestItem()
        {
            var item = new WookieItem
                                    {
                                        Name = "Wookie_" + GenerateToken(10),
                                        GuiLocationX = _random.Next(1000)/1000f,
                                        GuiLocationY = _random.Next(1000)/1000f
                                    };

            CreateTestProperties(item);

            Items.Add(item);
        }

        #region Properties
        public WookieItemCollection Items { get; set; }
        #endregion

        public void CreateTestProperties(WookieItem item)
        {
            var max = _random.Next(4) + 1;
            for (var i = 0; i < max; i++)
            {
                var t = _random.Next(3);
                if (t <= 0)
                {
                    var prop = new WookieFloat
                    {
                        Name = GenerateToken(10),
                        Value = _random.Next(1000)/1000f
                    };
                    item.Properties.Add(prop);
                }
                else if (t == 1)
                {
                    var prop = new WookieInt
                    {
                        Name = GenerateToken(10),
                        Value = _random.Next(1000)
                    };
                    item.Properties.Add(prop);
                }
                else if (t >= 2)
                {
                    var prop = new WookieString
                    {
                        Name = GenerateToken(10),
                        Text = GenerateToken(10)
                    };
                    item.Properties.Add(prop);
                }
            }

            
        }

        public string GenerateToken(Byte length)
        {
            var bytes = new byte[length];
            _random.NextBytes(bytes);
            return Convert.ToBase64String(bytes).Replace("=", "").Replace("+", "").Replace("/", "");
        }
    }
}
