﻿namespace WinFormsGraphEditor.Model
{
    public abstract class WookieProperty
    {
        public string Name { get; set; }

        public abstract string GetGuiText();
    }

    public class WookieFloat : WookieProperty
    {
        public float Value { get; set; }
        public override string GetGuiText()
        {
            return "F : " + Value;
        }
    }

    public class WookieString : WookieProperty
    {
        public string Text { get; set; }
        public override string GetGuiText()
        {
            return "T : " + Text;
        }
    }

    public class WookieInt : WookieProperty
    {
        public float Value { get; set; }
        public override string GetGuiText()
        {
            return "I : " + Value;
        }
    }
}