﻿namespace WinFormsGraphEditor.Model
{
    /// <summary>
    /// Contains wookie-properties that are connected to each other
    /// </summary>
    public class WookieItem
    {
        #region Declare
        #endregion

        #region Initialize
        public WookieItem()
        {
            Name = string.Empty;
            Properties = new WookieProperties();
        }
        #endregion

        #region Properties
        public string Name { get; set; }

        public WookieProperties Properties { get; set; }

        /// <summary>
        /// A relative x location between 0 and 1
        /// </summary>
        public float GuiLocationX { get; set; }

        /// <summary>
        /// A relative y location between 0 and 1
        /// </summary>
        public float GuiLocationY { get; set; }
        #endregion
    }
}