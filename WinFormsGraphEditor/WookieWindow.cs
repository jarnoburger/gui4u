﻿using GUI4UControls.Buttons;
using GUI4UControls.Containers;
using WinFormsGraphEditor.Model;

namespace WinFormsGraphEditor
{
    public class WookieWindow : Window
    {
        #region Declare
        private int _innerDistance = 10;
        #endregion

        #region Initialize
        public WookieWindow(string name) : base(name)
        {
            this.Config.Width = 100;
            this.Config.Height = 100;

        }
        #endregion

        public static WookieWindow Create(WookieItem item, GraphEditor game)
        {
            var wookieWindow = new WookieWindow(item.Name);
            wookieWindow.Config.PositionX = item.GuiLocationX * (float)(game.Width - wookieWindow.Config.Width);
            wookieWindow.Config.PositionY = item.GuiLocationY * (float)(game.Height - wookieWindow.Config.Height);
            wookieWindow.Title = item.Name;
            wookieWindow.Tag = item;

            return wookieWindow;
        }

        public void SyncProperties()
        {
            var wookieItem = Tag as WookieItem;
            if (wookieItem == null) return;
                    
            var wookieProperties = wookieItem.Properties;
            foreach (var wookieProperty in wookieProperties)
            {
                AddProperty(wookieProperty);
            }
        }

        public void AddProperty(WookieProperty wookieProperty)
        {
            var button = new Button(wookieProperty.Name)
            {
                Text = wookieProperty.GetGuiText(),
                Config =
                                        {
                                            Width = this.Config.Width - _innerDistance,
                                            Height = Theme.ControlHeight,
                                            PositionX = _innerDistance*0.5f,
                                            PositionY = _innerDistance*0.5f
                                        }
            };
            this.AddControl(button);
        }
    }
}