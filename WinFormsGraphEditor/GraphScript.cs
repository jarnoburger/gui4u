﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Windows.Forms;
using Gui4UFramework.Management;
using Gui4UFramework.Structural;
using GUI4UControls.Utility;
using WinFormAdapter;
using WinFormAdapter.Utility;
using WinFormsGraphEditor.Model;

namespace WinFormsGraphEditor
{
    public class GraphScript
    {
        #region Declare
        /// <summary>
        /// The game with the MAIN logic.
        /// </summary>
        private readonly GraphEditor _game;

        private WookieGraph _wookieGraph;

        /// <summary>
        /// The manager of the drawn nodes (controls).
        /// </summary>
        private NodeManager _manager;

        private DebugTree _debugTree;
        private DebugResources _debugResource;
        #endregion

        # region Initialize
        public GraphScript(GraphEditor game)
        {
            this._game = game;

            game.Width = 1280;
            game.Height = 800;
            
            // todo : set the location for the forms
        }
        #endregion

        public void Initialize()
        {
            this._wookieGraph = new WookieGraph();

            // create the image manager
            InputManager inputManager = new WindowsInputManager(this._game);

            // create the compositor
            var baseUrl = Path.GetDirectoryName(Application.ExecutablePath);
            if (baseUrl == null) throw new Exception("Could not retrieve application path");
            baseUrl = Path.Combine(baseUrl, "Content");
            var compositor = new FormsImageCompositor(this._game, baseUrl);

            // create the node manager (the boss of the GUI-node structure)
            this._manager = new NodeManager(compositor, inputManager);
         
            // create a mouse cursor
            var mouse = new MouseCursor("MyCursor");
            this._manager.AddForegroundControl(mouse);

            this._manager.Initialize();
            this._manager.DebugMe();

            this._debugTree = DebugTree.Show(this._manager.SceneNodes, this._manager.ImageCompositor);
            this._debugResource = DebugResources.Show(this._manager.ImageCompositor);

            Sync();
        }

        public void LoadContent()
        {
            
        }

        public void UnloadContent()
        {
            this._manager.UnloadContent();
        }

        public void Update(GameTime gameTime)
        {
            this._manager.Update(gameTime);
        }

        public void Draw(GameTime gameTime)
        {
            this._manager.Draw(gameTime);
        }

        [SuppressMessage("ReSharper", "UseNullPropagation")]
        [SuppressMessage("ReSharper", "UseNullPropagationWhenPossible")]
        public void Sync()
        {
            // add windows
            foreach (var item in _wookieGraph.Items)
            {
                var contains = false;
                foreach (var node in this._manager.SceneNodes.Children)
                {
                    var window = node as WookieWindow;

                    // ReSharper disable once UseNullPropagationWhenPossible
                    if (window == null) continue;

                    if (!window.Name.Equals(item.Name)) continue;

                    contains = true;
                    break;
                }

                if (contains == false)
                {
                    var wookieWindow = WookieWindow.Create(item, _game);
                    _manager.AddControl(wookieWindow);
                    wookieWindow.SyncProperties();
                }
            }
        }
    }
}
