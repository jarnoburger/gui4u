﻿using Gui4UFramework.Management;
using WinFormAdapter;

namespace WinFormsGraphEditor
{
    public partial class GraphEditor : Game
    {
        #region Declare
        private GraphScript _graphScript;
        #endregion

        #region Initialize
        public GraphEditor()
        {
            InitializeComponent();

            this.Width = 600;
            this.Height = 600;
        }
        #endregion

        protected override void Initialize()
        {
            base.Initialize();

            this._graphScript = new GraphScript(this);
            this._graphScript.Initialize();

            this.IsMouseVisible = true;
        }

        protected override void LoadContent()
        {
            base.LoadContent();
            this._graphScript.LoadContent();
        }

        protected override void UnloadContent()
        {
            base.UnloadContent();
            this._graphScript.UnloadContent();
        }

        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            this._graphScript.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

            this._graphScript.Draw(gameTime);
        }
    }
}
