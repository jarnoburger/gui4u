### What is this repository for? ###

* This lets you add user interfaces quickly to your monogame program.
* This is still in alpha state. But will soon be tested in a production enviroment.

### How to look at this project ? ###

* This project will try to support different renderers , monogame , system.drawing and later in the future the bohemiq rendererer. 
* From this , other people could make their own renderer for userinterfaces too.
* All usercontrols should be basic and simple. They just work in the most basic way.
* Everything shall be commented a lot. Documentation about how the code works should be primary.

Original project link : https://gui4u.codeplex.com/ and  https://simplegui.codeplex.com/

### Documentation here ###

https://simplegui.codeplex.com/documentation?referringTitle=Home
https://gui4u.codeplex.com/wikipage?title=Learn%20this.&version=4

### Todo list here ###

https://docs.google.com/spreadsheets/d/19Tffw4-dshOzbmIjBnmskXpHvx-cBT1S7TNWQY0iSkE/edit#gid=0

### Live coding channel here ###

https://www.livecoding.tv/jarnoburger/

### Working and tested builds ###

These builds are tested continuously :
* Monogame Windows AnyCPU
* Monogame WindowsPhone ARM
* WinForms AnyCPU

### How do I get set up? ###

* I am using Visual Studio 2013 , StyleCop , Resharper , Atlesian SourceTree and Monogame. 
* If you are using this code, then place my name in your Credit Roll !

### Contribution guidelines ###

If you want to contribute :
* try to add a usercontrol. Here is the list of controls that need to be supported : https://docs.google.com/spreadsheets/d/19Tffw4-dshOzbmIjBnmskXpHvx-cBT1S7TNWQY0iSkE/edit#gid=0

### Who do I talk to? ###

* I am the owner : Jarno Burger , please use the issue tracker , cause then i don't have a full email inbox.

### Screenshots ###

![ScreenShot-1.png](https://bitbucket.org/repo/LKRyRK/images/3463090417-ScreenShot-1.png)
![ScreenShot-2.png](https://bitbucket.org/repo/LKRyRK/images/1551510442-ScreenShot-2.png)
![ScreenShot-3.png](https://bitbucket.org/repo/LKRyRK/images/1928175374-ScreenShot-3.png)
![ScreenShot-4.png](https://bitbucket.org/repo/LKRyRK/images/3073666739-ScreenShot-4.png)
![ScreenShot-5.png](https://bitbucket.org/repo/LKRyRK/images/2037078689-ScreenShot-5.png)

### Updates ###

10 Apr 2015:

Did a lot of Code Styling.. :
- Microsoft StyleCop : Following them at highest quality now. 
- Code analysis in VS2013 : Following Extended Design Guidelines now.
- Resharper Code analysis : Following 80% of the guidelines now.

MonoGame-controls working visually for 80%
System.Drawing-controls working visually for 80%

30 Mar 2015 :
Created : MessageDialog , ErrorDialog , OkCancelDialog, Warning Dialog, YEsNo Dialog , MenuBar , LogControl. Also set the Code Analysys way higher and corrected 1500 little 'errors'. 

23 Mar 2015 :
A lot has changed. I am LiveCoding now.. https://www.livecoding.tv/jarnoburger/. And i made a google doc that is my todo-list. So you can see what i am doing. All in all i am working out to create all my needed controls first visually in monogame. Then i will add functionality like mouse/keyboard. And the i will make the system.drawing version alive.. After this i will work out the bohemiq 3d engine of a friend of mine. this is used for my dome projections.

3 Mar 2015 :
The forms.drawing version almost works, and the windows phone version also partly works. It will be time now to introduce features for every control and to introduce new ones. When all controls are added, and code is showing functionality , we will go to beta.

18 Feb 2015 :
Most of the code has been rewritten to work with the way i like it. I will now combine working out features for the monogame ui version. And making the forms.drawing version work.

03 Okt 2014 :

Today i made adjustments to make it work with the Microsoft Extended Design Guideline Rules 90% finished with it.

22 Sept 2014 :

Today i am cleaning up DPanel

introduced property  DPanel.Rectangle instead of X,Y,Width,Heigth 
introduced property DPanel.Config to contain configuration items for the panel
introduced property DPanel.State to contain state flags for the panel
moved alpha and borderSize to the DPanel.Theme 
AbosulteTransform-name has been changed to DrawPosition
removed some weird redundant stuff
add some extra comments on the right places
21 Sept 2014 :

cleanup even more typos.
textures were not disposed when they were recreated, did partly fixes for that.
16 to 21 Sept 2014 :

created 1 property for theming , in place of multiple
regionized , and removed redundant spacing a lot
adjusted namespace and project names
changed properties into methods , when they were actually methods
changed properties into auto-properties , when they were exactly the same as auto-properties
change the color theme for the test project, just for the looks of it
runs on monogame engine 3.2
listboxes are templated better
image box is shown
alpha layering is shown
added a simple 3d object in the background