// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CMYK.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Defines the CMYK type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Gui4UFramework.Colors
{
    /// <summary>Represents a color using the CMYK color-model.</summary>
    public sealed class Cmyk
    {
        /// <summary>The cyan part of the color.</summary>
        private double _cyan;

        /// <summary>The magenta part of the color.</summary>
        private double _magenta;

        /// <summary>The yellow part of the color.</summary>
        private double _yellow;

        /// <summary>The blackness of the color.</summary>
        private double _keyBlack;

        /// <summary>
        /// Initializes a new instance of the <see cref="Cmyk"/> class.
        /// </summary>
        public Cmyk()
        {
            this._cyan = 0;
            this._magenta = 0;
            this._yellow = 0;
            this._keyBlack = 0;
        }

        /// <summary>Gets or sets the cyan part of the color.</summary>
        /// <value>The cyan part of the color.</value>
        public double Cyan
        {
            get
            {
                return this._cyan;
            }

            set
            {
                this._cyan = value;
                this._cyan = this._cyan > 1 ? 1 : this._cyan < 0 ? 0 : this._cyan;
            }
        }

        /// <summary>Gets or sets the magenta part of the color.</summary>
        /// <value>The magenta part of the color.</value>
        public double Magenta
        {
            get
            {
                return this._magenta;
            }

            set
            {
                this._magenta = value;
                this._magenta = this._magenta > 1 ? 1 : this._magenta < 0 ? 0 : this._magenta;
            }
        }

        /// <summary>Gets or sets the yellow part of the color.</summary>
        /// <value>The yellow part of the color.</value>
        public double Yellow
        {
            get
            {
                return this._yellow;
            }

            set
            {
                this._yellow = value;
                this._yellow = this._yellow > 1 ? 1 : this._yellow < 0 ? 0 : this._yellow;
            }
        }

        /// <summary>Gets or sets the blackness of the color.</summary>
        /// <value>The blackness of the color.</value>
        public double KeyBlack
        {
            get
            {
                return this._keyBlack;
            }

            set
            {
                this._keyBlack = value;
                this._keyBlack = this._keyBlack > 1 ? 1 : this._keyBlack < 0 ? 0 : this._keyBlack;
            }
        }
    }
}