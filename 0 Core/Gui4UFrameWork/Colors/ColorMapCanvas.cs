﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ColorMapCanvas.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   This is a color-map where coordinate 0,0 is the center (in stead of top-left).
//   Left = minus
//   Right = plus
//   Top = minus
//   Bottom = plus
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;

namespace Gui4UFramework.Colors
{
    /// <summary>This is a color-map where coordinate 0,0 is the center (in stead of top-left).
    /// Left = minus
    /// Right = plus
    /// Top = minus
    /// Bottom = plus.</summary>
    public class ColorMapCanvas
    {
        /// <summary>The color map array that contains the actual data.</summary>
        private readonly ColorMap _colorMap;

        /// <summary>The total width of the color map.</summary>
        private readonly int _totalwidth;

        /// <summary>The total height of the color map.</summary>
        private readonly int _totalheight;

        /// <summary>
        /// The left side distance when canvas has a origin in the center of the color-map.
        /// </summary>
        private readonly float _left;

        /// <summary>
        /// The right side distance when canvas has a origin in the center of the color-map.
        /// </summary>
        private readonly float _right;

        /// <summary>
        /// The top side distance when canvas has a origin in the center of the color-map.
        /// </summary>
        private readonly float _top;

        /// <summary>
        /// The bottom side distance when canvas has a origin in the center of the color-map.
        /// </summary>
        private readonly float _bottom;

        /// <summary>Initializes a new instance of the <see cref="ColorMapCanvas"/> class.</summary>
        /// <param name="colorMap">The color map.</param>
        public ColorMapCanvas(ColorMap colorMap)
        {
#if DEBUG
            if (colorMap == null)
            {
                throw new ArgumentNullException(nameof(colorMap), "ColorMap can not be NULL");
            }
#endif
            this._colorMap = colorMap;
            this._totalwidth = colorMap.Width;
            this._totalheight = colorMap.Height;
            this._left = -(this._totalwidth * 0.5f);
            this._right = +(this._totalwidth * 0.5f);
            this._top = -(this._totalheight * 0.5f);
            this._bottom = +(this._totalheight * 0.5f);
        }

        /// <summary>Gets the specified color at given location. Point 0,0 is at center of the map.</summary>
        /// <param name="positionX">The x position.</param>
        /// <param name="positionY">The y position.</param>
        /// <returns>The color at given location.</returns>
        public GuiColor Get(int positionX, int positionY)
        {
            var locationX = (int)(positionX + (this.TotalWidth / 2));
            var locationY = (int)(positionY + (this.TotalHeight / 2));

            return this._colorMap.Get(locationX, locationY);
        }

        /// <summary>
        /// Gets the left side distance when canvas has a origin in the center of the color-map.
        /// </summary>
        /// <value>
        /// The left side..
        /// </value>
        public float Left
        {
            get
            {
                return this._left;
            }
        }

        /// <summary>
        /// Gets the right side distance when canvas has a origin in the center of the color-map.
        /// </summary>
        /// <value>
        /// The right side.
        /// </value>
        public float Right
        {
            get
            {
                return this._right;
            }
        }

        /// <summary>
        /// Gets the top side distance when canvas has a origin in the center of the color-map.
        /// </summary>
        /// <value>
        /// The top side.
        /// </value>
        public float Top
        {
            get
            {
                return this._top;
            }
        }

        /// <summary>
        /// Gets the bottom side distance when canvas has a origin in the center of the color-map.
        /// </summary>
        /// <value>
        /// The bottom side.
        /// </value>
        public float Bottom
        {
            get
            {
                return this._bottom;
            }
        }

        /// <summary>Gets the total width of the color-map.</summary>
        /// <value>The total width.</value>
        public float TotalWidth
        {
            get
            {
                return this._totalwidth;
            }
        }

        /// <summary>Gets the total height of the color-map.</summary>
        /// <value>The total height.</value>
        public float TotalHeight
        {
            get
            {
                return this._totalheight;
            }
        }
    }
}