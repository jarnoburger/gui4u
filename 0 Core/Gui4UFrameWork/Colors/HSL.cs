// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HSL.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Defines the FloatEventArgs type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

/******************************************************************/
/*****                                                        *****/
/*****     Project:           Adobe Color Picker Clone 1      *****/
/*****     Filename:          AdobeColors.cs                  *****/
/*****     Original Author:   Danny Blanchard                 *****/
/*****                        - scrabcakes@gmail.com          *****/
/*****     Updates:	                                          *****/
/*****      3/28/2005 - Initial Version : Danny Blanchard     *****/
/*****                                                        *****/
/******************************************************************/

using System.Diagnostics.CodeAnalysis;

namespace Gui4UFramework.Colors
{
    /// <summary>A representation of Color using the HSL-model.. Using double H, double S and double L. </summary>
    public sealed class HSL
    {
        /// <summary>The hue component of the color.</summary>
        private double _hue;

        /// <summary>The saturation component of the color.</summary>
        private double _saturation;

        /// <summary>The lighting component of the color.</summary>
        private double _lighting;

        /// <summary>
        /// Initializes a new instance of the <see cref="HSL"/> class.
        /// </summary>
        public HSL()
        {
            this._hue = 0;
            this._saturation = 0;
            this._lighting = 0;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HSL"/> class.
        /// </summary>
        /// <param name="hue">The hue component.</param>
        /// <param name="lighting">The lighting component.</param>
        /// <param name="saturation">The saturation component.</param>
        public HSL(double hue, double lighting, double saturation)
        {
            this._hue = hue;
            this._lighting = lighting;
            this._saturation = saturation;
        }

        /// <summary>Gets or sets the Hue component of the color.</summary>
        /// <value>Hue.</value>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "H")]
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1632:DocumentationTextMustMeetMinimumCharacterLength", Justification = "Reviewed. Suppression is OK here."),
        SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1630:DocumentationTextMustContainWhitespace", Justification = "Reviewed. Suppression is OK here.")]
        public double H
        {
            get
            {
                return this._hue;
            }

            set
            {
                this._hue = value;
                this._hue = this._hue > 1 ? 1 : this._hue < 0 ? 0 : this._hue;
            }
        }

        /// <summary>Gets or sets the Saturation component of the color.</summary>
        /// <value>Saturation.</value>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "S")]
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1630:DocumentationTextMustContainWhitespace", Justification = "Reviewed. Suppression is OK here.")]
        public double S
        {
            get
            {
                return this._saturation;
            }

            set
            {
                this._saturation = value;
                this._saturation = this._saturation > 1 ? 1 : this._saturation < 0 ? 0 : this._saturation;
            }
        }

        /// <summary>Gets or sets the lightness component of the color.</summary>
        /// <value>Lightness.</value>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "L")]
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1630:DocumentationTextMustContainWhitespace", Justification = "Reviewed. Suppression is OK here.")]
        public double L
        {
            get
            {
                return this._lighting;
            }

            set
            {
                this._lighting = value;
                this._lighting = this._lighting > 1 ? 1 : this._lighting < 0 ? 0 : this._lighting;
            }
        }
    }
}