// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ColorMap.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Defines the ColorMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Gui4UFramework.Colors
{
    /// <summary>
    /// Gives the user a 2d representation of the 1d color-array carried inside me.
    /// </summary>
    public class ColorMap
    {
        /// <summary>The colors contained in the color-map.</summary>
        private readonly GuiColor[] _colors;

        /// <summary>The width of the color-map.</summary>
        private readonly int _width;

        /// <summary>The height of the color-map.</summary>
        private readonly int _height;

        /// <summary>
        /// Initializes a new instance of the <see cref="ColorMap"/> class.
        /// </summary>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        public ColorMap(int width, int height)
        {
            this._width = width;
            this._height = height;

            this._colors = new GuiColor[this._width * this._height];
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ColorMap"/> class.
        /// </summary>
        /// <param name="colors">The colors in array.</param>
        /// <param name="width">The width of the array.</param>
        /// <param name="height">The height of the array.</param>
        public ColorMap(GuiColor[] colors, int width, int height)
        {
            this._colors = colors;
            this._width = width;
            this._height = height;
        }

        /// <summary>Gets the width of the color map.</summary>
        /// <value>The width.</value>
        public int Width
        {
            get
            {
                return this._width;
            }
        }

        /// <summary>Gets the height of the color map.</summary>
        /// <value>The height.</value>
        public int Height
        {
            get
            {
                return this._height;
            }
        }

        /// <summary>
        /// Gets the length of the array contained.
        /// </summary>
        /// <value>
        /// The length of the internal array.
        /// </value>
        public long Length
        {
            get
            {
                return this._width * this._height;
            }
        }

        /// <summary>
        /// Gets the internal color-array.
        /// </summary>
        /// <returns>The map converted to a array.</returns>
        public GuiColor[] ToArray()
        {
            return this._colors;
        }

        /// <summary>Gets the specified color at location x,y.</summary>
        /// <param name="locationX">The location x.</param>
        /// <param name="locationY">The location y.</param>
        /// <returns>The color at given location.</returns>
        public GuiColor Get(int locationX, int locationY)
        {
            return this._colors[locationX + (locationY * this._width)];
        }

        /// <summary>Sets the specified color at location x,y.</summary>
        /// <param name="locationX">The location x.</param>
        /// <param name="locationY">The location y.</param>
        /// <param name="color">The color.</param>
        public void Set(int locationX, int locationY, GuiColor color)
        {
            this._colors[locationX + (locationY * this._width)] = color;
        }

        /// <summary>Tries to set the specified color at location x,y.</summary>
        /// <param name="locationX">The location x.</param>
        /// <param name="locationY">The location y.</param>
        /// <param name="color">The color.</param>
        /// <returns>true when there is success setting it.</returns>
        public bool TrySet(int locationX, int locationY, GuiColor color)
        {
            var pos = locationX + (locationY * this._width);
            if (pos > this._colors.Length)
            {
                return false;
            }

            this._colors[pos] = color;
            return true;
        }

        /// <summary>
        /// Reverses this color-array in this instance.
        /// </summary>
        /// <returns>A reversed color-map.</returns>
        public ColorMap Reverse()
        {
            var reverseData = new GuiColor[this.Width * this.Height];
            var r = reverseData.Length - 1;
            foreach (var guiColor in this._colors)
            {
                reverseData[r] = guiColor;
                r--;
            }

            var map = new ColorMap(reverseData, this._width, this._height);

            return map;
        }
    }
}