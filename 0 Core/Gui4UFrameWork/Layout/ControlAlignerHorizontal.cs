﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControlAlignerHorizontal.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   This will align the control horizontally by given align option.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using Gui4UFramework.Graphics;
using Gui4UFramework.Layout.Enums;

namespace Gui4UFramework.Layout
{
    /// <summary>
    /// This will align the control horizontally by given align option.
    /// </summary>
    public class ControlAlignerHorizontal : ControlLayout
    {
        /// <summary>
        /// Gets or sets the horizontal alignment for each added control.
        /// </summary>
        /// <value>
        /// The alignment.
        /// </value>
        public HorizontalAlignment Alignment { get; set; }

        /// <summary>
        /// Calculates all the configurations for each specified control.
        /// </summary>
        /// <param name="refRectangle">The rectangle that all controls will be related to.</param>
        public override void Calculate(Rectangle refRectangle)
        {
            if (refRectangle == null)
            {
                throw new ArgumentNullException(nameof(refRectangle));
            }

            switch (this.Alignment)
            {
                case HorizontalAlignment.Left:
                    foreach (var control in this.Controls)
                    {
                        control.Config.PositionX = 0;
                    }

                    break;

                case HorizontalAlignment.Right:
                    foreach (var control in this.Controls)
                    {
                        control.Config.PositionX = refRectangle.Width - control.Config.Width;
                    }

                    break;

                case HorizontalAlignment.Center:
                    foreach (var control in this.Controls)
                    {
                        var parentCenter = refRectangle.Width / 2;
                        var controlCenter = control.Config.Width / 2;

                        control.Config.PositionX = parentCenter - controlCenter;
                    }

                    break;
            }
        }
    }
}