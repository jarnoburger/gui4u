﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControlLayout.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Defines the Aligner type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.ObjectModel;
using Gui4UFramework.Graphics;
using Gui4UFramework.Structural;

namespace Gui4UFramework.Layout
{
    /// <summary>
    /// This is a base class to control layout of specified controls compared to specified rectangle.
    /// </summary>
    public abstract class ControlLayout
    {
        /// <summary>
        /// Gets the controls that we modify.
        /// </summary>
        /// <value>
        /// The controls.
        /// </value>
        protected Collection<Control> Controls { get; private set; }

        /// <summary>Initializes a new instance of the <see cref="ControlLayout"/> class.</summary>
        protected ControlLayout()
        {
            this.Controls = new Collection<Control>();
        }

        /// <summary>
        /// Adds the specified control to the internal list, making it part of the calculation.
        /// </summary>
        /// <param name="control">The control.</param>
        public void AddControl(Control control)
        {
            this.Controls.Add(control);
        }

        /// <summary>
        /// Removes the specified control from the internal list.
        /// </summary>
        /// <param name="control">The control.</param>
        public void RemoveControl(Control control)
        {
            this.Controls.Remove(control);
        }

        /// <summary>
        /// Clears all the controls that we planned to layout.
        /// </summary>
        public void Clear()
        {
            this.Controls.Clear();
        }

        /// <summary>
        /// Calculates all the configurations for each specified control.
        /// </summary>
        /// <param name="refRectangle">The rectangle that all controls will be related to.</param>
        public abstract void Calculate(Rectangle refRectangle);
    }
}