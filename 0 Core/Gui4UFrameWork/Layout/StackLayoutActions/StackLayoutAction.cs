// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StackLayoutAction.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Defines the StackLayoutAction type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Gui4UFramework.Graphics;
using Gui4UFramework.Structural;

namespace Gui4UFramework.Layout.StackLayoutActions
{
    /// <summary>
    /// The base class to define the action that needs to be done on given neighbor control , to place it nicely next to home-control.
    /// </summary>
    public abstract class StackLayoutAction
    {
        /// <summary>
        /// Gets or sets the home control. The neighbor control will be placed relative to this.
        /// </summary>
        /// <value>
        /// The home control.
        /// </value>
        public Control HomeControl { get; set; }

        /// <summary>
        /// Gets or sets the neighbor control. this will be placed next to home control.
        /// </summary>
        /// <value>
        /// The neighbor control.
        /// </value>
        public Control ControlToMove { get; set; }

        /// <summary>
        /// Gets or sets the area where the placed neighbor must fit inside.
        /// </summary>
        /// <value>
        /// The area in which the neighbors must fit.
        /// </value>
        public Rectangle Area { get; set; }

        /// <summary>
        /// Checks if this action can be applied or not.
        /// </summary>
        /// <returns>True when it can be applied.</returns>
        public abstract bool Check();

        /// <summary>
        /// Applies the placement action to the neighbor.
        /// </summary>
        public abstract void Apply();

        /// <summary>
        /// Validates if given location is still in the area.
        /// </summary>
        /// <param name="positionX">The x position.</param>
        /// <param name="positionY">The y position.</param>
        /// <returns>True when its still inside.</returns>
        public bool ValidatePositionInArea(float positionX, float positionY)
        {
            return this.Area.Contains((int)positionX, (int)positionY);
        }
    }
}