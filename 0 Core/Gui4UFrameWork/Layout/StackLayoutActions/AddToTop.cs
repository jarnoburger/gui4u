// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AddToTop.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Sets given neighbor-control above given home-control.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Gui4UFramework.Layout.StackLayoutActions
{
    /// <summary>
    /// Sets given neighbor-control above given home-control.
    /// </summary>
    public class AddToTop : StackLayoutAction
    {
        /// <summary>
        /// Checks if this action can be applied or not.
        /// </summary>
        /// <returns>
        /// True when it can be applied.
        /// </returns>
        public override bool Check()
        {
            var currentPositionX = this.HomeControl.Config.PositionX;
            var currentPositionY = this.HomeControl.Config.PositionY;

            var targetPositionX = currentPositionX;
            var targetPositionY = currentPositionY - this.ControlToMove.Config.Height;

            if (this.ValidatePositionInArea(targetPositionX, targetPositionY) == false)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Applies the placement action to the neighbor.
        /// </summary>
        public override void Apply()
        {
            var currentPositionX = this.HomeControl.Config.PositionX;
            var currentPositionY = this.HomeControl.Config.PositionY;

            var targetPositionX = currentPositionX;
            var targetPositionY = currentPositionY - this.ControlToMove.Config.Height;

            this.ControlToMove.Config.PositionX = targetPositionX;
            this.ControlToMove.Config.PositionY = targetPositionY;
        }
    }
}