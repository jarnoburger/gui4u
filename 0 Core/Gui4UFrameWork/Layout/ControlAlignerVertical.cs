﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControlAlignerVertical.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   This will align the added controls vertically by given align option.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Diagnostics.CodeAnalysis;
using Gui4UFramework.Graphics;
using Gui4UFramework.Layout.Enums;
using Gui4UFramework.Structural;

namespace Gui4UFramework.Layout
{
    /// <summary>
    /// This will align the added controls vertically by given align option.
    /// </summary>
    public class ControlAlignerVertical : ControlLayout
    {
        /// <summary>
        /// Gets or sets the vertical alignment for each added control.
        /// </summary>
        /// <value>
        /// The alignment.
        /// </value>
        public VerticalAlignment Alignment { get; set; }

        /// <summary>
        /// Calculates all the configurations for each specified control.
        /// </summary>
        /// <param name="refRectangle">The rectangle that all controls will be related to.</param>
        public override void Calculate(Rectangle refRectangle)
        {
            switch (this.Alignment)
            {
                case VerticalAlignment.Top:
                    foreach (var control in this.Controls)
                    {
                        this.SetToTop(refRectangle, control);
                    }

                    break;

                case VerticalAlignment.Bottom:
                    foreach (var control in this.Controls)
                    {
                        this.SetToBottom(refRectangle, control);
                    }

                    break;

                case VerticalAlignment.Center:
                    foreach (var control in this.Controls)
                    {
                        this.SetToCenter(refRectangle, control);
                    }

                    break;
            }
        }

        /// <summary>
        /// Sets the configuration of specified control to the top inside of the parent control.
        /// </summary>
        /// <param name="refRectangle">The rectangle that all controls will be related to.</param>
        /// <param name="control">The control.</param>
        // ReSharper disable once UnusedParameter.Local
        [SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic"), SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "refRectangle")]
        private void SetToTop(Rectangle refRectangle, Control control)
        {
            control.Config.PositionY = 0;
        }

        /// <summary>
        /// Sets the configuration of specified control to the bottom inside of the parent control.
        /// </summary>
        /// <param name="refRectangle">The rectangle that all controls will be related to.</param>
        /// <param name="control">The control.</param>
        [SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
        private void SetToBottom(Rectangle refRectangle, Control control)
        {
            control.Config.PositionY = refRectangle.Height - control.Config.Height;
        }

        /// <summary>
        /// Sets the configuration of specified control to the center inside of the parent control.
        /// </summary>
        /// <param name="refRectangle">The rectangle that all controls will be related to.</param>
        /// <param name="control">The control.</param>
        [SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
        private void SetToCenter(Rectangle refRectangle, Control control)
        {
            var parentCenter = refRectangle.Height / 2;
            var controlCenter = control.Config.Height / 2;

            control.Config.PositionY = parentCenter - controlCenter;
        }
    }
}