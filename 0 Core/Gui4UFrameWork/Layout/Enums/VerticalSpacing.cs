﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VerticalSpacing.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Tells about which spacing algorithm should be used horizontally.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Gui4UFramework.Layout.Enums
{
    /// <summary>Tells about which spacing algorithm should be used horizontally.</summary>
    public enum VerticalSpacing
    {
        /// <summary>
        /// Spaces out all the controls from the top side , with Small Spacing in between.
        /// </summary>
        Topside,

        /// <summary>
        /// Spaces out all the controls from the whole length , all in between spacing is used to space each control evenly until total height is met.
        /// </summary>
        Evenly,

        /// <summary>
        /// Spaces out all the controls from the center, with Small Spacing in between.
        /// </summary>
        Centered,

        /// <summary>
        /// Spaces out all the controls from the bottom side , with Small Spacing in between.
        /// </summary>
        BottomSide,
    }
}