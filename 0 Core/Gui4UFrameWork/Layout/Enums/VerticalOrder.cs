﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VerticalOrder.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   How the objects are stacked next to each other.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Gui4UFramework.Layout.Enums
{
    /// <summary>
    /// How the objects are stacked next to each other.
    /// </summary>
    public enum VerticalOrder
    {
        /// <summary>
        /// Stack the items from the top to the bottom.
        /// </summary>
        TopToBottom,

        /// <summary>
        /// Stack the items from the bottom to the top.
        /// </summary>
        BottomToTop,
    }
}