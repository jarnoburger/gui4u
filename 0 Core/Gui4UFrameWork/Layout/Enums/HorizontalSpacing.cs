﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Spacing.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Tells about which spacing algorithm should be used horizontally.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Gui4UFramework.Layout.Enums
{
    /// <summary>Tells about which spacing algorithm should be used horizontally.</summary>
    public enum HorizontalSpacing
    {
        /// <summary>
        /// Spaces out all the controls from the left side , with Small Spacing in between.
        /// </summary>
        LeftSide,

        /// <summary>
        /// Spaces out all the controls from the whole length , all in between spacing is used to space each control evenly until total width is met.
        /// </summary>
        Evenly,

        /// <summary>
        /// Spaces out all the controls from the center, with Small Spacing in between.
        /// </summary>
        Centered,

        /// <summary>
        /// Spaces out all the controls from the right side , with Small Spacing in between.
        /// </summary>
        RightSide,
    }
}