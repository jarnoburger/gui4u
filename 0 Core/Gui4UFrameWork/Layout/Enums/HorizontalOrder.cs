﻿namespace Gui4UFramework.Layout.Enums
{
    /// <summary>
    /// How the objects are stacked next to each other.
    /// </summary>
    public enum HorizontalOrder
    {
        /// <summary>
        /// Stack the items from the left to the right.
        /// </summary>
        LeftToRight,

        /// <summary>
        /// Stacks the items from the right to the left.
        /// </summary>
        RightToLeft,
    }
}