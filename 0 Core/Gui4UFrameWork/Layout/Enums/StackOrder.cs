﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StackOrder.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Which order should we first apply ?.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Gui4UFramework.Layout.Enums
{
    /// <summary>
    /// Which order should we first apply ?.
    /// </summary>
    public enum StackOrder
    {
        /// <summary>
        /// Order first horizontal and then vertical.
        /// </summary>
        FirstHorizontalThenVertical,

        /// <summary>
        /// Order first vertical and the horizontal.
        /// </summary>
        FirstVerticalThenHorizontal,
    }
}