﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControlStacker.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Stacks the items next or on top of each other in a :
//   - top/down
//   - down/top
//   - left/right
//   - right/left
//   order.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using Gui4UFramework.Graphics;
using Gui4UFramework.Layout.Enums;
using Gui4UFramework.Layout.StackLayoutActions;
using Gui4UFramework.Structural;

namespace Gui4UFramework.Layout
{
    /// <summary>
    /// Stacks the items next or on top of each other in a :
    /// - top/down
    /// - down/top
    /// - left/right
    /// - right/left
    /// order.
    /// </summary>
    public class ControlStacker : ControlLayout
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ControlStacker"/> class.
        /// </summary>
        public ControlStacker()
        {
            var theme = new Theme();
            this.SpaceInBetween = theme.ControlSmallSpacing;
        }

        /// <summary>
        /// Gets or sets the space in between each control.
        /// </summary>
        /// <value>
        /// The space in between.
        /// </value>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private float SpaceInBetween { get; set; }

        /// <summary>
        /// Gets or sets the stack order (if we do horizontal of vertical first).
        /// </summary>
        /// <value>
        /// The stack order.
        /// </value>
        public StackOrder StackOrder { get; set; }

        /// <summary>
        /// Gets or sets the horizontal order.
        /// Left->Right or Right->Left.
        /// </summary>
        /// <value>
        /// The horizontal order.
        /// </value>
        public HorizontalOrder HorizontalOrder { get; set; }

        /// <summary>
        /// Gets or sets the vertical order.
        /// Top->Down or Down->Top.
        /// </summary>
        /// <value>
        /// The vertical order.
        /// </value>
        public VerticalOrder VerticalOrder { get; set; }

        public Collection<StackLayoutAction> Actions { get; private set; }

        /// <summary>
        /// Calculates all the configurations for each specified control.
        /// </summary>
        /// <param name="refRectangle">The rectangle that all controls will be related to.</param>
        public override void Calculate(Rectangle refRectangle)
        {
            this.Actions = this.CreateActions();

            var firstControl = this.Controls[0];

            this.SetStartPosition(firstControl, refRectangle);

            for (int index = 0; index < this.Controls.Count; index++)
            {
                var controlToMove = this.Controls[index];
                if (controlToMove == firstControl)
                {
                    continue;
                }

                var previousControl = this.Controls[index - 1];

                this.Actions[0].HomeControl = previousControl;
                this.Actions[0].ControlToMove = controlToMove;
                this.Actions[0].Area = refRectangle;
                if (this.Actions[0].Check() == true)
                {
                    this.Actions[0].Apply();
                }
                else
                {
                    this.Actions[1].HomeControl = previousControl;
                    this.Actions[1].ControlToMove = controlToMove;
                    this.Actions[1].Area = refRectangle;

                    this.Actions[1].Apply();
                }
            }
        }

        private Collection<StackLayoutAction> CreateActions()
        {
            Collection<StackLayoutAction> actions = new Collection<StackLayoutAction>();
            switch (this.StackOrder)
            {
                case StackOrder.FirstHorizontalThenVertical:
                    switch (this.HorizontalOrder)
                    {
                        case HorizontalOrder.LeftToRight:
                            actions.Add(new AddToRight());
                            break;

                        case HorizontalOrder.RightToLeft:
                            actions.Add(new AddToLeft());
                            break;
                    }

                    switch (this.VerticalOrder)
                    {
                        case VerticalOrder.TopToBottom:
                            actions.Add(new AddToBottom());
                            break;

                        case VerticalOrder.BottomToTop:
                            actions.Add(new AddToTop());
                            break;
                    }

                    break;

                case StackOrder.FirstVerticalThenHorizontal:
                    switch (this.VerticalOrder)
                    {
                        case VerticalOrder.TopToBottom:
                            actions.Add(new AddToBottom());
                            break;

                        case VerticalOrder.BottomToTop:
                            actions.Add(new AddToTop());
                            break;
                    }

                    switch (this.HorizontalOrder)
                    {
                        case HorizontalOrder.LeftToRight:

                            actions.Add(new AddToRight());
                            break;

                        case HorizontalOrder.RightToLeft:

                            actions.Add(new AddToLeft());
                            break;
                    }
                    break;
            }

            return actions;
        }

        private void SetStartPosition(Control control, Rectangle area)
        {
            switch (HorizontalOrder)
            {
                case HorizontalOrder.LeftToRight:
                    control.Config.PositionX = 0;
                    break;

                case HorizontalOrder.RightToLeft:
                    control.Config.PositionX = area.Right - control.Config.Width;
                    break;
            }

            switch (VerticalOrder)
            {
                case VerticalOrder.TopToBottom:
                    control.Config.PositionY = 0;
                    break;

                case VerticalOrder.BottomToTop:
                    control.Config.PositionY = area.Bottom - control.Config.Height;
                    break;
            }
        }
    }
}