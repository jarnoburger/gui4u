﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControlSpacerHorizontal.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   This class will space out given controls in specified rectangle.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Diagnostics.CodeAnalysis;
using Gui4UFramework.Graphics;
using Gui4UFramework.Layout.Enums;

namespace Gui4UFramework.Layout
{
    /// <summary>This class will space out given controls in specified rectangle.</summary>
    public class ControlSpacerHorizontal : ControlLayout
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ControlSpacerHorizontal"/> class.
        /// </summary>
        public ControlSpacerHorizontal()
        {
            var theme = new Theme();
            this.SpaceInBetween = theme.ControlSmallSpacing;
        }

        /// <summary>
        /// Gets or sets the space in between each control.
        /// </summary>
        /// <value>
        /// The space in between.
        /// </value>
        private float SpaceInBetween { get; set; }

        /// <summary>
        /// Gets or sets the horizontal spacing in relation to specified rectangle.
        /// </summary>
        /// <value>
        /// The horizontal spacing.
        /// </value>
        public HorizontalSpacing Spacing { get; set; }

        /// <summary>
        /// Calculates all the configurations for each specified control.
        /// </summary>
        /// <param name="refRectangle">The rectangle that all controls will be related to.</param>
        public override void Calculate(Rectangle refRectangle)
        {
            switch (this.Spacing)
            {
                case HorizontalSpacing.LeftSide:
                    this.SetToLeft(refRectangle);
                    break;

                case HorizontalSpacing.Evenly:
                    this.SetToEvenly(refRectangle);
                    break;

                case HorizontalSpacing.Centered:
                    this.SetToCenterd(refRectangle);
                    break;

                case HorizontalSpacing.RightSide:
                    this.SetToRightSide(refRectangle);
                    break;
            }
        }

        /// <summary>
        /// Sets the controls, to layout to the right side with small spacing in between.
        /// </summary>
        /// <param name="refRectangle">The reference rectangle.</param>
        private void SetToRightSide(Rectangle refRectangle)
        {
            var x = refRectangle.Width - this.SpaceInBetween;
            foreach (var control in this.Controls)
            {
                x = x - control.Config.Width;
                control.Config.PositionX = x;
                x = x - this.SpaceInBetween;
            }
        }

        /// <summary>
        /// Sets the controls, to layout to the center with small spacing in between.
        /// </summary>
        /// <param name="refRectangle">The reference rectangle.</param>
        private void SetToCenterd(Rectangle refRectangle)
        {
            // calculate the width for all the controls.
            var totalControlWidth = 0.0f;
            foreach (var control in this.Controls)
            {
                totalControlWidth = totalControlWidth + control.Config.Width;
            }

            // calculate spacing
            var totalSpacing = (this.Controls.Count - 1) * this.SpaceInBetween;
            totalControlWidth = totalControlWidth + totalSpacing;

            var x = (refRectangle.Width * 0.5f) - (totalControlWidth * 0.5f);
            foreach (var control in this.Controls)
            {
                control.Config.PositionX = x;
                x = x + control.Config.Width;
                x = x + this.SpaceInBetween;
            }
        }

        /// <summary>
        /// Sets the controls, to layout to the left side with small spacing in between.
        /// </summary>
        /// <param name="refRectangle">The reference rectangle.</param>
        [SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "refRectangle")]
        private void SetToLeft(Rectangle refRectangle)
        {
            var x = this.SpaceInBetween;
            foreach (var control in this.Controls)
            {
                control.Config.PositionX = x;
                x = x + control.Config.Width;
                x = x + this.SpaceInBetween;
            }
        }

        /// <summary>
        /// Sets the controls, to layout spaced evenly in the given rectangle width.
        /// </summary>
        /// <param name="refRectangle">The reference rectangle.</param>
        private void SetToEvenly(Rectangle refRectangle)
        {
            // calculate the width for all the controls.
            var totalControlWidth = 0.0f;
            foreach (var control in this.Controls)
            {
                totalControlWidth = totalControlWidth + control.Config.Width;
            }

            // calculate the space length
            var freeSpace = refRectangle.Width - totalControlWidth;
            var spacersCount = this.Controls.Count + 1;
            var spaceLength = freeSpace / spacersCount;

            // set each control
            var x = spaceLength;
            foreach (var control in this.Controls)
            {
                control.Config.PositionX = x;
                x = x + control.Config.Width;
                x = x + spaceLength;
            }
        }
    }
}