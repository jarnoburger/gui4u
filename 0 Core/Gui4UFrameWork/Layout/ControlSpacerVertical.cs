﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControlSpacerVertical.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   This class will space out given controls in specified rectangle.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Gui4UFramework.Graphics;
using Gui4UFramework.Layout.Enums;

namespace Gui4UFramework.Layout
{
    /// <summary>This class will space out given controls in specified rectangle.</summary>
    public class ControlSpacerVertical : ControlLayout
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ControlSpacerVertical"/> class.
        /// </summary>
        public ControlSpacerVertical()
        {
            var theme = new Theme();
            this.SpaceInBetween = theme.ControlSmallSpacing;
        }

        /// <summary>
        /// Gets or sets the space in between each control.
        /// </summary>
        /// <value>
        /// The space in between.
        /// </value>
        private float SpaceInBetween { get; set; }

        /// <summary>
        /// Gets or sets the horizontal spacing in relation to specified rectangle.
        /// </summary>
        /// <value>
        /// The horizontal spacing.
        /// </value>
        public VerticalSpacing Spacing { get; set; }

        /// <summary>
        /// Calculates all the configurations for each specified control.
        /// </summary>
        /// <param name="refRectangle">The rectangle that all controls will be related to.</param>
        public override void Calculate(Rectangle refRectangle)
        {
            switch (this.Spacing)
            {
                case VerticalSpacing.Topside:
                    this.SetToTop(refRectangle);
                    break;

                case VerticalSpacing.Evenly:
                    this.SetToEvenly(refRectangle);
                    break;

                case VerticalSpacing.Centered:
                    this.SetToCenterd(refRectangle);
                    break;

                case VerticalSpacing.BottomSide:
                    this.SetToBottomSide(refRectangle);
                    break;
            }
        }

        /// <summary>
        /// Sets the controls, to layout to the right side with small spacing in between.
        /// </summary>
        /// <param name="refRectangle">The reference rectangle.</param>
        private void SetToBottomSide(Rectangle refRectangle)
        {
            var y = refRectangle.PositionY + refRectangle.Height - this.SpaceInBetween;
            foreach (var control in this.Controls)
            {
                y = y - control.Config.Height;
                control.Config.PositionY = y;
                y = y - this.SpaceInBetween;
            }
        }

        /// <summary>
        /// Sets the controls, to layout to the center with small spacing in between.
        /// </summary>
        /// <param name="refRectangle">The reference rectangle.</param>
        private void SetToCenterd(Rectangle refRectangle)
        {
            // calculate the width for all the controls.
            var totalControlHeight = 0.0f;
            foreach (var control in this.Controls)
            {
                totalControlHeight = totalControlHeight + control.Config.Height;
            }

            // calculate spacing
            var totalSpacing = (this.Controls.Count - 1) * this.SpaceInBetween;
            totalControlHeight = totalControlHeight + totalSpacing;

            var y = (refRectangle.Height * 0.5f) - (totalControlHeight * 0.5f) + refRectangle.PositionY;
            foreach (var control in this.Controls)
            {
                control.Config.PositionY = y;
                y = y + control.Config.Height;
                y = y + this.SpaceInBetween;
            }
        }

        /// <summary>
        /// Sets the controls, to layout to the left side with small spacing in between.
        /// </summary>
        /// <param name="refRectangle">The reference rectangle.</param>
        // ReSharper disable once UnusedParameter.Local
        private void SetToTop(Rectangle refRectangle)
        {
            var y = this.SpaceInBetween + refRectangle.PositionY;
            foreach (var control in this.Controls)
            {
                control.Config.PositionY = y;
                y = y + control.Config.Height;
                y = y + this.SpaceInBetween;
            }
        }

        /// <summary>
        /// Sets the controls, to layout spaced evenly in the given rectangle width.
        /// </summary>
        /// <param name="refRectangle">The reference rectangle.</param>
        private void SetToEvenly(Rectangle refRectangle)
        {
            // calculate the width for all the controls.
            var totalControlHeight = 0.0f;
            foreach (var control in this.Controls)
            {
                totalControlHeight = totalControlHeight + control.Config.Height;
            }

            // calculate the space length
            var freeSpace = refRectangle.Height - totalControlHeight;
            var spacersCount = this.Controls.Count + 1;
            var spaceLength = freeSpace / spacersCount;

            // set each control
            var y = spaceLength + refRectangle.PositionY;
            foreach (var control in this.Controls)
            {
                control.Config.PositionY = y;
                y = y + control.Config.Height;
                y = y + spaceLength;
            }
        }
    }
}