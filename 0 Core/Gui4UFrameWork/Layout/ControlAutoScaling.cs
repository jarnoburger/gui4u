﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AutoScaler.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Defines the AutoScaler type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.ObjectModel;
using Gui4UFramework.Graphics;
using Gui4UFramework.Structural;

namespace Gui4UFramework.Layout
{
    public class ControlAutoScaling
    {
        private readonly Collection<Control> _controls;

        public ControlAutoScaling()
        {
            this._controls = new Collection<Control>();
        }

        /// <summary>
        /// Scales this window to surround each control on it.
        /// </summary>
        /// <param name="parent">The parent control.</param>
        public void Calculate(Control parent)
        {
#if DEBUG
            if (parent == null)
            {
                throw new ArgumentNullException(nameof(parent));
            }
#endif

            // start out with a empty rectangle.
            Rectangle union = Rectangle.Empty();

            // add all the rectangles of each child-control to me.
            foreach (var child in _controls)
            {
                // a child can be a control , get it.
                var control = child as Control;
                if (control == null)
                {
                    continue;
                }

                if (control.Config.Changed == false)
                {
                    return;
                }

                // get the control's draw rectangle
                var rectangle = new Rectangle(
                                            control.Config.PositionX,
                                            control.Config.PositionY,
                                            control.State.Width,
                                            control.State.Height);

                // unify it with me
                union = Rectangle.Union(union, rectangle);
            }

            if (union.Width < 10)
            {
                union.Width = 10;
            }
            if (union.Height < 10)
            {
                union.Height = 10;
            }

            // The union contains all the controls..
            // use this as the new size
            parent.Config.Width = union.Left + union.Width;
            parent.Config.Height = union.Top + union.Height;
        }

        public void Add(Control control)
        {
            this._controls.Add(control);
        }
    }
}