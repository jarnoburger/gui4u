// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControlExtensions.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Defines the ControlExtensions type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Diagnostics.CodeAnalysis;
using Gui4UFramework.Graphics;

namespace Gui4UFramework.Structural
{
    /// <summary>
    /// Contains extra extensions for the Control class..
    /// </summary>
    public static class ControlExtensions
    {
        /// <summary>
        /// Determines whether given location is inside the control.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <param name="positionX">The positionX of the location.</param>
        /// <param name="positionY">The positionY of the location.</param>
        /// <returns>Whether the given location is inside.</returns>
        public static bool IsPointInside(this Control control, int positionX, int positionY)
        {
#if DEBUG
            if (control == null)
            {
                throw new ArgumentNullException(nameof(control));
            }
#endif

            var absPos = new DVector2(control.State.DrawPosition.X, control.State.DrawPosition.Y);

            return absPos.X < positionX && positionX < (absPos.X + control.State.Width) &&
                   absPos.Y < positionY && positionY < (absPos.Y + control.State.Height);
        }

        /// <summary>
        /// Removes the child control from the parent. Disconnect the manager too..
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="child">The child.</param>
        [SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters")]
        public static void RemoveControl(this Control parent, Control child)
        {
#if DEBUG
            if (parent == null)
            {
                throw new ArgumentNullException(nameof(parent));
            }

            if (child == null)
            {
                throw new ArgumentNullException(nameof(child));
            }
#endif

            child.Parent = null;
            child.Manager = null;

            parent.Children.Remove(child);
        }
    }
}