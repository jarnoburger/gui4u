// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DrawState.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Defines the DrawState type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Gui4UFramework.Graphics;

namespace Gui4UFramework.Structural
{
    /// <summary>
    /// Contains all the info for drawing a control on the screen.
    /// </summary>
    public class DrawState
    {
        private string _currentTextureName;

        #region Declare

        #endregion

        #region Initialize
        /// <summary>
        /// Initializes a new instance of the <see cref="DrawState"/> class.
        /// </summary>
        public DrawState()
        {
            this.MouseHoveringOver = false;
            this.UseHovering = false;
            this.Visible = true;
            this.IsLoaded = false;
            this.MustReload = false;
        }
        #endregion

        #region Properties Size and location
        /// <summary>Gets or sets the location in local space to move and center the scene node when displayed.</summary>
        /// <value>The offset to start the draw.</value>
        public DVector2 Offset { get; set; }

        /// <summary>
        /// Gets or sets the SourceRectangle. Each control has its rectangle , some controls have stuff over them (clipped)
        /// This contains the part that will be shown.
        /// </summary>
        /// <value>
        /// The source rectangle.
        /// </value>
        public Rectangle SourceRectangle { get; set; } = new Rectangle(0, 0, 0, 0);
        
        /// <summary>Gets or sets the position where the object is drawn relative to the total 3d canvas.
        /// The scene node's , calculated world transform.</summary>
        /// <value>The root position where i start drawing.</value>
        public DVector2 DrawPosition { get; set; }

        /// <summary>Gets or sets the scene node's transformed width.</summary>
        /// <value>The width of the part that i draw.</value>
        public float Width { get; set; }

        /// <summary>Gets or sets the scene node's transformed height.</summary>
        /// <value>The height of the part that i draw.</value>
        public float Height { get; set; }
        #endregion

        #region Painting
        public bool MustPaint { get; set; }

        public bool IsRepainted { get; set; }
        #endregion

        #region Other Properties
        /// <summary>Gets or sets the update index.
        /// Each node has a moment of update.
        /// On each update of a node , updateIndex is incremented
        /// Making each node have a unique updateIndex.
        /// You can use this for depth sorting , or finding out which control is on top
        /// If ControlA.UpdateIndex is smaller then ControlB.UpdateIndex , then ControlA is underneath ControlB.</summary>
        /// <value>When i update.</value>
        public long UpdateIndex { get; set; }

        /// <summary>Gets or sets a value indicating whether the mouse is hovering over the control.</summary>
        /// <value>Whether the mouse is hovering over me.</value>
        public bool MouseHoveringOver { get; set; }

        /// <summary>Gets or sets a value indicating whether we use hovering behavior for this control.</summary>
        /// <value><c>true</c> if [use hovering]; otherwise, <c>false</c>.</value>
        public bool UseHovering { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the control is visible on the canvas.
        /// </summary>
        /// <value>
        /// Control visibility.
        /// </value>
        public bool Visible { get; set; }

        public bool MustReload { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is loaded using the Load() function.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is loaded; otherwise, <c>false</c>.
        /// </value>
        public bool IsLoaded { get; set; }

        /// <summary>
        /// Gets or sets the name of the current texture used when we are using the standard Draw call.
        /// </summary>
        /// <value>
        /// The name of the current texture.
        /// </value>
        public string CurrentTextureName
        {
            get { return _currentTextureName; }
            set
            {
                if (_currentTextureName != value)
                {
                    MustPaint = true;
                }

                _currentTextureName = value;
            }
        }

        #endregion

        #region Debug
        /// <summary>
        /// Debugs this instance in the debug-output window.
        /// </summary>
        public void Debug()
        {
            System.Diagnostics.Debug.WriteLine(this.ToString());
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return $"State  : Pos{this.DrawPosition} Size({this.Width}, {this.Height})";
        }
        #endregion
    }
}