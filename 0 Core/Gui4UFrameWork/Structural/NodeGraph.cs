// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NodeGraph.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Defines a scene graph.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Diagnostics;
using Gui4UFramework.Graphics;
using Gui4UFramework.Management;

namespace Gui4UFramework.Structural
{
    /// <summary>
    /// Defines a scene graph.
    /// </summary>
    public class NodeGraph
    {
        #region Initialize
        /// <summary>
        /// Initializes a new instance of the <see cref="NodeGraph"/> class.
        /// </summary>
        /// <param name="manager">The manager.</param>
        public NodeGraph(NodeManager manager)
        {
            this.RootNode = new RootNode("RootNode");
            this.Manager = manager;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the <see cref="T:SceneNode"/> that is the root of the graph.
        /// </summary>
        /// <value>
        /// The first node of the tree. The root.
        /// </value>
        public RootNode RootNode { get; set; }

        /// <summary>
        /// Gets or sets the manager.
        /// The manager is the object that gives you control to drawing/input/etc.. Your main access point to the outside world.
        /// </summary>
        /// <value>
        /// The manager.
        /// </value>
        public NodeManager Manager { get; set; }
        #endregion

        #region Main Functions
        /// <summary>
        /// Use this method to query for any required services, and load any non-graphics resources.
        /// </summary>
        public void Initialize()
        {
            this.InitializeRecursive(RootNode);
        }

        /// <summary>
        /// walk trough every node recursive , and if a node is a control , then initialize it.
        /// Use this method to query for any required services, and load any non-graphics resources.
        /// </summary>
        /// <param name="rootNode">The node where we start to initialize recursive.</param>
        private void InitializeRecursive(Node rootNode)
        {
            var control = rootNode as Control;
            if (control != null)
            {
                ControlInitialize(control);
            }

            // Update children recursively
            for (var i = 0; i < rootNode.Children.Count; i++)
            {
                this.InitializeRecursive(rootNode.Children[i]);
            }
        }

        /// <summary>
        /// 1 reset root-node to origin
        /// 2 calculate the positions for every control recursive
        /// 3 call the update on each control recursive
        /// Called when the game has determined that game logic needs to be processed.
        /// This might include the management of the game state, the processing of user input, or the updating of simulation data.
        /// Use this method with game-specific logic.
        /// </summary>
        /// <param name="gameTime">The time that will be used by the nodes.</param>
        public void Update(GameTime gameTime)
        {
            var updateIndex = 0;
            this.UpdateRecursive(gameTime, RootNode, ref updateIndex);
        }

        /// <summary>
        /// Call the UpdateCall on each node recursively
        /// Called when the game has determined that game logic needs to be processed.
        /// This might include the management of the game state, the processing of user input, or the updating of simulation data.
        /// Use this method with game-specific logic.</summary>
        /// <param name="time">The time used for updating animated objects thanks to time differences.</param>
        /// <param name="currentNode">The current Node to update.</param>
        /// <param name="updateIndex">The update Index , used for height calculations.</param>
        private void UpdateRecursive(GameTime time, Node currentNode, ref int updateIndex)
        {
            var currentControl = currentNode as Control;

            // ReSharper disable once UseNullPropagationWhenPossible
            if (currentControl != null && currentControl.DrawMeAndMyChildren)
            {
                ControlUpdate(time,ref updateIndex, currentControl);
            }

            // Update children recursively
            for (var i = 0; i < currentNode.Children.Count; i++)
            {
                if (currentControl != null && currentControl.DrawMeAndMyChildren == false)
                {
                    if (currentNode.Children[i] is Control)
                    {
                        continue;
                    }
                }

                this.UpdateRecursive(time, currentNode.Children[i], ref updateIndex);
            }
        }

        /// <summary>
        /// This will repaint the items canvas
        /// </summary>
        /// <param name="gameTime"></param>
        public void Paint(GameTime gameTime)
        {
            this.PaintRecursive(gameTime, RootNode);
        }

        private void PaintRecursive(GameTime time, Node currentNode)
        {
            var currentControl = currentNode as Control;

            // ReSharper disable once UseNullPropagationWhenPossible
            if (currentControl != null)
            {
                if (currentControl.State.MustPaint)
                {
                    ControlPaint(time, currentControl);
                }
            }

            // Update children recursively
            for (var i = 0; i < currentNode.Children.Count; i++)
            {
                if (currentControl != null && 
                    currentControl.DrawMeAndMyChildren == false &&
                    currentNode.Children[i] is Control)
                {
                    continue;
                }

                this.PaintRecursive(time, currentNode.Children[i]);
            }
        }

        /// <summary>
        /// Allows the scene graph to render.
        /// Called when the game determines it is time to draw a frame. Use this method with game-specific rendering code.
        /// </summary>
        /// <param name="gameTime">
        /// The game Time. Used by objects to change  what they draw , because time  is changed.
        /// </param>
        public void Draw(GameTime gameTime)
        {
            this.DrawRecursive(gameTime, RootNode);
        }

        /// <summary>Draws each node recursive. But does not draw it is not visible.
        /// Your code now already knows where the draw and how to draw each control , thanks to your update implementation.</summary>
        /// <param name="gameTime">The game Time. Used by objects to change  what they draw , because time  is changed.</param>
        /// <param name="currentNode">The current Node to draw.</param>
        // ReSharper disable once UnusedParameter.Local
        private void DrawRecursive(GameTime gameTime, Node currentNode)
        {
            // if current node is drawable
            var control = currentNode as Control;

            // if we have a control
            // ReSharper disable once UseNullPropagationWhenPossible
            if (control != null)
            {
                // and if "me and my children should be drawn" (like tab-sheets)
                if (control.DrawMeAndMyChildren)
                {
                    if (control.State.IsLoaded == false)
                    {
                        Debug.WriteLine("Control not loaded : " + control.Name );
                        return;
                    }

                    // and if i should be drawn
                    if (control.State.Visible)
                    {
                        ControlDraw(control);
                    }
                }
            }

            // and go recursive
            for (var i = 0; i < currentNode.Children.Count; i++)
            {
                var currentChildNode = currentNode.Children[i];

                // if control is not null, then we are a Control.
                // if  control is a Control, then maybe we should not draw her or its children
                if (control != null && control.DrawMeAndMyChildren == false)
                {
                    if (currentChildNode is Control)
                    {
                        continue;
                    }
                }

                // draw my child
                this.DrawRecursive(gameTime, currentChildNode);
            }
        }

        /// <summary>Called when graphics resources need to be unloaded.
        /// Override this method to unload any game-specific graphics resources.
        /// Use this for the :
        /// - destruction of the internal embedded controls.
        /// - cleaning up of variables and resource in this control.</summary>
        public void UnloadContent()
        {
            this.UnloadContentRecursive(RootNode);
        }

        /// <summary>Tells each control to unload content recursive.</summary>
        /// <param name="currentNode">The current Node to unload.</param>
        private void UnloadContentRecursive(Node currentNode)
        {
            var currentControl = currentNode as Control;
            currentControl?.UnloadContent();

            // Update children recursively
            for (var i = 0; i < currentNode.Children.Count; i++)
            {
                this.UnloadContentRecursive(currentNode.Children[i]);
            }
        }
        #endregion

        #region Item Functions
        private void ControlInitialize(Control control)
        {
            Debug.WriteLine("Initializing : " + control.Name);

            if (control.Manager == null)
            {
                control.Manager = this.Manager;
            }

            if (string.IsNullOrEmpty(control.State.CurrentTextureName))
            {
                control.State.CurrentTextureName = "Control " + control.Name;
            }

            control.Initialize();
        }

        private void ControlLoad(Control control)
        {
            Debug.WriteLine("Loading : " + control.Name);

            if (control.Manager == null)
            {
                control.Manager = this.Manager;
            }

            control.State.SourceRectangle = new Rectangle(
                                                            0, 
                                                            0, 
                                                            (int)control.Config.Width, 
                                                            (int)control.Config.Height);
            control.LoadContent();

            // force this control to redraw itself
            control.Invalidate();
        }

        private void ControlUpdate(GameTime time, ref int updateIndex, Control control)
        {
            CalculateStatePosition(control);
            CaclulateStateSize(control);
            CalculateStateSourceRectangle(control);

            // invalidate the control if config changed
            if (control.Config.Changed)
            {
                control.Invalidate();
            }

            // if we must reload the control , then do it
            if (control.State.MustReload == true)
            {
                control.State.MustReload = false;
                ControlUnload(control);
                control.State.IsLoaded = false;
            }

            // if we must load the control then do it
            if (control.State.IsLoaded == false)
            {
                ControlLoad(control);
                control.State.IsLoaded = true;
            }

            // do the monogame version of update
            control.Update(time);

            // reset changes-flag
            control.Config.ResetChanged();

            // Set update index, for height calculations
            updateIndex++;
            control.State.UpdateIndex = updateIndex;

            // paint the texture again if it changed
            if (control.State.MustPaint)
            {
                ControlPaint(time, control);
            }
        }

        private void ControlPaint(GameTime time, Control control)
        {
            control.State.MustPaint = false;
            control.State.IsRepainted = false;
            control.Paint();
            control.State.IsRepainted = true;
        }

        private void ControlDraw(Control control)
        {
            if (control.Config.DebugLayout)
            {
                
            }
            else
            {
                // if not visible, do nothing
                if (control.State.Visible)
                {
                    control.DrawMyData();
                }
            }
        }

        private void ControlUnload(Control control)
        {
            control.UnloadContent();
        }
        #endregion

        #region Utility
        private static void CalculateStateSourceRectangle(Control control)
        {
            control.State.SourceRectangle = new Rectangle(
                                                0,
                                                0,
                                                (int)control.Config.Width,
                                                (int)control.Config.Height);
        }

        private static void CaclulateStateSize(Control control)
        {
            control.State.Width = control.Config.Width;
            control.State.Height = control.Config.Height;
        }

        private static void CalculateStatePosition(Control control)
        {
            // todo :update the draw.position only when the config position changed
            var parentPosition = new DVector2();
            if (control.Parent != null)
            {
                parentPosition = control.Parent.State.DrawPosition;
            }
            var newPosition = parentPosition + new DVector2(control.Config.PositionX, control.Config.PositionY);
            control.State.DrawPosition = newPosition;
        }
        #endregion
    }
}