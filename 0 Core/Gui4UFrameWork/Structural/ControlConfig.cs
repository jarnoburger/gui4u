// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControlConfig.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   This configuration doesn't change a lot.
//   Configuration is normally set at creation time.
//   Don't expect that ControlConfig will change during runtime.
//   For changes during runtime, use NodeState
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Gui4UFramework.Structural
{
    /// <summary>This configuration doesn't change a lot.
    /// Configuration is normally set at creation time.
    /// Don't expect that ControlConfig will change during runtime.
    /// For changes during runtime, use NodeState.</summary>
    public class ControlConfig
    {
        #region Declare
        /// <summary>
        /// The width to start out with for the control.
        /// </summary>
        protected const int InitialWidth = 140;

        /// <summary>
        /// The height to start out with for the control.
        /// </summary>
        protected const int InitialHeight = 40;

        /// <summary>
        /// When one of the properties has changed.
        /// </summary>
        private bool _changed;

        private bool _sizeChanged;

        private bool _positionChanged;

        /// <summary>
        /// If this control will react to focus changes.
        /// </summary>
        private bool _acceptsFocus;

        /// <summary>
        /// If this control behaves like a window.
        /// </summary>
        private bool _isWindow;

        /// <summary>
        /// If this control is enabled.
        /// </summary>
        private bool _enabled;

        /// <summary>
        /// If we should use different colors when the mouse is hovering over.
        /// </summary>
        private bool _hoverColorsEnabled;

        /// <summary>
        /// The x position for this control , relative to my parent.
        /// </summary>
        private float _positionX;

        /// <summary>
        /// The y position for this control , relative to my parent.
        /// </summary>
        private float _positionY;

        /// <summary>
        /// The width for this control.
        /// </summary>
        private float _width;

        /// <summary>
        /// The height for this control.
        /// </summary>
        private float _height;
        #endregion

        #region Initialize
        /// <summary>
        /// Initializes a new instance of the <see cref="ControlConfig"/> class.
        /// </summary>
        public ControlConfig()
        {
            this._enabled = true;
            this._hoverColorsEnabled = true;
            this._isWindow = false;
            this._acceptsFocus = true;
            this._width = InitialWidth;
            this._height = InitialHeight;
            this._positionX = 0;
            this._positionY = 0;
            this._changed = false;
            this.DebugLayout = false;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether this control will react to focus changes.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [accepts focus]; otherwise, <c>false</c>.
        /// </value>
        public bool AcceptsFocus
        {
            get
            {
                return this._acceptsFocus;
            }

            set
            {
                this._acceptsFocus = value;
                this._changed = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is window If this control behaves like a window.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is window; otherwise, <c>false</c>.
        /// </value>
        public bool IsWindow
        {
            get
            {
                return this._isWindow;
            }

            set
            {
                this._isWindow = value;
                this._changed = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ControlConfig"/> is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        public bool Enabled
        {
            get
            {
                return this._enabled;
            }

            set
            {
                this._enabled = value;
                this._changed = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [hover colors enabled].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [hover colors enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool HoverColorsEnabled
        {
            get
            {
                return this._hoverColorsEnabled;
            }

            set
            {
                this._hoverColorsEnabled = value;
                this._changed = true;
            }
        }

        /// <summary>Gets or sets the location relative to its parent (from left to right). </summary>
        /// <value>The x position of the node.</value>
        public float PositionX
        {
            get
            {
                return this._positionX;
            }

            set
            {
                if (_positionX != value)
                {
                    this._positionX = value;
                    this._positionChanged = true;
                }
            }
        }

        /// <summary>Gets or sets the location relative to its parent {from top to bottom).</summary>
        /// <value>The y position of the node.</value>
        public float PositionY
        {
            get
            {
                return this._positionY;
            }

            set
            {
                if (_positionY != value)
                {
                    this._positionY = value;
                    this._positionChanged = true;
                }
            }
        }

        /// <summary>Gets or sets the width of the control.</summary>
        /// <value>The width of the node.</value>
        public float Width
        {
            get { return this._width; }
            set
            {
                if (_width != value)
                {
                    this._width = value;
                    this._sizeChanged = true;
                }
            }
        }

        public void SetSize(float width, float heigth)
        {
            if (_width != width)
            {
                this._width = width;
                this._sizeChanged = true;
            }

            if (_height != heigth)
            {
                this._height = heigth;
                this._sizeChanged = true;
            }
        }

        /// <summary>Gets or sets the height of the control.</summary>
        /// <value>The height of the node.</value>
        public float Height
        {
            get { return this._height; }
            set
            {
                if (_height != value)
                {
                    this._height = value;
                    this._sizeChanged = true;
                }
            }
        }
        #endregion

        #region Changes
        public bool SizeChanged { get { return this._sizeChanged; } }
        public bool PositionChanged {  get { return this._positionChanged; } }
        /// <summary>
        /// Gets a value indicating whether this config is changed.
        /// This is exclusive size and position, they have their own respective changes
        /// </summary>
        /// <value>
        ///   <c>true</c> if changed; otherwise, <c>false</c>.
        /// </value>
        public bool Changed
        {
            get
            {
                return this._changed;
            }
        }

        /// <summary>Resets the changed property to false.</summary>
        public void ResetChanged()
        {
            this._changed = false;
        }

        public void ResetSizeChanged()
        {
            this._sizeChanged = false;
        }

        public void ResetPositionChanged()
        {
            this._positionChanged = false;
        }
        #endregion

        #region Debug
        /// <summary>
        /// Debugs this instance in the debug-output window.
        /// </summary>
        public void Debug()
        {
            System.Diagnostics.Debug.WriteLine(this.ToString());
        }

        public bool DebugLayout { get; set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return
                $"Config : Pos({this._positionX}, {this._positionY}) Size({this._width}, {this._height}))";
        }
        #endregion

    }
}