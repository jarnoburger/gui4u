// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Control.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   This is the base control, every item extends from this.
//   Has a configuration , and a current state
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using Gui4UFramework.Graphics;
using Gui4UFramework.Management;

namespace Gui4UFramework.Structural
{
    /// <summary>
    /// This is the base control, every item extends from this.
    /// Has a configuration , and a current state.
    /// </summary>
    public class Control : Node
    {
        #region Declare

        #endregion

        #region Initialize
        /// <summary>
        /// Initializes a new instance of the <see cref="Control"/> class.
        /// </summary>
        /// <param name="name">The name to u.</param>
        public Control(string name)
        {
            this.Name = name;
            this.State = new DrawState();
            this.Config = new ControlConfig();
            this.Theme = new Theme();
            this.TooltipText = "No toolkit text is set";
            this.DrawMeAndMyChildren = true;

            this.State.IsLoaded = false;
        }
        #endregion

        #region Properties
        /// <summary>Gets the configuration that is set mostly during initialize.</summary>
        /// <value>The configuration. That contains all the information that can be used by the control to determine what to draw (set in this.State).</value>
        public ControlConfig Config { get; private set; }

        /// <summary>Gets the current state of the control.</summary>
        /// <value>The state , that contains the information to know how to draw on-screen.</value>
        public DrawState State { get; private set; }

        /// <summary>Gets or sets the theme for the control.</summary>
        /// <value>Gets or sets the theme , that defines how the control will look.</value>
        public Theme Theme { get; set; }

        /// <summary>Gets or sets the manager that does the heavy lifting of the nodes.</summary>
        /// <value>The manager, the object that can do stuff for controls.</value>
        public NodeManager Manager { get; set; }

        /// <summary>
        /// Gets or sets the tool-tip text. This is optional and could be used be a optional tool-tip-control to show tips about this control.
        /// </summary>
        /// <value>
        /// The tool-tip text.
        /// </value>
        public string TooltipText { get; set; }

        /// <summary>Gets or sets a value indicating whether [to draw me and my children].
        /// When true, this control and all of its children will be skipped in draw by.</summary>
        /// <value>When true : will draw me and my children. When false : will not draw.</value>
        public bool DrawMeAndMyChildren { get; set; }

        /// <summary>
        /// Gets a value indicating whether children were added.
        /// Will be reset during Control.Update().
        /// </summary>
        /// <value>
        ///   <c>true</c> if [added children]; otherwise, <c>false</c>.
        /// </value>
        public bool AddedChildren { get; private set; }
        #endregion

        #region Main functions
        /// <summary>Use this method to query for any required services, and load any non-graphics resources.
        /// Do not build your control here ! Build it in LoadContent !.</summary>
        public void Initialize() {}

        /// <summary>Called when graphics resources need to be loaded.
        /// Use this for the usage of :
        /// - creation of the internal embedded controls.
        /// - setting of the variables and resources in this control
        /// - to load any game-specific graphics resources
        /// - take over the config width and height and use it into State
        /// - overriding how this item looks like , by settings its texture or theme
        /// Call base.LoadContent before you do your override code, this will cause :
        /// - State.SourceRectangle to be reset to the Config.Size.</summary>
        public virtual void LoadContent() {}

        /// <summary>
        /// Unloads the content.
        /// </summary>
        public virtual void UnloadContent() {}

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public virtual void Update(GameTime gameTime)
        {
            this.AddedChildren = false;
        }

        /// <summary>Draw the texture from CurrentTextureName at DrawPosition combined with its offset.</summary>
        public virtual void DrawMyData()
        {
            this.Manager.ImageCompositor.Place(this.State.CurrentTextureName, this.State);
        }
        #endregion

        #region Paint
        public void Invalidate()
        {
            this.State.MustPaint = true;
            this.State.IsRepainted = false;
        }

        // Do the redraw of the texture.
        public virtual void Paint() {}
        #endregion

        #region Utility
        /// <summary>
        /// Check where given location is relative to me.
        /// </summary>
        /// <param name="location">The location.</param>
        /// <returns>The given point in relation to this control.</returns>
        public DVector2 PointRelative(DVector2 location)
        {
            return location - this.State.DrawPosition;
        }
        #endregion

        #region Debug
        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            var message = $"Name:{this.Name},Config:{this.Config},State:{this.State}";
            return message;
        }

        /// <summary>Writes down the position of this control in debug-output.</summary>
        public void DebugPositions()
        {
            Debug.WriteLine("*** Debugging positions ***");
            this.DebugPositionsRecursive();
            Debug.WriteLine(string.Empty);
        }

        /// <summary>Writes down the position of this control in debug-output, recursively.</summary>
        private void DebugPositionsRecursive()
        {
            var x = this.Config.PositionX;
            var y = this.Config.PositionY;
            var w = this.Config.Width;
            var h = this.Config.Height;

            string p;
            if (this.Parent == null)
            {
                p = "NULL";
            }
            else
            {
                p = Parent.Name;
            }

            var message = $"Name:{Name},X:{x},Y:{y},W:{w},H:{h},P:{p}";

            Debug.WriteLine(message);

            foreach (var child in this.Children)
            {
                var node = child as Control;

                node?.DebugPositionsRecursive();
            }
        }
        #endregion

        #region Events
        /// <summary>When mouse enters this control.</summary>
        public virtual void HoverEnter()
        {
        }

        /// <summary>When mouse leaves this control.</summary>
        public virtual void HoverExit()
        {
        }
        #endregion

        /// <summary>
        /// Centers this control to the parents canvas.
        /// </summary>
        public void CentreToParent()
        {
            var childW = this.Config.Width;
            var childH = this.Config.Height;

            var parent = Parent;
            var parentW = parent.Config.Width;
            var parentH = parent.Config.Height;

            var locx = (parentW / 2) - (childW / 2);
            var locy = (parentH / 2) - (childH / 2);

            this.Config.PositionX = locx;
            this.Config.PositionY = locy;
        }

        /// <summary>
        /// Destroys me.
        /// </summary>
        /// <param name="control">The control.</param>
        public void DestroyMe(Control control)
        {
            for (var i = Children.Count - 1; i >= 0; i--)
            {
                var child = Children[i];
                if (child != control)
                {
                    continue;
                }

                Children.Remove(child);
                var cnt = child as Control;

                cnt?.UnloadContent();
            }
        }

        /// <summary>
        /// Adds the child control to the parent. Passes along the manager and parent props too.
        /// </summary>
        /// <param name="child">The child.</param>
        public void AddControl(Control child)
        {
#if DEBUG
            if (child == null)
            {
                throw new ArgumentNullException(nameof(child));
            }
#endif

            child.Parent = this;
            child.Manager = this.Manager;

            this.Children.Add(child);

            this.AddedChildren = true;
        }
    }
}