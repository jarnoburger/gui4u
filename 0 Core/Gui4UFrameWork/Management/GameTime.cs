﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GameTime.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Defines the GameTime type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;

namespace Gui4UFramework.Management
{
    /// <summary>
    /// The time in a object.
    /// </summary>
    public class GameTime
    {
        /// <summary>
        /// Gets the total game time passed since starting the program.
        /// </summary>
        /// <value>
        /// The total game time.
        /// </value>
        public TimeSpan TotalGameTime { get; private set; }

        /// <summary>
        /// Updates the specified total game time.
        /// </summary>
        /// <param name="totalGameTime">The total game time.</param>
        public void Update(TimeSpan totalGameTime)
        {
            this.TotalGameTime = totalGameTime;
        }
    }

    public class FrameCounter
    {
        #region Properties
        public double TotalFrames { get; private set; }
        public double TotalSeconds { get; private set; }
        public double AverageFramesPerSecond { get; private set; }
        public double CurrentFramesPerSecond { get; private set; }
        #endregion

        #region Declare
        public const int MaximumSamples = 10;

        private readonly Queue<double> _sampleBuffer = new Queue<double>();

        private double _lastTime;

        private int _count;
        #endregion

        #region Functions
        public bool Update(double currentTime)
        {
            _count ++;
            var deltaTime = currentTime - _lastTime;

            CurrentFramesPerSecond = 1.0f / deltaTime;

            _sampleBuffer.Enqueue(CurrentFramesPerSecond);

            if (_sampleBuffer.Count > MaximumSamples)
            {
                _sampleBuffer.Dequeue();
                AverageFramesPerSecond = _sampleBuffer.Average(i => i);
            }
            else
            {
                AverageFramesPerSecond = CurrentFramesPerSecond;
            }

            TotalFrames++;
            TotalSeconds += deltaTime;

            _lastTime = currentTime;
            return true;
        }
        #endregion
    }
}