﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KeySwitches.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Contains the keyboard actions that happened
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Gui4UFramework.Management
{
    /// <summary>Contains the keyboard actions that happened.</summary>
    public class KeySwitches
    {
        /// <summary>There is change in text.</summary>
        private bool _changeText;

        /// <summary>There is selection of all.</summary>
        private bool _selectAll;

        /// <summary>There is a enter pressed.</summary>
        private bool _enterPressed;

        /// <summary>There is a cursor movement to the left.</summary>
        private bool _doCursorLeft;

        /// <summary>There is a cursor movement to the right.</summary>
        private bool _doCursorRight;

        /// <summary>There is a text insertion.</summary>
        private bool _insertText;

        /// <summary>There is a backspace.</summary>
        private bool _backspace;

        /// <summary>There is key delete.</summary>
        private bool _keyDelete;

        /// <summary>The text to insert.</summary>
        private string _textToInsert;

        /// <summary>
        /// Gets or sets a value indicating whether there is [change text].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [change text]; otherwise, <c>false</c>.
        /// </value>
        public bool ChangeText
        {
            get
            {
                return this._changeText;
            }

            set
            {
                this._changeText = value;
                this.Changed = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether there is [[select all].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [select all]; otherwise, <c>false</c>.
        /// </value>
        public bool SelectAll
        {
            get
            {
                return this._selectAll;
            }

            set
            {
                this._selectAll = value;
                this.Changed = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether there is [enter pressed].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [enter pressed]; otherwise, <c>false</c>.
        /// </value>
        public bool EnterPressed
        {
            get
            {
                return this._enterPressed;
            }

            set
            {
                this._enterPressed = value;
                this.Changed = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether there is [do cursor left].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [do cursor left]; otherwise, <c>false</c>.
        /// </value>
        public bool DoCursorLeft
        {
            get
            {
                return this._doCursorLeft;
            }

            set
            {
                this._doCursorLeft = value;
                this.Changed = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether there is [do cursor right].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [do cursor right]; otherwise, <c>false</c>.
        /// </value>
        public bool DoCursorRight
        {
            get
            {
                return this._doCursorRight;
            }

            set
            {
                this._doCursorRight = value;
                this.Changed = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether there is [insert text].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [insert text]; otherwise, <c>false</c>.
        /// </value>
        public bool InsertText
        {
            get
            {
                return this._insertText;
            }

            set
            {
                this._insertText = value;
                this.Changed = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether there is <see cref="KeySwitches"/> is backspace.
        /// </summary>
        /// <value>
        ///   <c>true</c> if backspace; otherwise, <c>false</c>.
        /// </value>
        public bool Backspace
        {
            get
            {
                return this._backspace;
            }

            set
            {
                this._backspace = value;
                this.Changed = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether there is [key delete].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [key delete]; otherwise, <c>false</c>.
        /// </value>
        public bool KeyDelete
        {
            get
            {
                return this._keyDelete;
            }

            set
            {
                this._keyDelete = value;
                this.Changed = true;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="KeySwitches"/> has changed.
        /// </summary>
        /// <value>
        ///   <c>true</c> if changed; otherwise, <c>false</c>.
        /// </value>
        public bool Changed
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the text to insert.
        /// </summary>
        /// <value>
        /// The text to insert.
        /// </value>
        public string TextToInsert
        {
            get
            {
                return this._textToInsert;
            }

            set
            {
                this._textToInsert = value;
                this.Changed = true;
            }
        }

        /// <summary>
        /// Resets this instance.
        /// </summary>
        public void Reset()
        {
            this._changeText = false;
            this._selectAll = false;
            this._enterPressed = false;
            this._doCursorLeft = false;
            this._doCursorRight = false;
            this._insertText = false;
            this._backspace = false;
            this._keyDelete = false;
            this._textToInsert = string.Empty;

            this.Changed = false;
        }
    }
}