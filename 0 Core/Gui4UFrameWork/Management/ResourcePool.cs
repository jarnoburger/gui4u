﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ResourcePool.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   uses the crud method..
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;

namespace Gui4UFramework.Management
{
    /// <summary>uses the crud method..</summary>
    /// <remarks>http://en.wikipedia.org/wiki/Create,_read,_update_and_delete.</remarks>
    /// <typeparam name="T">The resource type contained in this resource-pool.</typeparam>
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1630:DocumentationTextMustContainWhitespace", Justification = "Reviewed. Suppression is OK here.")]
    public abstract class ResourcePool<T> where T : class
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ResourcePool{T}"/> class.
        /// </summary>
        protected ResourcePool()
        {
            this.Resources = new Dictionary<string, T>();
            this.UniqueNameCreator = UniqueNameCreator.CreateInstance();
            this.DebugResources = false;
        }

        /// <summary>
        /// Gets the dictionary that contains the resources.
        /// </summary>
        /// <value>
        /// The resources.
        /// </value>
        private Dictionary<string, T> Resources { get; }

        /// <summary>
        /// Gets the unique name creator.
        /// </summary>
        /// <value>
        /// The unique name creator.
        /// </value>
        protected UniqueNameCreator UniqueNameCreator { get; }

        /// <summary>Creates a resource in the resource pool.
        /// The specified prefferedName is used to make a name that is unique , using the unique-name-creator.
        /// That unique name is used in the resource-pool to know where it is , and that named is returned to the user.</summary>
        /// <param name="preferredName">the preferred name for the new resource.</param>
        /// <param name="value">The value to add to the resources.</param>
        /// <param name="finalName"></param>
        /// <returns>A special unique prefferedName to tell where the object is in the resource pool.</returns>
        public bool Create(string preferredName, T value, out string finalName)
        {
            finalName = this.UniqueNameCreator.GetUniqueName(preferredName);
            this.Resources.Add(finalName, value);

            if (this.DebugResources)
            { 
                var sentence = $"Resource0 created : {finalName} {preferredName} {value}";
                System.Diagnostics.Debug.WriteLine(sentence);
            }

            return true;
        }

        /// <summary>
        /// Updates the specified the object a specified prefferedName-location with the new specified object.
        /// </summary>
        /// <param name="name">The named location in the resource pool.</param>
        /// <param name="newObject">The new object.</param>
        public bool Update(string name, T newObject)
        {
            if (this.Resources.ContainsKey(name) == false)
            {
                return false;
            }

            this.Resources[name] = newObject;

            if (this.DebugResources)
            {
                var sentence = $"Resource0 updated : {name} {newObject}";
                System.Diagnostics.Debug.WriteLine(sentence);
            }

            return true;
        }

        /// <summary>Reads the object at a location with specified name.</summary>
        /// <param name="name">The item to find using its name, in my resource pool.</param>
        /// <param name="value"></param>
        /// <returns>The object at a location with specified name.</returns>
        public bool Read(string name, out T value)
        {
#if DEBUG
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException(nameof(name), "If the prefferedName variable is NULL, then we already know for sure we will NOT get the resource with THAT.");
            }
#endif
            var success = this.Resources.TryGetValue(name, out value);

            if (success)
            {
                return true;
            }

            if (this.DebugResources)
            {
                System.Diagnostics.Debug.WriteLine("Trying to get [" + name + "] but can not find it");
                this.Debug();
                Debugger.Break();
            }

            return false;
        }

        /// <summary>
        /// Deletes the object at a location with specified name.
        /// </summary>
        /// <param name="name">The name of the location where the object is to delete.</param>
        public bool Delete(string name)
        {
            var success = this.Resources.Remove(name);
            if (success == false)
            {
                if (DebugResources)
                {
                    System.Diagnostics.Debug.WriteLine("Could not delete " + name + " in library, cannot find it.");
                    Debugger.Break();
                    this.Debug();
                }

                return false;
            }
            else
            {
                if (DebugResources)
                {
                    System.Diagnostics.Debug.WriteLine("Deleted " + name);
                }

                return true;
            }
        }

        /// <summary>
        /// Determines whether there is a object at the location with [the specified resource name].
        /// </summary>
        /// <param name="resourceName">Name of the resource.</param>
        /// <returns>True if we have the resource, otherwise false.</returns>
        public bool Contains(string resourceName)
        {
            if (string.IsNullOrEmpty(resourceName))
            {
                if (DebugResources)
                {
                    System.Diagnostics.Debug.WriteLine("Given name was null or empty");    
                }

                return false;
            }

            return this.Resources.ContainsKey(resourceName);
        }

        /// <summary>
        /// Debugs this instance. Let this class spit out debug info.
        /// </summary>
        public void Debug()
        {
            System.Diagnostics.Debug.WriteLine("Library for Bitmaps contains : ");
            if (this.Count == 0)
            {
                System.Diagnostics.Debug.WriteLine("Nothing");
                return;
            }

            foreach (var resource in this.Resources)
            {
                var value = resource.Value;
                System.Diagnostics.Debug.WriteLine("resource " + resource.Key + " with " + value);
            }
        }

        public bool DebugResources { get; }

        public int Count { get { return this.Resources.Count; } }

        public List<string> GetResourceList()
        {
            var list = new List<string>();
            foreach (var resource in this.Resources)
            {
                list.Add(resource.Key);
            }
            return list;
        } 
    }
}