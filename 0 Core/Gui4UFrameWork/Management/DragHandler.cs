﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DragHandler.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Defines the DragHandler type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Diagnostics;
using Gui4UFramework.Graphics;
using Gui4UFramework.Structural;

namespace Gui4UFramework.Management
{
    /// <summary>
    /// Remembers when a item gets dragged and moved. And how it moved.
    /// </summary>
    public class DragHandler
    {
        /// <summary>
        /// The control to follow for movements.
        /// </summary>
        private readonly Control _controlToFollow;

        /// <summary>
        /// The manager that can read inputs.
        /// </summary>
        private readonly InputManager _manager;

        public delegate void DebugMessaging(object sender, DebugEventArgs e);

        public event DebugMessaging DebugMessage;

        /// <summary>
        /// Initializes a new instance of the <see cref="DragHandler"/> class.
        /// </summary>
        /// <param name="manager">The manager.</param>
        /// <param name="controlToFollow">The control to follow.</param>
        public DragHandler(InputManager manager, Control controlToFollow)
        {
            this._controlToFollow = controlToFollow;
            this._manager = manager;
        }

        /// <summary>
        /// Gets a value indicating whether [mouse pressed].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [mouse pressed]; otherwise, <c>false</c>.
        /// </value>
        public bool StateMousePressed { get; private set; }

        /// <summary>
        /// Gets a value indicating whether we are [Dragging].
        /// </summary>
        /// <value>
        ///   <c>true</c> if dragging; otherwise, <c>false</c>.
        /// </value>
        public bool Dragging { get; private set; }

        /// <summary>
        /// Gets the location where the mouse started the drag. Relative to the parent.
        /// </summary>
        /// <value>
        /// The started drag location.
        /// </value>
        public DVector2 DragStartLocation { get; private set; }

        /// <summary>
        /// Gets the location where we are currently dragging with the mouse. Relative to the parent.
        /// </summary>
        /// <value>
        /// The current drag location.
        /// </value>
        public DVector2 DragCurrentLocation { get; private set; }

        /// <summary>
        /// Gets the location where we ended dragging with the mouse. Relative to the parent.
        /// </summary>
        /// <value>
        /// The drag end location.
        /// </value>
        public DVector2 DragEndLocation { get; private set; }

        /// <summary>
        /// Gets the amount of movement after starting of the drag.
        /// </summary>
        /// <value>
        /// The drag current delta.
        /// </value>
        public DVector2 DragCurrentDelta
        {
            get
            {
                return this.DragCurrentLocation - this.DragStartLocation;
            }
        }

        /// <summary>
        /// Gets the current mouse location.
        /// </summary>
        /// <value>
        /// The mouse location.
        /// </value>
        public DVector2 MouseLocation { get; private set; }

        /// <summary>
        /// Reads if we are dragging, and react to it if we are.
        /// </summary>
        public void Read()
        {
            // AQCUIRE VALUES : take mouse input
            this.MouseLocation = this._manager.ReadMouseLocation();
            this.StateMousePressed = this._manager.ReadLeftMousePressed();

            // VALIDATE : if we have nothing to follow then return
            if (this._controlToFollow == null)
            {
                return;
            }

            // VALIDATE : if the control is not visible then return
            if (this._controlToFollow.State.Visible == false)
            {
                return;
            }

            // DO LOGIC : if mouse was pressed on top of the control and we were not dragging yet.. then we have started dragging
            if (this.ControlMousedOver() && this.StateMousePressed && this.Dragging == false)
            {
                this.DraggingStart();
            }

            // DO LOGIC : if mouse was released and we are still dragging, then we stop dragging
            if (!this.StateMousePressed && this.Dragging)
            {
                this.DraggingEnd();
            }

            // DO LOGIC : if we are dragging , the update current location
            if (this.Dragging)
            {
                this.DraggingUpdate();
            }
        }

        /// <summary>
        /// Gets the relative mouse location to the parent OR the window.
        /// </summary>
        /// <returns>The relative location.</returns>
        private DVector2 GetMousePosition()
        {
            return this._manager.ReadMouseLocation();
        }

        /// <summary>
        /// Checks if the moused is over this control.
        /// </summary>
        /// <returns>True when mouse is over.</returns>
        private bool ControlMousedOver()
        {
            return this._controlToFollow.State.MouseHoveringOver;
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return
                $"Mouse:{this.MouseLocation},{this.StateMousePressed} Dragging:{this.Dragging} Start:{this.DragStartLocation} Current:{this.DragCurrentLocation} End:{this.DragEndLocation} Delta:{this.DragCurrentDelta}";
        }

        /// <summary>
        /// Starts the dragging.
        /// </summary>
        private void DraggingStart()
        {
            // validate new data
            var location = this.GetMousePosition();
            if (location == this.DragStartLocation)
            {
                return;
            }

            // we are now dragging , and know where we started
            this.DragStartLocation = location;
            this.Dragging = true;
            this.ControlStartStateLocation = this._controlToFollow.State.DrawPosition;
            this.ControlStartConfigLocation = new DVector2(
                                                            this._controlToFollow.Config.PositionX,
                                                            this._controlToFollow.Config.PositionY);

            // debug
            RaiseDebug("Start dragging at " + this.DragStartLocation);
        }

        /// <summary>
        /// Updates the dragging.
        /// </summary>
        private void DraggingUpdate()
        {
            // validate new data
            var mousePosition = this.GetMousePosition();
            if (mousePosition == this.DragCurrentLocation)
            {
                return;
            }

            // we know where the new location is
            this.DragCurrentLocation = mousePosition;           
            this.ControlCurrentLocation = this.ControlStartStateLocation + this.DragCurrentDelta;

            // debug
            RaiseDebug("Currently dragging at " + this.DragCurrentLocation);
        }

        /// <summary>
        /// End the dragging.
        /// </summary>
        private void DraggingEnd()
        {
            this.DragEndLocation = this.GetMousePosition();
            this.DragStartLocation = new DVector2();
            RaiseDebug("Ended dragging at " +this.DragEndLocation);

            this.Dragging = false;
        }

        private void RaiseDebug(string message)
        {
            Debug.WriteLine(this._controlToFollow.Name + ">>> " + message);

            DebugMessage?.Invoke(this, new DebugEventArgs(message));
        }

        public DVector2 ControlStartStateLocation;

        public DVector2 ControlStartConfigLocation;

        public DVector2 ControlCurrentLocation;
    }
}