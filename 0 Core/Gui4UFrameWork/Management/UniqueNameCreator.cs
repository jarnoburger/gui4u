﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UniqueNameCreator.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Creates unique names.. It will add a number to given name , if name is already used.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;

namespace Gui4UFramework.Management
{
    /// <summary>Creates unique names.. It will add a number to given name, if name is already used.</summary>
    /// <remarks>This uses the singleton pattern. http://en.wikipedia.org/wiki/Singleton_pattern .</remarks>
    public class UniqueNameCreator
    {
        /// <summary>The private static instance (like specified in the singleton pattern).</summary>
        private static UniqueNameCreator _instance;

        /// <summary>The names that were already used / created.</summary>
        private readonly List<string> _names;

        /// <summary>
        /// The counter is incremented every that there is a new object..
        /// This counter can be added to a string to make the string unique.
        /// </summary>
        private static int _counter;

        /// <summary>
        /// Prevents a default instance of the <see cref="UniqueNameCreator"/> class from being created.
        /// </summary>
        private UniqueNameCreator()
        {
            this._names = new List<string>();
        }

        /// <summary>Creates the instance using the singleton pattern.</summary>
        /// <returns>A singleton instance of the unique name creator.</returns>
        public static UniqueNameCreator CreateInstance()
        {
            if (_instance == null)
            {
                _instance = new UniqueNameCreator();
            }

            return _instance;
        }

        /// <summary>Creates a unique name , using preferred name.</summary>
        /// <param name="preferredName">Name of the preferred.</param>
        /// <returns>A unique name.</returns>
        public string GetUniqueName(string preferredName)
        {
            var name = preferredName;

            // check if the name is in our dictionary.
            // if it is , get a counter that is higher then the current one.
            if (this._names.Contains(preferredName))
            {
                _counter++;
                name = preferredName + " " + _counter;
            }

            this._names.Add(name);
            return name;
        }
    }
}