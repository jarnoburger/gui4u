﻿namespace Gui4UFramework.Management
{
    public class DebugEventArgs : System.EventArgs
    {
        public DebugEventArgs(string message)
        {
            Message = message;
        }

        public string Message { get; set; }
    }
}