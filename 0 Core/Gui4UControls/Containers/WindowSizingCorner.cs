﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WindowSizingCorner.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Defines the WindowSizingCorner type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Diagnostics;
using Gui4UFramework.Graphics;
using Gui4UFramework.Structural;

namespace GUI4UControls.Containers
{
    /// <summary>
    /// Shows a sizing button on the bottom right , to reshape a window in size.
    /// </summary>
    public class WindowSizingCorner : Control
    {
        /// <summary>
        /// The texture size of the control in the corner.
        /// </summary>
        private DVector2 _textureSize;

        /// <summary>
        /// Initializes a new instance of the <see cref="WindowSizingCorner"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public WindowSizingCorner(string name) : base(name)
        {
        }

        /// <summary>
        /// Called when graphics resources need to be loaded.
        /// Use this for the usage of :
        /// - creation of the internal embedded controls.
        /// - setting of the variables and resources in this control
        /// - to load any game-specific graphics resources
        /// - take over the config width and height and use it into State
        /// - overriding how this item looks like , by settings its texture or theme
        /// Call base.LoadContent before you do your override code, this will cause :
        /// - State.SourceRectangle to be reset to the Config.Size
        /// </summary>
        public override void LoadContent()
        {
            string finalName;
            bool success;
            success = this.Manager.ImageCompositor.CreateImageTexture(this.Name + "-Sizer", "Textures\\resizeicon", out finalName);
            Debug.Assert(success);
            this.State.CurrentTextureName = finalName;

            this._textureSize = this.Manager.ImageCompositor.ReadSizeTexture(this.State.CurrentTextureName);
            this.Config.Width = this._textureSize.X;
            this.Config.Height = this._textureSize.Y;

            // do the basic stuff
            base.LoadContent();
        }
    }
}