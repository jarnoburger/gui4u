﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WindowTitleBar.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Defines the WindowTitleBar type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Diagnostics;
using Gui4UFramework.Management;
using Gui4UFramework.Structural;

namespace GUI4UControls.Containers
{
    /// <summary>
    /// The title-bar is shown on the top of a window.
    /// It has a Title and a close-button and should also have a Icon.
    /// </summary>
    public class WindowTitleBar : Control
    {
        #region Initialize
        /// <summary>
        /// Initializes a new instance of the <see cref="WindowTitleBar"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public WindowTitleBar(string name) : base(name)
        {
        }
        #endregion

        /// <summary>
        /// Called when graphics resources need to be loaded.
        /// Use this for the usage of :
        /// - creation of the internal embedded controls.
        /// - setting of the variables and resources in this control
        /// - to load any game-specific graphics resources
        /// - take over the config width and height and use it into State
        /// - overriding how this item looks like , by settings its texture or theme
        /// Call base.LoadContent before you do your override code, this will cause :
        /// - State.SourceRectangle to be reset to the Config.Size
        /// </summary>
        public override void LoadContent()
        {
            // do the basic stuff
            base.LoadContent();

            // make the background
            bool success;
            string titleTexture = null;
            success = this.Manager.ImageCompositor.CreateRectangleTexture(this.Name,(int)this.Config.Width,(int)this.Config.Height,1,this.Theme.ContainerFillColor,this.Theme.BorderColor, out titleTexture);
            Debug.Assert(success);
            this.State.CurrentTextureName = titleTexture;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (Config.SizeChanged)
            {
                // change size for my texture
                Manager.ImageCompositor.Delete(this.State.CurrentTextureName);
                string titleTexture;
                bool success;
                success = this.Manager.ImageCompositor.CreateRectangleTexture(this.Name,(int)this.Config.Width,(int)this.Config.Height,1,this.Theme.ContainerFillColor,this.Theme.BorderColor, out titleTexture);
                Debug.Assert(success);
                this.State.CurrentTextureName = titleTexture;
            }
            
            Config.ResetSizeChanged();
        }
    }
}