﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Window.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Window with sub-controls.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using Gui4UFramework.EventArgs;
using Gui4UFramework.Graphics;
using Gui4UFramework.Management;
using Gui4UFramework.Structural;
using GUI4UControls.Buttons;
using GUI4UControls.Menu;
using GUI4UControls.Text;

namespace GUI4UControls.Containers
{
    /// <summary>
    /// Window with sub-controls.
    /// </summary>
    public class Window : Control
    {
        #region Declare
        /// <summary>
        /// Occurs when gets shown
        /// </summary>
        public event EventHandler OnWindowShow;

        /// <summary>
        /// Occurs when gets hidden
        /// </summary>
        public event EventHandler OnWindowHide;

        private DVector2 _startedDragLocation;
        #endregion

        #region Initialize
        /// <summary>
        /// Initializes a new instance of the <see cref="Window"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public Window(string name) : base(name)
        {
            this.Title = name;

            this._startedDragLocation = new DVector2();

            this.Config.IsWindow = true;

            this.Config.Width = Theme.ControlWidth * 5;
            this.Config.Height = Theme.ControlHeight * 5;

            this.ChildWindows = new Dictionary<string, Window>();

            this.MenuBarHorizontal = new MenuBarHorizontal(this.Name + "-Menu Bar")
            {
                DebugLayout = false,
                ConfigAlwaysExpanded = true
            };

            this.ShowMenuBar = false;
        }
        #endregion

        #region Control
        /// <summary>
        /// Gets or sets the title bar control.
        /// </summary>
        /// <value>
        /// The title bar.
        /// </value>
        protected WindowTitleBar TitleBar { get; set; }

        /// <summary>
        /// Gets or sets the close button. The close-button should close the window.
        /// </summary>
        /// <value>
        /// The close button.
        /// </value>
        protected Button CloseButton { get; set; }

        /// <summary>
        /// Gets or sets the text label in the title bar.
        /// </summary>
        /// <value>
        /// The text label.
        /// </value>
        protected Label TextLabel { get; set; }

        /// <summary>
        /// Gets or sets the sizing corner control
        /// </summary>
        /// <value>
        /// The sizing corner.
        /// </value>
        protected WindowSizingCorner SizingCorner { get; set; }

        /// <summary>
        /// Gets or sets the menu bar control
        /// </summary>
        /// <value>
        /// The menu bar horizontal.
        /// </value>
        protected MenuBarHorizontal MenuBarHorizontal { get; set; }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether we show a menu bar or not.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [show menu bar]; otherwise, <c>false</c>.
        /// </value>
        public bool ShowMenuBar { get; set; }

        /// <summary>
        /// Gets or sets the title for the window.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Window"/> is shown.
        /// </summary>
        /// <value>
        ///   <c>true</c> if shown; otherwise, <c>false</c>.
        /// </value>
        public bool Shown { get; protected set; }

        /// <summary>
        /// Gets the windows that are children of me..
        /// </summary>
        /// <value>
        /// The child windows.
        /// </value>
        public Dictionary<string, Window> ChildWindows { get; private set; }

        /// <summary>
        /// Gets the window drag locater.
        /// </summary>
        /// <value>
        /// The window drag locater.
        /// </value>
        public DragHandler WindowDragHandler { get; private set; }

        /// <summary>
        /// Gets the windows size-control drag locater.
        /// </summary>
        /// <value>
        /// The size-control drag locater.
        /// </value>
        public DragHandler SizeControlDragHandler { get; private set; }
        #endregion

        /// <summary>
        /// Called when graphics resources need to be loaded.
        /// Use this for the usage of :
        /// - creation of the internal embedded controls.
        /// - setting of the variables and resources in this control
        /// - to load any game-specific graphics resources
        /// - take over the config width and height and use it into State
        /// - overriding how this item looks like , by settings its texture or theme
        /// Call base.LoadContent before you do your override code, this will cause :
        /// - State.SourceRectangle to be reset to the Config.Size
        /// </summary>
        public override void LoadContent()
        {
            // **** do the basic stuff *****************************************************************
            base.LoadContent();

            // set the visual state using the current configuration *****************************************************************
            this.State.DrawPosition = new DVector2(this.Config.PositionX, this.Config.PositionY);
            this.State.Width = this.Config.Width;
            this.State.Height = this.Config.Height;

            // make the background *****************************************************************
            string finalName;
            bool success;
            success = this.Manager.ImageCompositor.CreateRectangleTexture(
                                                    this.Name,
                                                    (int)this.State.Width,
                                                    (int)this.State.Height,
                                                    this.Theme.BorderWidth,
                                                    this.Theme.WindowFillColor,
                                                    this.Theme.BorderColor, out finalName);
            Debug.Assert(success);
            this.State.CurrentTextureName = finalName;

            // make the title bar *****************************************************************
            this.TitleBar = new WindowTitleBar(this.Name + "-TitleBar");
            this.AddControl(this.TitleBar);

            // make the sizer *****************************************************************
            this.SizingCorner = new WindowSizingCorner(this.Name + "-Sizer");
            this.AddControl(this.SizingCorner);

            // make the close button *****************************************************************
            this.CloseButton = new Button(this.Name + "-CloseButton") {Text = "X",};
            this.AddControl(this.CloseButton);

            // make the title bar *****************************************************************
            this.TextLabel = new Label(this.Name + "-Title"){ConfigText = this.Title,};
            this.AddControl(this.TextLabel);

            // make the menu bar *****************************************************************
            if (this.ShowMenuBar) this.AddControl(this.MenuBarHorizontal);

            // add events *****************************************************************
            this.CloseButton.Clicked += this.OnCloseClicked;

            // make the window drag handler *****************************************************************
            this.WindowDragHandler = new DragHandler(this.Manager.InputManager, this.TitleBar);

            // make the size drag handler *****************************************************************
            this.SizeControlDragHandler = new DragHandler(this.Manager.InputManager, this.SizingCorner);
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // do the basic stuff first
            base.Update(gameTime);

            // read window dragging
            ReadWindowDragging();

            // read sizer dragging
            ReadSizerDragging();

            // Update the title
            if (this.TextLabel.ConfigText != this.Title)
            {
                this.TextLabel.ConfigText = this.Title;
            }

            // check if settings changed in config
            if (Config.Changed)
            {

            }

            // check if size changed in config
            if (Config.SizeChanged)
            {
                // change the close button location
                this.CloseButton.Config.Width = this.Theme.ControlHeight;
                this.CloseButton.Config.Height = this.Theme.ControlHeight;
                this.CloseButton.Config.PositionX = this.Config.Width - this.CloseButton.Config.Width;
                this.CloseButton.Config.PositionY = -this.Theme.ControlHeight;

                // change size for the title-bar
                this.TitleBar.Config.PositionX = 0;
                this.TitleBar.Config.PositionY = -this.Theme.ControlHeight;
                this.TitleBar.Config.Width = this.Config.Width - this.CloseButton.Config.Width;
                this.TitleBar.Config.Height = this.Theme.ControlHeight;
                
                // set the text label
                this.TextLabel.Config.PositionX = 0;
                this.TextLabel.Config.PositionY = -this.Theme.ControlHeight;
                this.TextLabel.Config.Width = this.TitleBar.Config.Width;
                this.TextLabel.Config.Height = this.TitleBar.Config.Height;

                // change the location for the sizing corner
                this.SizingCorner.Config.PositionX = this.Config.Width - 16;
                this.SizingCorner.Config.PositionY = this.Config.Height - 16;

                // change the location/size for the menu-bar
                this.MenuBarHorizontal.WindowModus = true;
                this.MenuBarHorizontal.Config.PositionX = 1;
                this.MenuBarHorizontal.Config.PositionY = 0;
                this.MenuBarHorizontal.Config.Width = this.TitleBar.Config.Width;
                this.MenuBarHorizontal.Config.Height = this.TitleBar.Config.Height;

                // change size for my texture
                Manager.ImageCompositor.Delete(this.State.CurrentTextureName);
                string finalName;
                bool success;
                success = Manager.ImageCompositor.CreateRectangleTexture(
                                        this.Name,
                                        (int)State.Width,
                                        (int)State.Height,
                                        Theme.BorderWidth,
                                        Theme.WindowFillColor,
                                        Theme.BorderColor, out finalName);
                Debug.Assert(success);
                this.State.CurrentTextureName = finalName;
            }

            if (Config.PositionChanged)
            {
                if (Config.PositionX < 0)
                {
                    Config.PositionX = 0;
                }

                if (Config.PositionY < 0 + this.TitleBar.Config.Height)
                {
                    Config.PositionY = 0 + this.TitleBar.Config.Height;
                }

                var size = this.Manager.ImageCompositor.GetScreenSize();

                if (Config.PositionX + Config.Width + 16 > size.X)
                {
                    Config.PositionX = size.X - Config.Width - 16;
                }

                if (Config.PositionY + Config.Height + this.TitleBar.Config.Height + 16> size.Y)
                {
                    Config.PositionY = size.Y - Config.Height - TitleBar.Config.Height - 16;
                }
            }

            Config.ResetChanged();
            Config.ResetSizeChanged();
            Config.ResetPositionChanged();
        }

        private void ReadSizerDragging()
        {
            this.SizeControlDragHandler.Read();
            var dragging = this.SizeControlDragHandler.Dragging;

            if (dragging)
            {
                
                var posx = this.SizeControlDragHandler.ControlCurrentLocation.X;
                var posy = this.SizeControlDragHandler.ControlCurrentLocation.Y;

                var relativeToWindowX = posx - this.Config.PositionX + 16;
                var relativeToWindowY = posy - this.Config.PositionY + 16;

                this.Title = "Size : " + relativeToWindowX + " " + relativeToWindowX;

                this.Config.SetSize(relativeToWindowX, relativeToWindowY);
            }
        }

        private void ReadWindowDragging()
        {
            this.WindowDragHandler.Read();

            var dragging = this.WindowDragHandler.Dragging;
            
            if (dragging)
            {
                this.Title = "Pos : " + this.WindowDragHandler.ControlCurrentLocation.ToString();
                this.Config.PositionX = this.WindowDragHandler.ControlCurrentLocation.X;
                this.Config.PositionY = this.WindowDragHandler.ControlCurrentLocation.Y + this.TitleBar.Config.Height;
            }
        }

        /// <summary>
        /// Draw the texture at DrawPosition combined with its offset
        /// </summary>
        public override void DrawMyData()
        {
            if (this.State.Visible == false)
            {
                return;
            }

            base.DrawMyData();
        }

        /// <summary>
        /// Called when we clicked the close button.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="GameTimeEventArgs"/> instance containing the event data.</param>
        private void OnCloseClicked(object sender, GameTimeEventArgs e)
        {
            if (this.Parent == null)
            {
                Debug.WriteLine("Close clicked for a window where parent was null.");
                return;
            }

            this.Parent.DestroyMe(this);
        }

        /// <summary>
        /// Creates a default menu to test the menu bar.
        /// </summary>
        public void CreateDefaultMenu()
        {
            this.ShowMenuBar = true;

            var file = this.MenuBarHorizontal.AddGroup("File", "File");
            var edit = this.MenuBarHorizontal.AddGroup("Edit", "Edit");
            var view = this.MenuBarHorizontal.AddGroup("View", "View");
            var project = this.MenuBarHorizontal.AddGroup("Project", "Project");
            var tools = this.MenuBarHorizontal.AddGroup("Tools", "Tools");
            var window = this.MenuBarHorizontal.AddGroup("Window", "Window");
            var help = this.MenuBarHorizontal.AddGroup("Help", "Help");

            //// the file menu
            file.AddButton("New", "New");
            file.AddButton("Open", "Open");
            file.AddSeparator("Seperator1");
            file.AddButton("Close", "Close");

            // the edit menu
            edit.AddButton("Undo", "Undo");
            edit.AddButton("Redo", "Redo");
            edit.AddSeparator("Seperator2");
            edit.AddButton("Cut", "Cut");
            edit.AddButton("Copy", "Cut");
            edit.AddButton("Paste", "Paste");

            // the view menu
            view.AddButton("PropWindow", "Property window");

            // the project menu
            project.AddButton("AddItem", "Add item");
            project.AddButton("ProjectProperties", "Project properties");

            // the tools menu
            tools.AddButton("Options", "Options");

            // window menu
            window.AddButton("CloseAll", "Close all");

            // help menu
            help.AddButton("ViewHelp", "View help");
            help.AddButton("About", "About");

            this.ConnectEventToButtons(this);
        }

        /// <summary>
        /// Utility function to create a default menu.
        /// </summary>
        /// <param name="node">The node.</param>
        private void ConnectEventToButtons(Node node)
        {
            var btn = node as MenuItemButton;
            if (btn != null)
            {
                btn.MouseClicked += this.OnButtonClicked;
            }

            foreach (var child in node.Children)
            {
                this.ConnectEventToButtons(child);
            }
        }

        /// <summary>
        /// Called when a button in the menu bar is clicked.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OnButtonClicked(object sender, EventArgs e)
        {
            var btn = sender as MenuItemButton;
            if (btn == null)
            {
                return;
            }

            Debug.WriteLine(btn);
        }

        /// <summary>
        /// Gets the window area where controls are drawn.
        /// </summary>
        /// <returns>The area where controls are drawn.</returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        protected Rectangle GetWindowArea()
        {
            var x = 0;
            var y = this.TitleBar.Config.Height;
            var width = this.Config.Width;
            var height = this.Config.Height - y;

            return new Rectangle(x, y, width, height);
        }
    }
}