﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TabContainer.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Defines the TabContainer type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Diagnostics;
using Gui4UFramework.Structural;

namespace GUI4UControls.Containers
{
    /// <summary>
    /// Is the container where the controls of a tab will be.
    /// </summary>
    public class TabContainer : Control
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TabContainer"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public TabContainer(string name) : base(name)
        {
        }

        /// <summary>
        /// Called when graphics resources need to be loaded.
        ///
        /// Use this for the usage of :
        /// - creation of the internal embedded controls.
        /// - setting of the variables and resources in this control
        /// - to load any game-specific graphics resources
        /// - take over the config width and height and use it into State
        /// - overriding how this item looks like , by settings its texture or theme
        ///
        /// Call base.LoadContent before you do your override code, this will cause :
        /// - State.SourceRectangle to be reset to the Config.Size
        /// </summary>
        public override void LoadContent()
        {
            base.LoadContent();

            string finalName;
            bool success;
            success = Manager.ImageCompositor.CreateRectangleTexture(
                                                                                    this.Name + "-background",
                                                                                    (int)Config.Width,
                                                                                    (int)Config.Height,
                                                                                    1,
                                                                                    Theme.ContainerFillColor,
                                                                                    Theme.BorderColor, out finalName);
            Debug.Assert(success);
            this.State.CurrentTextureName = finalName;
        }
    }
}