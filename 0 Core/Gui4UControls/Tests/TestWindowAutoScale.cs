﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TestWindowAutoScale.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   The test window auto scale.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.ObjectModel;
using Gui4UFramework.Layout;
using Gui4UFramework.Management;
using Gui4UFramework.Structural;
using GUI4UControls.Buttons;
using GUI4UControls.Containers;

namespace GUI4UControls.Tests
{
    /// <summary>
    /// The test window auto scale.
    /// </summary>
    public class TestWindowAutoScale : Window
    {
        /// <summary>
        /// The class that takes care of the scaling of the window.
        /// </summary>
        /// <value>
        /// The automatic scaler.
        /// </value>
        private ControlAutoScaling _controlAutoScaling;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestWindowAutoScale"/> class.
        /// Initializes a new instance of the <see cref="TestWindowBase"/> class.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        public TestWindowAutoScale(string name) : base(name)
        {
            this.Title = " Auto scale";
            this.Buttons = new Collection<Button>();
        }

        /// <summary>
        /// Gets or sets the buttons.The buttons that trigger auto scaling.
        /// </summary>
        /// <value>
        /// The buttons.
        /// </value>
        public Collection<Button> Buttons { get; private set; }

        /// <summary>
        /// Called when graphics resources need to be loaded.
        /// Use this for the usage of :
        /// - creation of the internal embedded controls.
        /// - setting of the variables and resources in this control
        /// - to load any game-specific graphics resources
        /// - take over the config width and height and use it into State
        /// - overriding how this item looks like , by settings its texture or theme
        /// Call base.LoadContent before you do your override code, this will cause :
        /// - State.SourceRectangle to be reset to the Config.Size
        /// </summary>
        public override void LoadContent()
        {
            base.LoadContent();

            this._controlAutoScaling = new ControlAutoScaling();

            var totalCount = 30;
            for (int i = 0; i < totalCount; i++)
            {
                var btn = new Button("Button " + i);
                btn.Text = i.ToString();

                this.AddControl(btn);
                this.Buttons.Add(btn);
                this._controlAutoScaling.Add(btn);
            }
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            var random = new Random();

            foreach (var child in this.Buttons)
            {
                var control = child as Control;
                if (control == null)
                {
                    continue;
                }

                control.Config.PositionX = random.Next(300);
                control.Config.PositionY = this.TitleBar.Config.Height + random.Next(240);
            }

            this._controlAutoScaling.Calculate(this);
        }
    }
}