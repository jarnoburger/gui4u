﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TestWindowHorizontalSpacing.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   The test window for testing out horizontal spacing.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.ObjectModel;
using Gui4UFramework.Graphics;
using Gui4UFramework.Layout;
using Gui4UFramework.Layout.Enums;
using Gui4UFramework.Management;
using GUI4UControls.Buttons;
using GUI4UControls.Containers;

namespace GUI4UControls.Tests
{
    /// <summary>The test window for testing out horizontal spacing.</summary>
    public class TestWindowHorizontalSpacing : Window
    {
        /// <summary>
        /// The is a flag to be used to check if we already switched to a other spacing option
        /// </summary>
        private bool _otherSpacingOptionChosen;

        /// <summary>
        /// Spaces out given buttons equally horizontal
        /// </summary>
        private ControlSpacerHorizontal _controlSpacerHorizontal;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestWindowHorizontalSpacing" /> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// 87543`
        public TestWindowHorizontalSpacing(string name) : base(name)
        {
            this.Title = "Test window auto horizontal spacing.";
            this.Buttons = new Collection<Button>();
        }

        /// <summary>
        /// Gets or sets the buttons.The buttons that trigger auto scaling.
        /// </summary>
        /// <value>
        /// The buttons.
        /// </value>
        public Collection<Button> Buttons { get; private set; }

        /// <summary>
        /// Called when graphics resources need to be loaded.
        /// Use this for the usage of :
        /// - creation of the internal embedded controls.
        /// - setting of the variables and resources in this control
        /// - to load any game-specific graphics resources
        /// - take over the config width and height and use it into State
        /// - overriding how this item looks like , by settings its texture or theme
        /// Call base.LoadContent before you do your override code, this will cause :
        /// - State.SourceRectangle to be reset to the Config.Size
        /// </summary>
        public override void LoadContent()
        {
            base.LoadContent();

            this._controlSpacerHorizontal = new ControlSpacerHorizontal();
            this._controlSpacerHorizontal.Spacing = HorizontalSpacing.Evenly;

            const int totalCount = 4;
            for (var i = 0; i < totalCount; i++)
            {
                var btn = new Button("Button " + i)
                {
                    Text = i.ToString(),
                    Config = { PositionY = 40 }
                };
                this.AddControl(btn);
                this.Buttons.Add(btn);
                this._controlSpacerHorizontal.AddControl(btn);
            }
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
#if DEBUG
            if (gameTime == null)
            {
                throw new ArgumentNullException(nameof(gameTime));
            }
#endif

            base.Update(gameTime);

            var elapsed = gameTime.TotalGameTime.TotalMilliseconds;
            var mod = elapsed % 1000;
            if (mod < 500)
            {
                this.Tick();
            }
            else
            {
                this.Tack();
            }
        }

        private void Tick()
        {
            // check if the do-once flag is set
            if (this._otherSpacingOptionChosen == true)
            {
                return;
            }

            // and do stuff
            switch (this._controlSpacerHorizontal.Spacing)
            {
                case HorizontalSpacing.Centered:
                    this._controlSpacerHorizontal.Spacing = HorizontalSpacing.Evenly;
                    break;

                case HorizontalSpacing.Evenly:
                    this._controlSpacerHorizontal.Spacing = HorizontalSpacing.LeftSide;
                    break;

                case HorizontalSpacing.LeftSide:
                    this._controlSpacerHorizontal.Spacing = HorizontalSpacing.RightSide;
                    break;

                case HorizontalSpacing.RightSide:
                    this._controlSpacerHorizontal.Spacing = HorizontalSpacing.Centered;
                    break;
            }

            var rect = new Rectangle(0, 0, this.Config.Width, this.Config.Height);
            this._controlSpacerHorizontal.Calculate(rect);

            this.Title = this._controlSpacerHorizontal.Spacing.ToString();

            // set the do-once flag
            this._otherSpacingOptionChosen = true;
        }

        private void Tack()
        {
            // reset the do-once flag
            this._otherSpacingOptionChosen = false;
        }
    }
}