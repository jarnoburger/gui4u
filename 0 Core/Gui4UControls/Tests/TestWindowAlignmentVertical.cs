﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TestWindowAlignmentVertical.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Defines the TestWindowAlignmentVertical type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.ObjectModel;
using Gui4UFramework.Graphics;
using Gui4UFramework.Layout;
using Gui4UFramework.Layout.Enums;
using Gui4UFramework.Management;
using GUI4UControls.Buttons;
using GUI4UControls.Containers;

namespace GUI4UControls.Tests
{
    /// <summary>
    /// Is a window that contains a test to see if ControlAlignerVertical works correctly.
    /// </summary>
    public class TestWindowAlignmentVertical : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestWindowAlignmentVertical"/> class.
        /// Initializes a new instance of the <see cref="TestWindowBase"/> class.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        public TestWindowAlignmentVertical(string name) : base(name)
        {
            this.Title = "Test alignment vertical";
            this.Config.Width = (Theme.ControlWidth * 5) + (Theme.ControlSmallSpacing * 6);
            this.Buttons = new Collection<Button>();
            this.Aligner = new ControlAlignerVertical();
        }

        /// <summary>
        /// Gets or sets the aligner that will do the main work.
        /// </summary>
        /// <value>
        /// The aligner.
        /// </value>
        protected ControlAlignerVertical Aligner { get; set; }

        /// <summary>
        /// Gets or sets the buttons.
        /// </summary>
        /// <value>
        /// The buttons.
        /// </value>
        protected Collection<Button> Buttons { get; private set; }

        /// <summary>
        /// Called when graphics resources need to be loaded.
        /// Use this for the usage of :
        /// - creation of the internal embedded controls.
        /// - setting of the variables and resources in this control
        /// - to load any game-specific graphics resources
        /// - take over the config width and height and use it into State
        /// - overriding how this item looks like , by settings its texture or theme
        /// Call base.LoadContent before you do your override code, this will cause :
        /// - State.SourceRectangle to be reset to the Config.Size
        /// </summary>
        public override void LoadContent()
        {
            base.LoadContent();

            // Set the number of buttons to make
            const int maxCountX = 5;

            // set the counter , to be used for naming the button
            var counter = 0;

            for (var indexX = 0; indexX < maxCountX; indexX++)
            {
                var btn = new Button("Button " + counter);
                btn.Config.PositionX = (indexX * (Theme.ControlWidth + Theme.ControlSmallSpacing)) + Theme.ControlSmallSpacing;

                // increment the counter
                counter++;

                this.Aligner.AddControl(btn);
                this.Buttons.Add(btn);
                this.AddControl(btn);
            }

            this.Aligner.Alignment = VerticalAlignment.Center;
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
#if DEBUG
            if (gameTime == null)
            {
                throw new ArgumentNullException(nameof(gameTime));
            }
#endif

            base.Update(gameTime);

            var elapsed = gameTime.TotalGameTime.TotalMilliseconds;
            var mod = elapsed % 1000;
            if (mod < 500)
            {
                this.Tick();
            }
            else
            {
                this.Tack();
            }
        }

        /// <summary>
        /// Does the tick.
        /// </summary>
        private void Tick()
        {
            // crat the strngth onto the y position could diverge
            const float scaleY = 40f;

            // create a random number generator , used for randomly placing the buttons in the y-direction
            var random = new Random();

            // randomize positions
            foreach (var btn in this.Buttons)
            {
                var unitShift = (random.Next(100) * 0.01f) + 0.5f;

                btn.Config.PositionY = scaleY + (unitShift * scaleY) - (btn.Config.Height / 2);
            }
        }

        /// <summary>
        /// Does the tack.
        /// </summary>
        private void Tack()
        {
            // align positions
            var rect = new Rectangle(0, 0, this.Config.Width, this.Config.Height);
            this.Aligner.Calculate(rect);
        }
    }
}