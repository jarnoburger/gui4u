﻿using Gui4UFramework.EventArgs;
using GUI4UControls.Containers;

namespace GUI4UControls.Tests
{
    /// <summary>
    /// Creates a window to test Button-Controls
    /// </summary>
    public class TestWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestWindow"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public TestWindow(string name)
            : base(name)
        {
            this.Config.Width = Theme.ControlWidth + (2 * Theme.ControlLargeSpacing);
        }

        /// <summary>
        /// Called when graphics resources need to be loaded.
        /// Use this for the usage of :
        /// - creation of the internal embedded controls.
        /// - setting of the variables and resources in this control
        /// - to load any game-specific graphics resources
        /// - take over the config width and height and use it into State
        /// - overriding how this item looks like , by settings its texture or theme
        /// Call base.LoadContent before you do your override code, this will cause :
        /// - State.SourceRectangle to be reset to the Config.Size
        /// </summary>
        public override void LoadContent()
        {
            base.LoadContent();

            var posy = Theme.ControlLargeSpacing;
        }

        /// <summary>
        /// Called when the toggle-button is pressed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="buttonStateEventArgs">The <see cref="ButtonStateEventArgs"/> instance containing the event data.</param>
        private void OnTogglePress(object sender, ButtonStateEventArgs buttonStateEventArgs)
        {
            var state = buttonStateEventArgs.ButtonState;
        }
    }
}