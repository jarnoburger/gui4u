﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ColorBoxGradient.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Defines the ColorBoxGradient type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Diagnostics;
using Gui4UFramework.Colors;
using Gui4UFramework.Graphics;
using Gui4UFramework.Management;
using Gui4UFramework.Structural;

namespace GUI4UControls.Color
{
    /// <summary>
    /// Shows a gradient color box with a indicator on top that changes color by changes in the HSL property
    /// </summary>
    public class ColorBoxGradient : Control
    {
        /// <summary>
        /// How we show data on the control for the used color
        /// </summary>
        private DrawStyle _drawStyle = DrawStyle.Hue;

        /// <summary>
        /// The color that we are showing on the control
        /// </summary>
        private HSL _hsl;

        /// <summary>
        /// The color that we are showing on the control
        /// </summary>
        private GuiColor _rgba;

        /// <summary>
        /// If we are dragging the indicator over this control
        /// </summary>
        private bool _dragging;

        /// <summary>
        /// The gradient value
        /// </summary>
        private DVector2 _gradientValue;

        /// <summary>
        /// If we must redraw this control during update
        /// </summary>
        private bool _redrawControlFlag;

        /// <summary>
        /// Initializes a new instance of the <see cref="ColorBoxGradient"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public ColorBoxGradient(string name) : base(name)
        {
            this._hsl = new HSL { H = 0.3, S = 0.6, L = 0.8 };
            this._rgba = AdobeColors.HSLToRgb(this._hsl);
            this._drawStyle = DrawStyle.Hue;
            Config.Width = Theme.ControlWidth;
            Config.Height = Theme.ControlWidth;
            this._redrawControlFlag = true;
        }

        /// <summary>
        /// Gets the indicator. The indicator points out which value is picked by this control.
        /// </summary>
        /// <value>
        /// The indicator.
        /// </value>
        public ColorBoxGradientIndicator Indicator { get; private set; }

        /// <summary>
        /// Gets or sets the color that we are showing on the control
        /// </summary>
        /// <value>
        /// The color that we are showing.
        /// </value>
        public HSL HSL
        {
            get
            {
                return this._hsl;
            }

            set
            {
                this._hsl = value;
                this._redrawControlFlag = true;
            }
        }

        /// <summary>
        /// Gets or sets the color that we are showing on the control
        /// </summary>
        /// <value>
        /// The color that we are showing.
        /// </value>
        public GuiColor RGBA
        {
            get
            {
                return this._rgba;
            }

            set
            {
                this._rgba = value;
                this._redrawControlFlag = true;
            }
        }

        /// <summary>
        /// Gets or sets how we show data on the control for the used color
        /// </summary>
        /// <value>
        /// The style to use to draw the control.
        /// </value>
        public DrawStyle DrawStyle
        {
            get
            {
                return this._drawStyle;
            }

            set
            {
                this._drawStyle = value;
                this._redrawControlFlag = true;
            }
        }

        /// <summary>
        /// Called when graphics resources need to be loaded.
        ///
        /// Use this for the usage of :
        /// - creation of the internal embedded controls.
        /// - setting of the variables and resources in this control
        /// - to load any game-specific graphics resources
        /// - take over the config width and height and use it into State
        /// - overriding how this item looks like , by settings its texture or theme
        ///
        /// Call base.LoadContent before you do your override code, this will cause :
        /// - State.SourceRectangle to be reset to the Config.Size
        /// </summary>
        public override void LoadContent()
        {
            this.Indicator = new ColorBoxGradientIndicator(Name + "-Indicator");
            this.AddControl(this.Indicator);

            this.SetIndicatorPosition(new DVector2(0, 0));

            this._redrawControlFlag = true;

            base.LoadContent();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            var leftMousePressed = Manager.InputManager.ReadLeftMousePressed();
            var mouseLocation = Manager.InputManager.ReadMouseLocation();

            // check the redraw FLAG !
            if (this._redrawControlFlag == true)
            {
                this.RedrawControl();
                this._redrawControlFlag = false;
            }

            if (this.Manager != null && this.Manager.FocusedNode == this && leftMousePressed)
            {
                this._dragging = true;
            }

            if (this._dragging && leftMousePressed)
            {
                var relativeLocation = PointRelative(mouseLocation);
                this._gradientValue = new DVector2(relativeLocation.X / Config.Width, relativeLocation.Y / Config.Height);
                this._redrawControlFlag = true;
                this.TakeValue(this._gradientValue);
                Debug.WriteLine("Rel " + relativeLocation + " val " + this._gradientValue);
            }

            if (this._dragging && leftMousePressed == false)
            {
                this._dragging = false;
            }
        }

        /// <summary>
        /// Takes the value that came from the relative position of the mouse , to update color.
        /// </summary>
        /// <param name="newValue">The new value.</param>
        private void TakeValue(DVector2 newValue)
        {
            switch (this._drawStyle)
            {
                case DrawStyle.Hue:
                    HSL.S = newValue.X;
                    HSL.L = 1 - newValue.Y;
                    this.RGBA = AdobeColors.HSLToRgb(HSL);
                    break;

                case DrawStyle.Saturation:
                    HSL.H = newValue.X;
                    HSL.L = 1 - newValue.Y;
                    this.RGBA = AdobeColors.HSLToRgb(HSL);
                    break;

                case DrawStyle.Brightness:
                    HSL.H = newValue.X;
                    HSL.S = 1 - newValue.Y;
                    this.RGBA = AdobeColors.HSLToRgb(HSL);
                    break;

                case DrawStyle.Red:
                    this.RGBA.UpdateG((byte)(1 - newValue.Y));
                    this.RGBA.UpdateB((byte)newValue.X);
                    HSL = AdobeColors.RGBToHSL(this.RGBA);
                    break;

                case DrawStyle.Green:
                    this.RGBA.UpdateB((byte)newValue.X);
                    this.RGBA.UpdateR((byte)(1 - newValue.Y));
                    HSL = AdobeColors.RGBToHSL(this.RGBA);
                    break;

                case DrawStyle.Blue:
                    this.RGBA.UpdateR((byte)newValue.X);
                    this.RGBA.UpdateG((byte)(1 - newValue.Y));
                    HSL = AdobeColors.RGBToHSL(this.RGBA);
                    break;
            }
        }

        /// <summary>
        /// Redraws the control using a chosen style.
        /// </summary>
        private void RedrawControl()
        {
            this.SetIndicatorPosition(this._gradientValue);

            var width = (int)Config.Width;
            var heigth = (int)Config.Height;

            if (Manager.ImageCompositor.Contains(this.State.CurrentTextureName) == false)
            {
                string finalName;
                var success = Manager.ImageCompositor.CreateFlatTexture(this.Name + "-Background", width, heigth, GuiColor.Gainsboro(), out finalName);
                Debug.Assert(success);
                this.State.CurrentTextureName = finalName;
            }

            var array = new ColorMap(width, heigth);
            switch (this._drawStyle)
            {
                case DrawStyle.Hue:
                    this.DrawStyleHue(ref array);
                    break;

                case DrawStyle.Saturation:
                    this.DrawStyleSaturation(ref array);
                    break;

                case DrawStyle.Brightness:
                    this.DrawStyleLuminance(ref array);
                    break;

                case DrawStyle.Red:
                    this.DrawStyleRed(ref array);
                    break;

                case DrawStyle.Green:
                    this.DrawStyleGreen(ref array);
                    break;

                case DrawStyle.Blue:
                    this.DrawStyleBlue(ref array);
                    break;
            }

            Manager.ImageCompositor.UpdateTexture(this.State.CurrentTextureName, array);
        }

        /// <summary>
        /// Draws this control in the style blue.
        /// </summary>
        /// <param name="map">The map to update.</param>
        private void DrawStyleBlue(ref ColorMap map)
        {
            var blue = this._rgba.B;
            for (var x = 0; x < map.Width; x++)
            {
                for (var y = 0; y < map.Height; y++)
                {
                    // red = x, green = y , blue is constant
                    var red = Round((x / map.Width) * 255);
                    var green = Round(255 - (255 * (double)y / (map.Height - 4)));

                    map.Set(x, y, new GuiColor((byte)red, (byte)green, blue));
                }
            }
        }

        /// <summary>
        /// Draws this control in the style green.
        /// </summary>
        /// <param name="map">The map to update.</param>
        private void DrawStyleGreen(ref ColorMap map)
        {
            var green = this._rgba.G;
            for (var x = 0; x < map.Width; x++)
            {
                for (var y = 0; y < map.Height; y++)
                {
                    // red = x, green = constant , blue = y
                    var red = Round((x / map.Width) * 255);
                    var blue = Round(255 - (255 * (double)y / (map.Height - 4)));

                    map.Set(x, y, new GuiColor((byte)red, green, (byte)blue));
                }
            }
        }

        /// <summary>
        /// Draws this control in the style red.
        /// </summary>
        /// <param name="map">The map to update.</param>
        private void DrawStyleRed(ref ColorMap map)
        {
            var red = this._rgba.G;
            for (var x = 0; x < map.Width; x++)
            {
                for (var y = 0; y < map.Height; y++)
                {
                    // red = constant, green = x , blue = y
                    var green = Round((x / map.Width) * 255);
                    var blue = Round(255 - (255 * (double)y / (map.Height - 4)));

                    map.Set(x, y, new GuiColor(red, (byte)green, (byte)blue));
                }
            }
        }

        /// <summary>
        /// Draws this control in the style luminance.
        /// </summary>
        /// <param name="map">The map to update.</param>
        private void DrawStyleLuminance(ref ColorMap map)
        {
            var hslStart = new HSL();
            var hslEnd = new HSL();
            hslStart.L = this._hsl.L;
            hslEnd.L = this._hsl.L;
            hslStart.S = 1.0;
            hslEnd.S = 0.0;

            for (var x = 0; x < map.Width; x++)
            {
                for (var y = 0; y < map.Height; y++)
                {
                    // Calculate Hue at this line (Saturation and Luminance are constant)
                    hslStart.H = (double)x / map.Width;
                    hslEnd.H = hslStart.H;

                    var rgbStart = AdobeColors.HSLToRgb(hslStart);
                    var rgbEnd = AdobeColors.HSLToRgb(hslEnd);

                    var lerpValue = y / map.Height;

                    var rgbValue = AdobeColors.Lerp(rgbStart, rgbEnd, lerpValue);
                    map.Set(x, y, rgbValue);
                }
            }
        }

        /// <summary>
        /// Draws this control in the style saturation.
        /// </summary>
        /// <param name="map">The map to update.</param>
        private void DrawStyleSaturation(ref ColorMap map)
        {
            var hslStart = new HSL();
            var hslEnd = new HSL();
            hslStart.S = this._hsl.S;
            hslEnd.S = this._hsl.S;
            hslStart.L = 1.0;
            hslEnd.L = 0.0;

            for (var x = 0; x < map.Width; x++)
            {
                for (var y = 0; y < map.Height; y++)
                {
                    // Calculate Hue at this line (Saturation and Luminance are constant)
                    hslStart.H = (double)x / map.Width;
                    hslEnd.H = hslStart.H;

                    var rgbStart = AdobeColors.HSLToRgb(hslStart);
                    var rgbEnd = AdobeColors.HSLToRgb(hslEnd);

                    var lerpValue = y / map.Height;

                    var rgbValue = AdobeColors.Lerp(rgbStart, rgbEnd, lerpValue);
                    map.Set(x, y, rgbValue);
                }
            }
        }

        /// <summary>
        /// Draws this control in the style hue.
        /// </summary>
        /// <param name="map">The map to update.</param>
        private void DrawStyleHue(ref ColorMap map)
        {
            var hslStart = new HSL();
            var hslEnd = new HSL();

            hslStart.H = this._hsl.H;
            hslEnd.H = this._hsl.H;

            hslStart.S = 0;
            hslEnd.S = 1;

            for (var y = 0; y < map.Height; y++)
            {
                hslStart.L = (float)y / map.Height;
                hslEnd.L = hslStart.L;

                var rgbStart = AdobeColors.HSLToRgb(hslStart);
                var rgbEnd = AdobeColors.HSLToRgb(hslEnd);

                for (var x = 0; x < map.Width; x++)
                {
                    var lerpValue = 1 - (x / (float)map.Width);

                    // System.Diagnostics.Debug.WriteLine(lerpValue);
                    var rgbValue = AdobeColors.Lerp(rgbStart, rgbEnd, lerpValue);
                    map.Set(x, y, rgbValue);
                }
            }
        }

        /// <summary>
        /// Sets the indicator position.
        /// </summary>
        /// <param name="newValue">The new value.</param>
        private void SetIndicatorPosition(DVector2 newValue)
        {
            var x = Config.Width * newValue.X;
            if (x < 0)
            {
                x = 0;
            }

            if (x > Config.Width)
            {
                x = Config.Width;
            }

            var y = Config.Height * newValue.Y;
            if (y < 0)
            {
                y = 0;
            }

            if (y > Config.Height)
            {
                y = Config.Height;
            }

            this.Indicator.Config.PositionX = x;
            this.Indicator.Config.PositionY = y;
        }

        /// <summary>
        /// Kind of self explanatory, I really need to look up the .NET function that does this.
        /// </summary>
        /// <param name="val">Double value to be rounded to an integer.</param>
        /// <returns>The rounded value.</returns>
        private static int Round(double val)
        {
            var retVal = (int)val;

            var temp = (int)(val * 100);

            if ((temp % 100) >= 50)
            {
                retVal += 1;
            }

            return retVal;
        }
    }
}