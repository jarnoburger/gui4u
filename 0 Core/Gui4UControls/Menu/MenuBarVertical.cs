// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MenuBarVertical.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Defines the MenuBarVertical type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;

namespace GUI4UControls.Menu
{
    /// <summary>
    /// Shows a vertical menu like photo-shop does
    /// </summary>
    public class MenuBarVertical : MenuContainer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MenuBarVertical"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public MenuBarVertical(string name) : base(name)
        {
            this.ConfigExpanded = true;
        }

        /// <summary>
        /// Draw the texture at DrawPosition combined with its offset
        /// </summary>
        public override void DrawMyData()
        {
            if (!State.Visible)
            {
                return;
            }

            if (this.DebugLayout)
            {
                Manager.ImageCompositor.Draw(this.State);
            }
            else
            {
                Manager.ImageCompositor.Draw(this.State);
            }
        }

        /// <summary>
        /// Will redraw myself , and set the children values correctly (when collapsed/expanded)
        /// </summary>
        protected override void Redraw()
        {
            if (this.ConfigExpanded)
            {
                this.DrawAllExpanded();
            }
            else
            {
                this.DrawAllCollapsed();
            }
        }

        /// <summary>
        /// Called when a button was [clicked].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected override void OnMouseClicked(object sender, EventArgs e)
        {
            this.ConfigExpanded = !this.ConfigExpanded;
        }

        /// <summary>
        /// Draws all expanded.
        /// </summary>
        private void DrawAllExpanded()
        {
            // place the first child on the right of me at the same height
            // the other children will be underneath me
            const int x = 0;
            var y = this.ParentButton.Config.Height;

            foreach (var child in this.Children)
            {
                // check if the child is a menu-item
                var menuItemBase = child as MenuItemBase;
                if (menuItemBase == null)
                {
                    return;
                }

                // lets draw it
                menuItemBase.State.Visible = true;
                menuItemBase.Config.PositionX = x;
                menuItemBase.Config.PositionY = y;
                menuItemBase.Config.Width = Config.Width;
                menuItemBase.Config.Height = Theme.ControlHeight * 0.9f;
                y = (int)(y + menuItemBase.Config.Height);
            }
        }

        /// <summary>
        /// Draws all collapsed.
        /// </summary>
        private void DrawAllCollapsed()
        {
            // place the first child on the right of me at the same height
            // the other children will be underneath me
            this.State.Visible = false;

            foreach (var child in this.Children)
            {
                // check if the child is a menu-item
                var menuItemBase = child as MenuItemBase;
                if (menuItemBase == null)
                {
                    return;
                }

                menuItemBase.State.Visible = false;
            }
        }
    }
}