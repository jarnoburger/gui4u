// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Button.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Just a standard button
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using Gui4UFramework.EventArgs;
using Gui4UFramework.Graphics;
using Gui4UFramework.Management;

namespace GUI4UControls.Buttons
{
    /// <summary>
    /// Just a standard button.
    /// </summary>
    public class Button : ButtonBase
    {
        #region Declare
        /// <summary>
        /// Button is pressed in.
        /// </summary>
        public event EventHandler<GameTimeEventArgs> LeftMousePressed;

        /// <summary>
        /// Left mouse is released after button pressed in.
        /// </summary>
        public event EventHandler<GameTimeEventArgs> LeftMouseReleased;

        /// <summary>
        /// Click-release event.
        /// </summary>
        public event EventHandler<GameTimeEventArgs> Clicked;

        /// <summary>
        /// The texture name for in the event of a "left mouse down".
        /// </summary>
        private string _textureNameLeftMouseDown;

        private string _textureToUse;
        #endregion

        #region Initialize
        /// <summary>
        /// Initializes a new instance of the <see cref="Button"/> class.
        /// </summary>
        /// <param name="name">The name of the button.</param>
        public Button(string name) : base(name)
        {
            this.Config.Width = this.Theme.ControlWidth;
            this.Config.Height = this.Theme.ControlHeight;
        }
        #endregion

        /// <summary>Called when graphics resources need to be loaded.
        /// Use this for the usage of :
        /// - creation of the internal embedded controls.
        /// - setting of the variables and resources in this control
        /// - to load any game-specific graphics resources
        /// - take over the config width and height and use it into State
        /// - overriding how this item looks like , by settings its texture or theme
        ///
        /// Call base.LoadContent before you do your override code, this will cause :
        /// - State.SourceRectangle to be reset to the Config.Size.
        /// </summary>
        public override void LoadContent()
        {
            base.LoadContent();

            // create leftmouse-down-texture
            var success = Manager.ImageCompositor.CreateRectangleTexture(
                this.Name,
                (int)this.Config.Width,
                (int)this.Config.Height,
                this.Theme.BorderWidth,
                this.Theme.ClickedFillColor,
                this.Theme.ClickedBorderColor, out this._textureNameLeftMouseDown);

            _textureToUse = this._textureNameLeftMouseDown;
            Debug.Assert(success);
        }

        /// <summary>
        /// Unloads the content.
        /// </summary>
        public override void UnloadContent()
        {
            Manager.ImageCompositor.Delete(this._textureNameLeftMouseDown);
            base.UnloadContent();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // retrieve the mouse pressed
            var leftMousePressed = Manager.InputManager.ReadLeftMousePressed();
            var leftMouseReleased = Manager.InputManager.ReadLeftMouseReleased();

            // update the label text if needed
            if (this.Text != Label.ConfigText)
            {
                Label.ConfigText = this.Text;
            }

            // update the label visibility with my visibility
            this.Label.State.Visible = this.State.Visible;

            // Is mouse hovering over, or does the control have focus ?
            if (State.MouseHoveringOver && this.Manager.FocusedNode == this)
            {
                this.State.CurrentTextureName = this.TextureNameHover;

                // Is there a mouse press and the button is OFF ?
                if (leftMousePressed && this.ButtonState == ButtonState.Off)
                {
                    this.RaiseMouseDownEvent(gameTime);

                    this.State.CurrentTextureName = this._textureNameLeftMouseDown;

                    this.ButtonState = ButtonState.On;
                }
                else if (leftMouseReleased && this.ButtonState == ButtonState.On)
                {
                    this.ButtonState = ButtonState.Off;

                    Config.HoverColorsEnabled = true;

                    this.RaiseMouseUpEvent(gameTime);
                    this.RaiseMouseClickEvent(gameTime);
                }
            }
            else
            {
                this.State.CurrentTextureName = this.TextureNameDefault;

                // turn it off if the mouse hovers off it
                if (this.ButtonState == ButtonState.On)
                {
                    this.ButtonState = ButtonState.Off;
                    this.RaiseMouseUpEvent(gameTime);
                }
            }
            
            base.Update(gameTime);
        }

        /// <summary>
        /// Gets a value indicating whether this button is pressed.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [state is pressed]; otherwise, <c>false</c>.
        /// </value>
        public bool StateIsPressed
        {
            get
            {
                return this.ButtonState == ButtonState.On;
            }
        }

        /// <summary>
        /// Raises the mouse up event.
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        private void RaiseMouseUpEvent(GameTime gameTime)
        {
            this.LeftMouseReleased?.Invoke(this, new GameTimeEventArgs(gameTime));
        }

        /// <summary>
        /// Raises the mouse down event.
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        private void RaiseMouseDownEvent(GameTime gameTime)
        {
            this.LeftMousePressed?.Invoke(this, new GameTimeEventArgs(gameTime));
        }

        /// <summary>
        /// Raises the mouse click event.
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        private void RaiseMouseClickEvent(GameTime gameTime)
        {
            this.Clicked?.Invoke(this, new GameTimeEventArgs(gameTime));
        }
    }
}