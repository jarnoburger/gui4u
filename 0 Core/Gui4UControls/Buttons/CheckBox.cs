// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CheckBox.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Toggle check-box with label
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Diagnostics;
using Gui4UFramework.Colors;
using Gui4UFramework.EventArgs;
using Gui4UFramework.Layout.Enums;
using Gui4UFramework.Management;
using Gui4UFramework.Structural;
using GUI4UControls.Text;

namespace GUI4UControls.Buttons
{
    /// <summary>
    /// Toggle check-box with label
    /// </summary>
    public class CheckBox : Control
    {
        /// <summary>
        /// To allow only one change at a time
        /// </summary>
        private bool _mouseReleased = true;

        /// <summary>
        /// If the check-box is set  on this control
        /// </summary>
        private bool _isChecked;

        /// <summary>
        /// Initializes a new instance of the <see cref="CheckBox"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public CheckBox(string name)
            : base(name)
        {
            this.FontColor = GuiColor.MidnightBlue();

            this.Theme.FillColor = this.Theme.InputFillColor;
            this.Theme.BorderColor = this.Theme.BorderColor;

            this.Config.Width = this.Theme.ControlWidth;
            this.Config.Height = this.Theme.ControlHeight;

            this.ConfigText = string.Empty;
        }

        /// <summary>
        /// Gets or sets the label that contains the text that defines what is been checked.
        /// </summary>
        /// <value>
        /// The text label.
        /// </value>
        protected Label Label { get; set; }

        /// <summary>Gets or sets the button that gives you the option to 'tick' the check-box</summary>
        /// <value>The tick box control.</value>
        protected Button TickBox { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="CheckBox"/> is checked.
        /// </summary>
        /// <value>
        ///   <c>true</c> if checked; otherwise, <c>false</c>.
        /// </value>
        public bool Checked
        {
            get
            {
                return this._isChecked;
            }

            set
            {
                this._isChecked = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the font that is used to draw the text
        /// </summary>
        /// <value>
        /// The name of the font.
        /// </value>
        public string FontName { get; set; }

        /// <summary>
        /// Gets or sets the text that is shown on the text-box
        /// </summary>
        /// <value>
        /// The configuration text.
        /// </value>
        public string ConfigText { get; set; }

        /// <summary>
        /// Gets or sets the color of the font of the text that is shown on the check-box
        /// </summary>
        /// <value>
        /// The color of the font.
        /// </value>
        public GuiColor FontColor { get; set; }

        /// <summary>
        /// Called when graphics resources need to be loaded.
        /// Use this for the usage of :
        /// - creation of the internal embedded controls.
        /// - setting of the variables and resources in this control
        /// - to load any game-specific graphics resources
        /// - take over the config width and height and use it into State
        /// - overriding how this item looks like , by settings its texture or theme
        /// Call base.LoadContent before you do your override code, this will cause :
        /// - State.SourceRectangle to be reset to the Config.Size
        /// </summary>
        public override void LoadContent()
        {
            // **** do the basic stuff
            base.LoadContent();

            // the padding is like this
            // outer border
            // pad
            // xText-width
            // pad
            // label-width
            // pad
            // outer border

            // **** add the tick-box
            const int padding = 2;
            this.TickBox = new Button(this.Name + "-Check")
            {
                Manager = this.Manager,
                Text = string.Empty,
                Config = { PositionX = padding, PositionY = padding }
            };
            this.Config.Height = this.Config.Height - padding - padding;
            this.TickBox.Config.DebugLayout = this.Config.DebugLayout;

            // make some room between top and bottom
            this.TickBox.Config.Width = this.TickBox.Config.Height; // make me square
            this.TickBox.Clicked += this.HandleToggle;

            // TickBox.LoadContent();
            this.AddControl(this.TickBox);

            // **** add the label
            this.Label = new Label(this.Name + "-ConfigText")
            {
                Manager = this.Manager,
                ConfigText = this.ConfigText,
                ConfigHorizontalAlignment = HorizontalAlignment.Left,
                ConfigVerticalAlignment = VerticalAlignment.Center,
                Config =
                {
                    DebugLayout = this.Config.DebugLayout,
                    PositionX = padding + this.TickBox.Config.Width + padding - this.Theme.BorderWidth,
                    PositionY = padding,
                }
            };
            this.Label.Config.Height = this.TickBox.Config.Height;
            this.Label.Config.Width = this.Config.Width - (padding * 3) - this.TickBox.Config.Width + this.Theme.BorderWidth;

            // start with some space next to the check-square
            // place me at the same height as the check-square
            // make me the same height as the check-square
            // Label.LoadContent();
            this.AddControl(this.Label);

            bool success;
            string finalName;
            // **** add the background
            success = this.Manager.ImageCompositor.CreateRectangleTexture(this.Name + "-Back", (int)this.State.Width, (int)this.State.Height, this.Theme.BorderWidth, this.Theme.FillColor, this.Theme.BorderColor, out finalName);
            Debug.Assert(success);
            this.State.CurrentTextureName = finalName;
        }

        /// <summary>
        /// Unloads the content.
        /// </summary>
        public override void UnloadContent()
        {
            this.Label.UnloadContent();
            this.TickBox.UnloadContent();
            this.Manager.ImageCompositor.Delete(this.State.CurrentTextureName);
            base.UnloadContent();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            var leftPressed = this.Manager.InputManager.ReadLeftMousePressed();
            var leftReleased = this.Manager.InputManager.ReadLeftMouseReleased();
            base.Update(gameTime);

            // Is mouse hovering over?
            if (this.Manager != null && this.Manager.FocusedNode == this && this.State.MouseHoveringOver)
            {
                // Mouse click?
                if (leftPressed && this._mouseReleased)
                {
                    this.HandleToggle(this, null);

                    this._mouseReleased = false;
                }
            }

            if (leftReleased)
            {
                this._mouseReleased = true;
            }
        }

        /// <summary>
        /// When the check event is been toggled
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="gameTimeEventArgs">The <see cref="GameTimeEventArgs"/> instance containing the event data.</param>
        private void HandleToggle(object sender, GameTimeEventArgs gameTimeEventArgs)
        {
            if (this._isChecked)
            {
                this._isChecked = false;
                this.TickBox.Text = string.Empty;
            }
            else
            {
                this._isChecked = true;
                this.TickBox.Text = "X";
            }

            Debug.WriteLine(this.Name + " checked = " + this._isChecked);
        }
    }
}