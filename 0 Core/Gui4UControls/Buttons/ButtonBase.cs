﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ButtonBase.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Base class for buttons , like toggle button or standard button
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Diagnostics;
using Gui4UFramework.Graphics;
using Gui4UFramework.Layout.Enums;
using Gui4UFramework.Structural;
using GUI4UControls.Text;

namespace GUI4UControls.Buttons
{
    /// <summary>
    /// Base class for buttons , like toggle button or standard button.
    /// </summary>
    public abstract class ButtonBase : Control
    {
        #region Declare
        /// <summary>
        /// The  text shown on the button.
        /// </summary>
        private string _text;
        #endregion

        #region Initialize
        /// <summary>
        /// Initializes a new instance of the <see cref="ButtonBase"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        protected ButtonBase(string name) : base(name)
        {
            this.ButtonState = ButtonState.Off;
            Config.HoverColorsEnabled = true;
            this._text = string.Empty;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the label that represent the text on the button.
        /// </summary>
        /// <value>
        /// The label.
        /// </value>
        public Label Label { get; private set; }

        /// <summary>
        /// Gets or sets the text shown on the button.
        /// </summary>
        /// <value>
        /// The text shown on the button.
        /// </value>
        public string Text
        {
            get
            {
                return this._text;
            }

            set
            {
                this._text = value;
            }
        }

        /// <summary>
        /// Gets or sets the state of the button. (on/off)
        /// </summary>
        /// <value>
        /// The state of the button.
        /// </value>
        public ButtonState ButtonState { get; set; }

        /// <summary>
        /// Gets the hover texture name.
        /// </summary>
        /// <value>
        /// The hover texture name.
        /// </value>
        protected string TextureNameHover;

        /// <summary>
        /// Gets the default texture name.
        /// </summary>
        /// <value>
        /// The default texture name.
        /// </value>
        protected string TextureNameDefault;
        #endregion

        #region Events
        /// <summary>
        /// When mouse enters this control
        /// </summary>
        public override void HoverEnter()
        {
            State.UseHovering = true;

            if (Config.HoverColorsEnabled)
            {
                this.State.CurrentTextureName = this.TextureNameHover;
            }

            base.HoverEnter();
        }

        /// <summary>
        /// When mouse leaves this control
        /// </summary>
        public override void HoverExit()
        {
            State.UseHovering = false;

            if (Config.HoverColorsEnabled)
            {
                this.State.CurrentTextureName = this.TextureNameDefault;
            }

            base.HoverExit();
        }
        #endregion

        #region Important functions
        /// <summary>
        /// Called when graphics resources need to be loaded.
        /// Use this for the usage of :
        /// - creation of the internal embedded controls.
        /// - setting of the variables and resources in this control
        /// - to load any game-specific graphics resources
        /// - take over the config width and height and use it into State
        /// - overriding how this item looks like , by settings its texture or theme
        /// Call base.LoadContent before you do your override code, this will cause :
        /// - State.SourceRectangle to be reset to the Config.Size
        /// </summary>
        public override void LoadContent()
        {
            // make my text that i show
            this.Label = new Label(Name + "-ConfigText")
                                                        {
                                                            ConfigText = this._text,
                                                            ConfigHorizontalAlignment = HorizontalAlignment.Center,
                                                            ConfigVerticalAlignment = VerticalAlignment.Center,
                                                            Manager = Manager,
                                                        };
            this.Label.Config.Width = Config.Width - Theme.BorderWidth;
            this.Label.Config.Height = Config.Height - Theme.BorderWidth;
            this.Label.Config.PositionX = (Config.Width - this.Label.Config.Width) / 2;
            this.Label.Config.PositionY = (Config.Height - this.Label.Config.Height) / 2;
            this.AddControl(this.Label);

            // create my default background texture
            var success = Manager.ImageCompositor.CreateRectangleTexture(
                                                                    this.Name + " default",
                                                                    (int)State.Width,
                                                                    (int)State.Height, 
                                                                    Theme.BorderWidth,
                                                                    Theme.FillColor,
                                                                    Theme.BorderColor,
                                                                    out TextureNameDefault);
            Debug.Assert(success);

            // create my hover texture
            success = Manager.ImageCompositor.CreateRectangleTexture(
                                                                    this.Name + " hover", 
                                                                    (int)State.Width, 
                                                                    (int)State.Height, 
                                                                    Theme.BorderWidth, 
                                                                    Theme.HoverFillColor, 
                                                                    Theme.HoverBorderColor, 
                                                                    out TextureNameHover);
            Debug.Assert(success);

            //set current texture to background-texture
            this.State.CurrentTextureName = this.TextureNameDefault;
        }

        /// <summary>
        /// Unloads the content.
        /// </summary>
        public override void UnloadContent()
        {
            this.Label.UnloadContent();
            Manager.ImageCompositor.Delete(this.TextureNameDefault);
            Manager.ImageCompositor.Delete(this.TextureNameHover);
            base.UnloadContent();
        }
        #endregion
    }
}