﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TreeViewExpander.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   I am like a switch button.
//   I can be on , i can be off
//   I only use a two images instead of a button up/down look.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Diagnostics;
using Gui4UFramework.Colors;
using Gui4UFramework.Structural;

namespace GUI4UControls.Tree
{
    /// <summary>
    /// I am like a switch button.
    /// I can be on , i can be off
    /// I only use a two images instead of a button up/down look.
    /// </summary>
    public class TreeViewExpander : Control
    {
        /// <summary>Initializes a new instance of the <see cref="TreeViewExpander"/> class.</summary>
        /// <param name="name">The name.</param>
        public TreeViewExpander(string name) : base(name)
        {
        }

        /// <summary>
        /// Gets or sets a value indicating whether we debug the location.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [debug location]; otherwise, <c>false</c>.
        /// </value>
        public bool DebugLocation { get; set; }

        /// <summary>
        /// Called when graphics resources need to be loaded.
        ///
        /// Use this for the usage of :
        /// - creation of the internal embedded controls.
        /// - setting of the variables and resources in this control
        /// - to load any game-specific graphics resources
        /// - take over the config width and height and use it into State
        /// - overriding how this item looks like , by settings its texture or theme
        ///
        /// Call base.LoadContent before you do your override code, this will cause :
        /// - State.SourceRectangle to be reset to the Config.Size
        /// </summary>
        public override void LoadContent()
        {
            base.LoadContent();

            const int width = 10;
            const int height = 10;
            var color = new GuiColor(0, 0, 255);
            string finalName;
            bool success;
            success = this.Manager.ImageCompositor.CreateFlatTexture(this.Name + "-Background", width, height, color, out finalName);
            Debug.Assert(success);
            this.State.CurrentTextureName = finalName;
        }
    }
}