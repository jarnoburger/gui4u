﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgressBarBase.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Defines the ProgressBarBase type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Gui4UFramework.Colors;
using Gui4UFramework.Management;
using Gui4UFramework.Structural;

namespace GUI4UControls.ProgressBar
{
    /// <summary>The base class for the progress bar</summary>
    public abstract class ProgressBarBase : Control
    {
        /// <summary>
        /// The default minimum bar value
        /// </summary>
        protected const int DefaultBarValueMax = 100;

        /// <summary>
        /// The default maximum bar value
        /// </summary>
        protected const int DefaultBarValueMin = 0;

        /// <summary>
        /// The default tick for the bar-value
        /// </summary>
        protected const int DefaultBarValueTick = 5;

        /// <summary>
        /// If we must redraw during Update()
        /// </summary>
        private bool _mustRedraw;

        /// <summary>
        /// The value that is shown
        /// </summary>
        private float _configValue;

        /// <summary>
        /// The maximum that the value can get, otherwise its clipped to maximum.
        /// </summary>
        private int _configMaximumValue;

        /// <summary>
        /// The minimum that the value can get, otherwise its clipped to minimum.
        /// </summary>
        private int _configMinimumValue;

        /// <summary>
        /// If we must check if value is still between minimum and maximum..
        /// </summary>
        private bool _mustRevalidate;

        /// <summary>
        /// The bar color that represents the value.
        /// </summary>
        private GuiColor _configValueBarColor;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgressBarBase"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        protected ProgressBarBase(string name)
            : base(name)
        {
            this.ConfigMinimumValue = DefaultBarValueMin;
            this.ConfigMaximumValue = DefaultBarValueMax;
            this.ConfigValue = DefaultBarValueMax;
            this.ConfigValueBarColor = new GuiColor(255, 255, 0);
        }

        /// <summary>
        /// Gets or sets the color of the value bar.
        /// </summary>
        /// <value>
        /// The color of the configuration bar.
        /// </value>
        public GuiColor ConfigValueBarColor
        {
            get
            {
                return this._configValueBarColor;
            }

            set
            {
                this._configValueBarColor = value;
                this._mustRedraw = true;
            }
        }

        /// <summary>
        /// Gets or sets the current value.
        /// </summary>
        /// <value>
        /// The current value.
        /// </value>
        public float ConfigValue
        {
            get
            {
                return this._configValue;
            }

            set
            {
                this._configValue = value;
                this._mustRevalidate = true;
                this._mustRedraw = true;
            }
        }

        /// <summary>
        /// Gets or sets the maximum value.
        /// </summary>
        /// <value>
        /// The maximum value.
        /// </value>
        public int ConfigMaximumValue
        {
            get
            {
                return this._configMaximumValue;
            }

            set
            {
                this._configMaximumValue = value;
                this._mustRevalidate = true;
                this._mustRedraw = true;
            }
        }

        /// <summary>
        /// Gets or sets the minimum value.
        /// </summary>
        /// <value>
        /// The minimum value.
        /// </value>
        public int ConfigMinimumValue
        {
            get
            {
                return this._configMinimumValue;
            }

            set
            {
                this._configMinimumValue = value;
                this._mustRevalidate = true;
                this._mustRedraw = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether we are debugging the layout.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [debug layout]; otherwise, <c>false</c>.
        /// </value>
        public bool DebugLayout { get; set; }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if (this._mustRevalidate)
            {
                this.Revalidate();
                this._mustRevalidate = false;
            }

            if (this._mustRedraw)
            {
                this.Redraw();
                this._mustRedraw = false;
            }
        }

        /// <summary>
        /// Draw the texture at DrawPosition combined with its offset
        /// </summary>
        public override void DrawMyData()
        {
            if (!State.Visible)
            {
                return;
            }

            if (this.DebugLayout)
            {
                Manager.ImageCompositor.Draw(this.State);
            }
            else
            {
                Manager.ImageCompositor.Draw(this.State);
            }
        }

        /// <summary>
        /// Revalidates the important values for this instance
        /// </summary>
        public void Revalidate()
        {
            if (this._configMaximumValue < this._configMinimumValue)
            {
                this._configMaximumValue = this._configMinimumValue;
                this._mustRedraw = true;
            }

            if (this._configValue < this._configMinimumValue)
            {
                this._configValue = this._configMinimumValue;
                this._mustRedraw = true;
            }

            if (this._configValue > this._configMaximumValue)
            {
                this._configValue = this._configMaximumValue;
                this._mustRedraw = true;
            }
        }

        /// <summary>
        /// Redraws this instance.
        /// </summary>
        protected abstract void Redraw();
    }
}