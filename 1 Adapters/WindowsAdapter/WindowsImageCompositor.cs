﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WindowsImageCompositor.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Here all the magic drawing happens.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;
using Gui4UFramework.Colors;
using Gui4UFramework.Graphics;
using Gui4UFramework.Management;
using Gui4UFramework.Structural;
using GUI4UWindows.Resources;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Rectangle = Microsoft.Xna.Framework.Rectangle;

namespace GUI4UWindows
{
    /// <summary>Here all the magic drawing happens.</summary>
    public sealed class WindowsImageCompositor : ImageCompositor, IDisposable
    {
        /// <summary>The resource pool, containing textures.</summary>
        private readonly ResourcesTexture _resourcesTexture;

        /// <summary>The resource pool, containing the RenderTarget2D items.</summary>
        private readonly ResourcesRenderTarget2D _resourcesRenderTarget;

        /// <summary>The resource pool, containing the SpriteFonts.</summary>
        private readonly ResourcesSpriteFont _resourcesSpriteFont;

        /// <summary>The graphics device.</summary>
        private readonly GraphicsDevice _device;

        /// <summary>The sprite batch.</summary>
        private readonly SpriteBatch _spriteBatch;

        /// <summary>The disposed.Flag: Has Dispose already been called?. </summary>
        private bool _disposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="WindowsImageCompositor"/> class.
        /// </summary>
        /// <param name="game">The game that must be used to use me.</param>
        public WindowsImageCompositor(Game game)
        {
#if DEBUG
            if (game == null)
            {
                throw new ArgumentNullException(nameof(game), "Game cannot be null, its used here.");
            }
#endif

            this._resourcesTexture = new ResourcesTexture(game.GraphicsDevice, game.Content);
            this._resourcesSpriteFont = new ResourcesSpriteFont(game.Content);
            this._resourcesRenderTarget = new ResourcesRenderTarget2D(game.GraphicsDevice);
            this._spriteBatch = new SpriteBatch(game.GraphicsDevice);
            this._device = game.GraphicsDevice;
        }

        /// <summary>Creates a rectangle texture.</summary>
        /// <param name="preferredName">The preferred name to use.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <param name="borderWidth">Width of the border.</param>
        /// <param name="fillColor">Color of the fill.</param>
        /// <param name="borderColor">Color of the border.</param>
        /// <param name="finalName"></param>
        /// <returns>The URL to the created texture resource that looks like a rectangle.</returns>
        /// <exception cref="ArgumentException">Width can not be zero or less;width
        /// or
        /// height can not be zero or less;height
        /// or
        /// Control name can not be null;preferredName
        /// or
        /// Border width can not be smaller then zero;borderWidth.</exception>
        public override bool CreateRectangleTexture(string preferredName, int width, int height, int borderWidth, GuiColor fillColor, GuiColor borderColor, out string finalName)
        {
#if DEBUG
            if (width <= 0)
            {
                throw new ArgumentException("width can not be zero or less", nameof(width));
            }

            if (height <= 0)
            {
                throw new ArgumentException("height can not be zero or less", nameof(height));
            }

            if (string.IsNullOrEmpty(preferredName))
            {
                throw new ArgumentException("Control name can not be null", nameof(preferredName));
            }

            if (borderWidth < 0)
            {
                throw new ArgumentException("Border width can not be smaller then zero", nameof(borderWidth));
            }
#endif
            var success =  this._resourcesTexture.Create(preferredName, width, height, fillColor, out finalName);
            System.Diagnostics.Debug.Assert(success);

            // ReSharper disable once HeuristicUnreachableCode
            // ReSharper disable HeuristicUnreachableCode
            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            if (success == false)
            {
                finalName = string.Empty;
                return false;
            }
            // ReSharper restore HeuristicUnreachableCode

            // var colordata = RectangleBrush.CreateRectangleColorMap(width, height,borderWidth,1,1, fillColor, borderColor);

            var colordata = ColorMapDrawer.CreateRectangleColorMap(width, height, borderWidth, fillColor, borderColor);

            // var colordata = ColorMapDrawer.CreateRoundedRectangleColorMap(
            //                                                            width,
            //                                                            height,
            //                                                            borderWidth,
            //                                                            20,
            //                                                            fillColor,
            //                                                            borderColor);

            this._resourcesTexture.Update(finalName, colordata.ToArray());

            System.Diagnostics.Debug.WriteLine("Created a rectangle texture : " + finalName);

            return true;
        }

        /// <summary>
        /// Creates the image texture.
        /// </summary>
        /// <param name="preferredName">Name of the base.</param>
        /// <param name="imageLocation">Where the image resides.</param>
        /// <param name="finalName"></param>
        /// <returns>
        /// A texture with a image inside.
        /// </returns>
        public override bool CreateImageTexture(string preferredName, string imageLocation, out string finalName)
        {
            var success = this._resourcesTexture.CreateFromUrl(preferredName, imageLocation, out finalName);
            System.Diagnostics.Debug.WriteLine("Created a image texture : " + finalName);
            return success;
        }

        /// <summary>
        /// Creates the flat texture.
        /// </summary>
        /// <param name="preferredName">Name of the preferred.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <param name="color">The color.</param>
        /// <param name="finalName"></param>
        /// <returns>A flat texture.</returns>
        public override bool CreateFlatTexture(string preferredName, int width, int height, GuiColor color, out string finalName)
        {
            var success = this._resourcesTexture.Create(preferredName, width, height, color, out finalName);
            System.Diagnostics.Debug.WriteLine("Created a flat texture : " + finalName);
            return success;
        }

        /// <summary>Creates a render target to render onto.</summary>
        /// <param name="preferredName">Name of the base.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <param name="mipMap">If it will use MipMapping.</param>
        /// <param name="finalName"></param>
        /// <returns>A render target.</returns>
        public override bool CreateRenderTarget(string preferredName, int width, int height, bool mipMap, out string finalName)
        {
            var success = this._resourcesRenderTarget.CreateRenderTarget(preferredName, width, height, mipMap, out finalName);
            System.Diagnostics.Debug.WriteLine("Created a rendertarget : " + finalName);
            return success;
        }

        /// <summary>
        /// Creates a sprite font, to be used for text rendering.
        /// </summary>
        /// <param name="spriteFontName">Name of the sprite font in your Content.</param>
        /// <param name="finalName"></param>
        public override bool CreateSpriteFont(string spriteFontName, out string finalName)
        {
            if (string.IsNullOrEmpty(spriteFontName))
            {
                System.Diagnostics.Debug.WriteLine("The font name cannot be NULL or empty");
                Debugger.Break();
                finalName = string.Empty;
                return false;
            }
            
            var success = this._resourcesSpriteFont.CreateFromFontName(spriteFontName, out finalName);

            return success;
        }

        /// <summary>
        /// Reads the size texture.
        /// </summary>
        /// <param name="textureName">The name of the texture.</param>
        /// <returns>The size found.</returns>
        public override DVector2 ReadSizeTexture(string textureName)
        {
            Texture2D texture;
            var success = this._resourcesTexture.Read(textureName, out texture);
            System.Diagnostics.Debug.Assert(success);
            return new DVector2(texture.Width, texture.Height);
        }

        /// <summary>
        /// Reads the size string.
        /// </summary>
        /// <param name="spriteFontName">Name of the sprite font.</param>
        /// <param name="text">The text that is being read.</param>
        /// <returns>The size found.</returns>
        public override DVector2 ReadSizeString(string spriteFontName, string text)
        {
            SpriteFont font;
            var success = this._resourcesSpriteFont.Read(spriteFontName, out font);
            System.Diagnostics.Debug.Assert(success);
            var size = font.MeasureString(text);
            return new DVector2(size.X, size.Y);
        }

        /// <summary>
        /// Updates the texture with given render-target.
        /// </summary>
        /// <param name="textureName">Name of the texture.</param>
        /// <param name="renderTargetName">Name of the render target.</param>
        public override void UpdateTexture(string textureName, string renderTargetName)
        {
            bool success;

            Texture2D texture;
            success = this._resourcesTexture.Read(textureName, out texture);
            System.Diagnostics.Debug.Assert(success);

            RenderTarget2D renderTarget;
            success = this._resourcesRenderTarget.Read(renderTargetName, out renderTarget);
            System.Diagnostics.Debug.Assert(success);
            // ReSharper disable once RedundantAssignment
            texture = renderTarget;
        }

        /// <summary>
        /// Updates the texture color image with give color image.
        /// </summary>
        /// <param name="textureName">Name of the texture.</param>
        /// <param name="colorMap">The color array.</param>
        public override void UpdateTexture(string textureName, ColorMap colorMap)
        {
#if DEBUG
            if (colorMap == null)
            {
                throw new ArgumentNullException(nameof(colorMap), "Given array can not be null , cause we use it to update the texture.");
            }
#endif

            // first reverse the data
            var reverseData = colorMap.Reverse();

            Texture2D texture;
            var success = this._resourcesTexture.Read(textureName, out texture);
            System.Diagnostics.Debug.Assert(success);

            texture.SetData(reverseData.ToArray());
        }

        /// <summary>
        /// Deletes the specified resource from the resource pool.
        /// </summary>
        /// <param name="resourceName">Name of the resource.</param>
        public override void Delete(string resourceName)
        {
            if (this._resourcesTexture.Contains(resourceName))
            {
                this._resourcesTexture.Delete(resourceName);
                return;
            }

            if (this._resourcesRenderTarget.Contains(resourceName))
            {
                this._resourcesRenderTarget.Delete(resourceName);
                return;
            }

            if (this._resourcesSpriteFont.Contains(resourceName))
            {
                this._resourcesSpriteFont.Delete(resourceName);
                return;
            }

            System.Diagnostics.Debug.WriteLine("Could not delete " + resourceName);
            Debugger.Break();
        }

        /// <summary>
        /// Notes the beginning of drawing the textures. This is for the engine a important signal.
        /// </summary>
        public override void BeginDraw()
        {
            this._spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
        }

        /// <summary>
        /// Draws the specified texture using specified draw-state , with a tint color.
        /// </summary>
        /// <param name="drawState">The state that contains all the info to draw the item.</param>
        public override void Draw(DrawState drawState)
        {
#if DEBUG
            if (drawState == null)
            {
                throw new ArgumentNullException(nameof(drawState), "DrawState is used for drawing. This can not be Null.");
            }
#endif
            // get the texture to draw
            Texture2D texture;
            var success = this._resourcesTexture.Read(drawState.CurrentTextureName, out texture);
            System.Diagnostics.Debug.Assert(success);

            // calculate what part to draw where
            var scaleVector = new Vector2(drawState.Width / texture.Width, drawState.Height / texture.Height);
            var position = new Vector2(drawState.DrawPosition.X + drawState.Offset.X, drawState.DrawPosition.Y + drawState.Offset.Y);
            var origin = new Vector2();
            //var color = new Color(tintColor.R, tintColor.G, tintColor.B, tintColor.A);
            var color = new Color(255,255,255,0);

            Rectangle srcRect;
            if (drawState.SourceRectangle == null)
            {
                srcRect = new Rectangle(0, 0, texture.Width, texture.Height);
            }
            else
            {
                srcRect = new Rectangle((int)drawState.SourceRectangle.PositionX, (int)drawState.SourceRectangle.PositionY, (int)drawState.SourceRectangle.Width, (int)drawState.SourceRectangle.Height);
            }

            this._spriteBatch.Draw(
                                texture,
                                position,
                                srcRect,
                                color,
                                0,
                                origin,
                                scaleVector,
                                SpriteEffects.None,
                                0);
        }

        /// <summary>
        /// Draws the string using specified location font and color.
        /// </summary>
        /// <param name="fontName">Name of the font.</param>
        /// <param name="text">The text to draw.</param>
        /// <param name="fontColor">Color of the font.</param>
        /// <param name="bitmap"></param>
        public override void DrawString(string fontName, string text, GuiColor fontColor, ref DVector2 position)
        {
            var castedPosition = new Vector2(position.X, position.Y);
            var origin = new Vector2(0, 0);

            SpriteFont spriteFont;
            var success = this._resourcesSpriteFont.Read(fontName, out spriteFont);
            System.Diagnostics.Debug.Assert(success);
            var color = new Color(fontColor.R, fontColor.G, fontColor.B, fontColor.A);

            this._spriteBatch.DrawString(
                                    spriteFont,
                                    text,
                                    castedPosition,
                                    color,
                                    0,
                                    origin,
                                    1,
                                    SpriteEffects.None,
                                    0);
        }

        /// <summary>
        /// Ends the drawing of textures. This is for the engine a important signal.
        /// </summary>
        public override void EndDraw()
        {
            this._spriteBatch.End();

            // Reset various things stuffed up by SpriteBatch
            this._device.DepthStencilState = new DepthStencilState { DepthBufferEnable = true, DepthBufferWriteEnable = true };
            //// device.BlendState = BlendState.Opaque;
            this._device.SamplerStates[0] = new SamplerState { AddressU = TextureAddressMode.Wrap, AddressV = TextureAddressMode.Wrap };
        }

        public override void Place(string name, DrawState drawState)
        {
        }

        /// <summary>
        /// Sets the render target to given target-name.
        /// </summary>
        /// <param name="renderTargetName">Name of the render target.</param>
        public override void SetRenderTarget(string renderTargetName)
        {
            RenderTarget2D renderTarget;
            var success = this._resourcesRenderTarget.Read(renderTargetName, out renderTarget);
            System.Diagnostics.Debug.Assert(success);
            this._device.SetRenderTarget(renderTarget);
        }

        /// <summary>
        /// Clears the current render target with given color.
        /// </summary>
        /// <param name="clearColor">Color of the clear.</param>
        public override void Clear(GuiColor clearColor)
        {
            var clr = new Color(clearColor.R, clearColor.G, clearColor.B, clearColor.A);
            this._device.Clear(ClearOptions.Target | ClearOptions.DepthBuffer, clr, 1, 0);
        }

        /// <summary>
        /// Unsets the render target.
        /// </summary>
        /// <param name="renderTargetName">Name of the render target.</param>
        public override void UnsetRenderTarget(string renderTargetName)
        {
            this._device.SetRenderTarget(null);
        }

        /// <summary>
        /// Debugs this instance. Making it spit out values of current state of this compositor.
        /// </summary>
        public override void Debug()
        {
            System.Diagnostics.Debug.WriteLine("*** ImageCompositor Debug ***");
            this._resourcesTexture.Debug();
            System.Diagnostics.Debug.WriteLine(string.Empty);
            this._resourcesRenderTarget.Debug();
            System.Diagnostics.Debug.WriteLine(string.Empty);
            this._resourcesSpriteFont.Debug();
            System.Diagnostics.Debug.WriteLine("*** ImageCompositor Debug End ***");
        }

        public override List<string> GetResourceList()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Determines whether a resource pool [contains] [the specified asset name].
        /// </summary>
        /// <param name="assetName">Name of the asset.</param>
        /// <returns>
        /// Whether we have the asset (True) or not (False).
        /// </returns>
        public override bool Contains(string assetName)
        {
            if (this._resourcesTexture.Contains(assetName))
            {
                return true;
            }

            if (this._resourcesRenderTarget.Contains(assetName))
            {
                return true;
            }

            if (this._resourcesSpriteFont.Contains(assetName))
            {
                return true;
            }

            return false;
        }

        public override object GetResource(string text)
        {
            throw new NotImplementedException();
        }

        public override DVector2 GetScreenSize()
        {
            var width = this._device.Viewport.Width;
            var heigth = this._device.Viewport.Height;
            return new DVector2(width, heigth);
        }

        /// <summary>Public implementation of Dispose pattern callable by consumers.</summary>
        public void Dispose()
        {
            this.Dispose(true);

            // ReSharper disable once GCSuppressFinalizeForTypeWithoutDestructor
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// Protected implementation of Dispose pattern.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (this._disposed)
            {
                return;
            }

            if (disposing)
            {
                //// Free any other managed objects here.

                this._spriteBatch.Dispose();
            }

            //// Free any unmanaged objects here.

            this._disposed = true;
        }
    }
}