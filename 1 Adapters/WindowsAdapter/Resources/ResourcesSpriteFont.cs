﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ResourcesSpriteFont.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Defines the ResourcesSpriteFont type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Gui4UFramework.Management;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace GUI4UWindows.Resources
{
    /// <summary>
    /// Of pool of resources containing Sprite-Fonts.
    /// </summary>
    public class ResourcesSpriteFont : ResourcePool<SpriteFont>
    {
        /// <summary>
        /// Contains the content manager.
        /// </summary>
        private readonly ContentManager _contentManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourcesSpriteFont"/> class.
        /// </summary>
        /// <param name="contentManager">The content manager.</param>
        public ResourcesSpriteFont(ContentManager contentManager)
        {
            this._contentManager = contentManager;
        }

        /// <summary>
        /// Creates font using the given name.
        /// </summary>
        /// <param name="fontName">Name of the font.</param>
        /// <param name="finalName"></param>
        /// <returns>The name of the location in the resource pool, where the created font resides.</returns>
        public bool CreateFromFontName(string fontName, out string finalName)
        {
            if (this.Contains(fontName))
            {
                finalName = fontName;
                return true;
            }

            var spriteFont = this._contentManager.Load<SpriteFont>(fontName);

            var success = this.Create(fontName, spriteFont, out finalName);

            System.Diagnostics.Debug.Assert(success);

            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            if (success == false) return false;

            return true;
        }
    }
}