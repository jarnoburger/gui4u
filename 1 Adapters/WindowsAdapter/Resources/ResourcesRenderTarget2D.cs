// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ResourcesRenderTarget2D.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Defines the ResourcesRenderTarget2D type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Diagnostics.CodeAnalysis;
using Gui4UFramework.Management;
using Microsoft.Xna.Framework.Graphics;

namespace GUI4UWindows.Resources
{
    /// <summary>Is a resource pool containing render-targets.</summary>
    public class ResourcesRenderTarget2D : ResourcePool<RenderTarget2D>
    {
        /// <summary>
        /// The graphics device.
        /// </summary>
        private readonly GraphicsDevice _device;

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourcesRenderTarget2D"/> class.
        /// </summary>
        /// <param name="device">The device.</param>
        public ResourcesRenderTarget2D(GraphicsDevice device)
        {
            this._device = device;
        }

        /// <summary>Creates the render target.</summary>
        /// <param name="preferredName">Name of the preferred.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <param name="mipMap">If the rendertarget will use MipMapping.</param>
        /// <param name="finalName"></param>
        /// <returns>The name of the location of the created render-target.</returns>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "mip", Justification = "MipMap is a normal function.")]
        public bool CreateRenderTarget(string preferredName, int width, int height, bool mipMap, out string finalName)
        {
            var renderTarget = new RenderTarget2D(
                                                this._device,
                                                width,
                                                height,
                                                mipMap,
                                                SurfaceFormat.Color,
                                                DepthFormat.Depth24);

            var success = this.Create(preferredName, renderTarget, out finalName);

            System.Diagnostics.Debug.Assert(success);

            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            if (success == false) return false;

            return true;
        }
    }
}