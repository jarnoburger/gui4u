// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ResourcesTexture.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Defines the ResourcesTexture type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using Gui4UFramework.Colors;
using Gui4UFramework.Management;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace GUI4UWindows.Resources
{
    /// <summary>
    /// The resource pool containing textures.
    /// </summary>
    public class ResourcesTexture : ResourcePool<Texture2D>
    {
        /// <summary>
        /// The graphics device.
        /// </summary>
        private readonly GraphicsDevice _device;

        /// <summary>
        /// The content manager.
        /// </summary>
        private readonly ContentManager _contentManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourcesTexture"/> class.
        /// </summary>
        /// <param name="device">The device.</param>
        /// <param name="contentManager">The content manager.</param>
        public ResourcesTexture(GraphicsDevice device, ContentManager contentManager)
        {
            this._device = device;
            this._contentManager = contentManager;
        }

        /// <summary>Creates the texture.</summary>
        /// <param name="name">The name of the texture.</param>
        /// <param name="width">The width of the texture.</param>
        /// <param name="height">The height of the texture.</param>
        /// <param name="color">The color of texture.</param>
        /// <param name="finalName"></param>
        /// <returns>The created texture.</returns>
        /// <exception cref="ArgumentNullException">Name is null.</exception>
        /// <exception cref="ArgumentOutOfRangeException">width can not be smaller then 1 and height can not be smaller then 1. </exception>
        public bool Create(string name, int width, int height, GuiColor color, out string finalName)
        {
#if DEBUG
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException(nameof(name));
            }

            if (width < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(width), "width can not be smaller then 1");
            }

            if (height < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(height), "height can not be smaller then 1");
            }
#endif

            var texture = new Texture2D(this._device, width, height, false, SurfaceFormat.Color);
            var count = width * height;
            var array = new GuiColor[count];

            for (var i = 0; i < array.Length; i++)
            {
                array[i] = color;
            }

            texture.SetData(array);

            var success = this.Create(name, texture, out finalName);

            System.Diagnostics.Debug.Assert(success);

            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            if (success == false) return false;
            texture.Name = finalName;

            return true;
        }

        /// <summary>Creates a image from given URL.</summary>
        /// <param name="preferredName">Name of the preferred.</param>
        /// <param name="imageLocation">The image location on disk.</param>
        /// <param name="finalName"></param>
        /// <returns>The location of the created resource.</returns>
        /// <exception cref="ArgumentNullException">PreferredName or imageLocation. </exception>
        public bool CreateFromUrl(string preferredName, string imageLocation, out string finalName)
        {
#if DEBUG
            if (string.IsNullOrEmpty(preferredName))
            {
                throw new ArgumentNullException(nameof(preferredName));
            }

            if (imageLocation == null)
            {
                throw new ArgumentNullException(nameof(imageLocation));
            }

            if (string.IsNullOrEmpty(imageLocation))
            {
                throw new ArgumentNullException(nameof(imageLocation));
            }
#endif

            var texture = this._contentManager.Load<Texture2D>(imageLocation);

            var success = this.Create(preferredName, texture, out finalName);

            System.Diagnostics.Debug.Assert(success);

            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            if (success == false)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Updates the item with given name with new color data.
        /// </summary>
        /// <param name="name">The name of the resource.</param>
        /// <param name="colorData">The colorData to set.</param>
        public bool Update(string name, GuiColor[] colorData)
        {
            Texture2D texture;
            var success = Read(name, out texture);
            if (success == false) return false;
            texture.SetData(colorData);
            return true;
        }
    }
}