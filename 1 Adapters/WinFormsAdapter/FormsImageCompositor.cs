﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FormsImageCompositor.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Defines the FormsImageCompositor type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Gui4UFramework.Colors;
using Gui4UFramework.Graphics;
using Gui4UFramework.Management;
using Gui4UFramework.Structural;
using WinFormAdapter.Resources;

namespace WinFormAdapter
{
    /// <summary>
    /// This is the image compositor. This draws the elements on the screen.
    /// </summary>
    public sealed class FormsImageCompositor : ImageCompositor, IDisposable
    {
        #region Declare
        /// <summary>
        /// Contains the resource pool with bitmaps.
        /// </summary>
        private readonly ResourcesBitmap _resourcesBitmap;

        /// <summary>
        /// The game that has main logic.
        /// </summary>
        private readonly Game _game;

        /// <summary>
        /// Contains the resource pool with fonts.
        /// </summary>
        private readonly ResourcesFont _resourcesFont;

        /// <summary>
        /// The brush that is used for drawing.
        /// </summary>
        private readonly SolidBrush _brush;

        /// <summary>The directory where all content resides.</summary>
        private readonly string _contentDirectory;

        /// <summary>
        /// Flag: Has Dispose already been called?.
        /// </summary>
        private bool _disposed;

        private bool _debugDraws = false;
        #endregion

        #region Initialize
        /// <summary>Initializes a new instance of the <see cref="FormsImageCompositor"/> class.</summary>
        /// <param name="game">The game to create this item.</param>
        /// <param name="contentDirectoryUrl">The content directory URL.</param>
        [SuppressMessage("Microsoft.Design", "CA1054:UriParametersShouldNotBeStrings", MessageId = "1#")]
        public FormsImageCompositor(Game game, string contentDirectoryUrl)
        {
            this._resourcesBitmap = new ResourcesBitmap();
            this._resourcesFont = new ResourcesFont();
            this._game = game;

            this._brush = new SolidBrush(Color.Orange);
            this._contentDirectory = contentDirectoryUrl;
        }
        #endregion

        #region Create
        /// <summary>
        /// Creates a rectangle texture.
        /// </summary>
        /// <param name="preferredName">The preferred name to use.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <param name="borderWidth">Width of the border.</param>
        /// <param name="fillColor">Color of the fill.</param>
        /// <param name="borderColor">Color of the border.</param>
        /// <param name="finalName"></param>
        /// <returns>
        /// The URL to the created texture resource that looks like a rectangle.
        /// </returns>
        public override bool CreateRectangleTexture(string preferredName, int width, int height, int borderWidth, GuiColor fillColor, GuiColor borderColor, out string finalName) 
        {
#if DEBUG
            if (width <= 0)
            {
                throw new ArgumentException(@"width can not be zero or less", nameof(width));
            }

            if (height <= 0)
            {
                throw new ArgumentException(@"height can not be zero or less", nameof(height));
            }

            if (string.IsNullOrEmpty(preferredName))
            {
                throw new ArgumentException(@"Control name can not be null", nameof(preferredName));
            }

            if (borderWidth < 0)
            {
                throw new ArgumentException(@"Border width can not be smaller then zero", nameof(borderWidth));
            }
#endif

            var success = this._resourcesBitmap.Create(preferredName, width, height, fillColor, out finalName);

            System.Diagnostics.Debug.Assert(success);

            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            if (success == false) return false;

            // var colordata = RectangleBrush.CreateRectangleColorMap(width, height,borderWidth,1,1, fillColor, borderColor);
            var colordata = ColorMapDrawer.CreateRectangleColorMap(width, height, borderWidth, fillColor, borderColor);

            this._resourcesBitmap.Update(finalName, colordata.ToArray());

            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            return success;
        }

        /// <summary>
        /// Creates a sprite font, to be used for text rendering.
        /// </summary>
        /// <param name="spriteFontName">Name of the sprite font in your Content.</param>
        /// <param name="finalName"></param>
        public override bool CreateSpriteFont(string spriteFontName, out string finalName)
        {
            if (string.IsNullOrEmpty(spriteFontName))
            {
                System.Diagnostics.Debug.WriteLine("The font name cannot be NULL or empty");
                Debugger.Break();
            }

            var success = this._resourcesFont.CreateFromFontName(spriteFontName, out finalName);

            System.Diagnostics.Debug.Assert(success);

            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            return success;
        }

        /// <summary>Creates the image texture.</summary>
        /// <param name="preferredName">Name of the base.</param>
        /// <param name="imageLocation">The file location for the image.</param>
        /// <param name="finalName"></param>
        /// <returns>A texture with a image inside.</returns>
        /// <exception cref="Exception">Image path is wrong.</exception>
        [SuppressMessage("Microsoft.Usage", "CA2201:DoNotRaiseReservedExceptionTypes")]
        public override bool CreateImageTexture(string preferredName, string imageLocation, out string finalName)
        {
#if DEBUG
            if (imageLocation == null)
            {
                throw new ArgumentNullException(nameof(imageLocation), @"The path to the file can not be Null.");
            }
#endif

            var files = Directory.GetFiles(this._contentDirectory, "*.*", SearchOption.AllDirectories);
            string name = string.Empty;
            foreach (var file in files)
            {
                System.Diagnostics.Debug.WriteLine(file);
                if (file.ToUpper().Contains(imageLocation.ToUpper()))
                {
                    name = file;
                }
            }

            if (string.IsNullOrEmpty(name))
            {
                throw new Exception($"Image path {imageLocation} is not point to a file.");
            }

            var success = this._resourcesBitmap.CreateFromUrl(preferredName, name, out finalName);

            System.Diagnostics.Debug.Assert(success);

            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            return success;
        }

        /// <summary>Creates a flat texture with one color.</summary>
        /// <param name="preferredName">The preferred name to use.</param>
        /// <param name="width">The width of the line texture.</param>
        /// <param name="height">The height of the line texture.</param>
        /// <param name="color">The fill color of the line texture.</param>
        /// <param name="finalName"></param>
        /// <returns>The name to use to find the texture back.</returns>
        public override bool CreateFlatTexture(string preferredName, int width, int height, GuiColor color, out string finalName)
        {
            var success = this._resourcesBitmap.Create(preferredName, width, height, color, out finalName);

            System.Diagnostics.Debug.Assert(success);

            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            return success;
        }

        /// <summary>Creates a render target to render onto.</summary>
        /// <param name="preferredName">Name of the base.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <param name="mipMap">If set to <c>true</c> [mip map].</param>
        /// <param name="finalName"></param>
        /// <returns>A render target.</returns>
        public override bool CreateRenderTarget(string preferredName, int width, int height, bool mipMap, out string finalName)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Read
        /// <summary>
        /// Reads the size string.
        /// </summary>
        /// <param name="spriteFontName">Name of the sprite font.</param>
        /// <param name="text">The text to use to measure the size.</param>
        /// <returns>The size of the text.</returns>
        public override DVector2 ReadSizeString(string spriteFontName, string text)
        {
            Font stringFont;
            var success = this._resourcesFont.Read(spriteFontName, out stringFont);
            System.Diagnostics.Debug.Assert(success);

            // Measure string.
            var size = TextRenderer.MeasureText(text, stringFont);

            ////var stringSize = _game.CurrentGraphics.MeasureString(text, stringFont);

            return new DVector2(size.Width, size.Height);
        }

        /// <summary>
        /// Reads the size texture.
        /// </summary>
        /// <param name="textureName">The name of the texture location.</param>
        /// <returns>The size of the texture.</returns>
        public override DVector2 ReadSizeTexture(string textureName)
        {
            Bitmap bitmap;
            var success = this._resourcesBitmap.Read(textureName, out bitmap);
            System.Diagnostics.Debug.Assert(success);
            return new DVector2(bitmap.Width, bitmap.Height);
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates the texture color image with give color image.
        /// </summary>
        /// <param name="textureName">Name of the texture.</param>
        /// <param name="colorMap">The color array.</param>
        public override void UpdateTexture(string textureName, ColorMap colorMap)
        {
#if DEBUG
            if (colorMap == null)
            {
                throw new ArgumentNullException(nameof(colorMap), @"ColorMap is used to update the texture, can not be null.");
            }
#endif

            this._resourcesBitmap.Update(textureName, colorMap.ToArray());
        }

        /// <summary>
        /// Updates the texture with given render-target.
        /// </summary>
        /// <param name="textureName">Name of the texture.</param>
        /// <param name="renderTargetName">Name of the render target.</param>
        public override void UpdateTexture(string textureName, string renderTargetName)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes the specified resource from the resource pool.
        /// </summary>
        /// <param name="resourceName">Name of the resource.</param>
        public override void Delete(string resourceName)
        {
            if (this._resourcesBitmap.Contains(resourceName))
            {
                this._resourcesBitmap.Delete(resourceName);
                return;
            }

            if (this._resourcesFont.Contains(resourceName))
            {
                this._resourcesFont.Delete(resourceName);
                return;
            }

            System.Diagnostics.Debug.WriteLine("Could not delete " + resourceName);
            Debugger.Break();
        }
        #endregion

        #region Draw
        /// <summary>
        /// Notes the beginning of drawing the textures. This is for the engine a important signal.
        /// </summary>
        public override void BeginDraw()
        {
        }

        /// <summary>
        /// Draws the specified texture using specified draw-state , with a tint color.
        /// </summary>
        /// <param name="drawState">The state that contains all the info to draw the item.</param>
        public override void Draw(DrawState drawState)
        {
            
        }

        public override void DrawString(string fontName, string text, GuiColor fontColor, ref DVector2 position)
        {
            // get the graphics device to draw 
            var g = this._game.CurrentGraphics;

            Font font;
            var success = this._resourcesFont.Read(fontName, out font);
            System.Diagnostics.Debug.Assert(success);


            this._brush.Color = Color.FromArgb(fontColor.A, fontColor.R, fontColor.G, fontColor.B);
            var point = new PointF(position.X, position.Y);

            if (_debugDraws)
            {
                System.Diagnostics.Debug.WriteLine($"Drawing string {fontName} {text} {position} {fontColor}");
            }

            g.DrawString(text, font, this._brush, point);
        }

        /// <summary>
        /// Ends the drawing of textures. This is for the engine a important signal.
        /// </summary>
        public override void EndDraw()
        {
        }

        /// <summary>
        /// Clears the current render target with given color.
        /// </summary>
        /// <param name="clearColor">Color of the clear.</param>
        public override void Clear(GuiColor clearColor)
        {
            throw new NotImplementedException();
        }

        public override void Place(string name, DrawState drawState)
        {
            //validate given data
            System.Diagnostics.Debug.Assert(drawState != null, @"The draw-state is used to draw , it can not be null.");

            // get the texture to draw
            Bitmap texture;
            var success = this._resourcesBitmap.Read(name, out texture);
            System.Diagnostics.Debug.Assert(success);

            // create the source-rectangle
            RectangleF srcRect;
            if (drawState.SourceRectangle == null)
            {
                srcRect = new RectangleF(
                    0,
                    0,
                    texture.Width,
                    texture.Height);
            }
            else
            {
                srcRect = new RectangleF(
                    (int)drawState.SourceRectangle.PositionX,
                    (int)drawState.SourceRectangle.PositionY,
                    (int)drawState.SourceRectangle.Width,
                    (int)drawState.SourceRectangle.Height);
            }

            // create the target-rectangle
            // var scaleVector = new DVector2(drawState.Width / texture.Width, drawState.Height / texture.Height);
            var position = new Point((int)(drawState.DrawPosition.X + drawState.Offset.X), (int)(drawState.DrawPosition.Y + drawState.Offset.Y));
            var destRect = new RectangleF(position.X, position.Y, drawState.Width, drawState.Height);

            if (_debugDraws)
            {
                System.Diagnostics.Debug.WriteLine($"Drawing image {name} {drawState} {srcRect} {destRect}");
            }

            this._game.CurrentGraphics.DrawImage(texture, destRect, srcRect, GraphicsUnit.Pixel);
        }
        #endregion

        #region Rendertarget
        /// <summary>
        /// Sets the render target to given target-name.
        /// </summary>
        /// <param name="renderTargetName">Name of the render target.</param>
        public override void SetRenderTarget(string renderTargetName)
        {
            throw new NotImplementedException();
        }

        /// <summary>Unsets the render target.</summary>
        /// <param name="renderTargetName">Name of the render target.</param>
        public override void UnsetRenderTarget(string renderTargetName)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Debug
        /// <summary>
        /// Debugs this instance. Making it spit out values of current state of this compositor.
        /// </summary>
        public override void Debug()
        {
            throw new NotImplementedException();
        }

        public override List<string> GetResourceList()
        {
            // make value containers
            var list = new List<string>();
            List<string> retrievedList;

            // retrieve a list of names from the bitmap resources and at those names to mine
            retrievedList = this._resourcesBitmap.GetResourceList();
            foreach (var bitmapResource in retrievedList)
            {
                list.Add("BMP : " + bitmapResource);   
            }

            // retrieve a list of names from the bitmap resources and at those names to mine
            retrievedList = this._resourcesFont.GetResourceList();
            foreach (var resourceFont in retrievedList)
            {
                list.Add("FNT : " + resourceFont);
            }

            return list;
        }

        #endregion

        /// <summary>
        /// Determines whether a resource pool [contains] [the specified asset name].
        /// </summary>
        /// <param name="assetName">Name of the asset.</param>
        /// <returns>
        /// Whether we have the asset (True) or not (False).
        /// </returns>
        public override bool Contains(string assetName)
        {
            if (this._resourcesBitmap.Contains(assetName))
            {
                return true;
            }

            if (this._resourcesFont.Contains(assetName))
            {
                return true;
            }

            return false;
        }

        public override object GetResource(string text)
        {
            object obj;

            var firstPart = text.Substring(0, 6);
            var secondPart = text.Substring(6, text.Length - 6);

            bool success = false;

            if (firstPart.Equals("BMP : "))
            {
                Bitmap bmp;
                success = _resourcesBitmap.Read(secondPart, out bmp);

                if (success == false)
                {
                    return null;
                }
                else
                {
                    return bmp;
                }
            }
            else if (firstPart.Equals("FNT : "))
            {
                Font fnt;
                success = _resourcesFont.Read(secondPart, out fnt);

                if (success == false)
                {
                    return null;
                }
                else
                {
                    return fnt;
                }
            }

            return null;
        }

        public override DVector2 GetScreenSize()
        {
            var width = this._game.Width;
            var heigth = this._game.Height;
            return new DVector2(width, heigth);
        }

        #region Dispose
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// Public implementation of Dispose pattern callable by consumers.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            // ReSharper disable once GCSuppressFinalizeForTypeWithoutDestructor
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// Protected implementation of Dispose pattern.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (this._disposed)
            {
                return;
            }

            if (disposing)
            {
                //// Free any other managed objects here.
                this._brush.Dispose();
            }

            //// Free any unmanaged objects here.
            this._disposed = true;
        }
        #endregion
    }
}