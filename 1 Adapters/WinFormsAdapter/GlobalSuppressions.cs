// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GlobalSuppressions.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   GlobalSuppressions.cs
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "WinFormAdapter")]
[assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "WinFormAdapter.Resources")]
[assembly: SuppressMessage("Microsoft.Design", "CA1055:UriReturnValuesShouldNotBeStrings", Scope = "member", Target = "WinFormAdapter.Resources.ResourcesBitmap.#CreateFromUrl(System.String,System.String)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "color", Scope = "member", Target = "WinFormAdapter.FormsImageCompositor.#Draw(System.String,GUI4UFramework.Structural.DrawState,GUI4UFramework.Colors.GuiColor)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "srcUnit", Scope = "member", Target = "WinFormAdapter.FormsImageCompositor.#Draw(System.String,GUI4UFramework.Structural.DrawState,GUI4UFramework.Colors.GuiColor)")]