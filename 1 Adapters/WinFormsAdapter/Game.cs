﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Game.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Defines the Game type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using Gui4UFramework.Management;

namespace WinFormAdapter
{
    /// <summary>
    /// This is the form that behaves like a game.
    /// </summary>
    public class Game : Form
    {
        #region Declare
        /// <summary>
        /// The timer that make the engine Tick.
        /// </summary>
        private readonly Timer _gameloopTimer;

        private readonly FrameCounter _frameCounter;

        /// <summary>
        /// The current time used.
        /// </summary>
        private readonly GameTime _gameTime;
        /// <summary>To measure currentTime.</summary>
        private readonly Stopwatch _gameTimeStopWatch;

        /// <summary>The draw interval (how many times we draw per second).</summary>
        private const float DrawInterval = 1000f / 50f;
        /// <summary> The last time that a new image was drawn. </summary>
        private double _lastTimeDrawn;

        /// <summary>The update interval (how many times we update per second).</summary>
        private const float UpdateInterval = 1000f / 50f;
        /// <summary>The last time that the game logic updated.</summary>
        private double _lastTimeUpdated;

        private const float FPSTimerInterval = 1000f/1f;
        private double _lastTimeFpsUpdated;

        private bool _debugDraw = false;
        #endregion

        #region Initialize
        /// <summary>
        /// Initializes a new instance of the <see cref="Game"/> class.
        /// </summary>
        protected Game()
        {
            // this will be used to know what the current time is
            this._gameTime = new GameTime();
            this._gameTimeStopWatch = new Stopwatch();
            this._gameTimeStopWatch.Start();

            // this will be used to count fps
            this._frameCounter = new FrameCounter();

            // this will be used to read out the user inputs
            this.InputManager = new WindowsInputManager(this);
            
            // set form inner workings
            this.DoubleBuffered = true;

            // initialize timer that continiuously invalidates the form
            this._gameloopTimer = new Timer { Interval = 5 };
            this._gameloopTimer.Tick += this.OnGameloopTimerTick;
            this._gameloopTimer.Start();
        }
        #endregion

        #region Properties

        #endregion

        #region Timed Actions
        /// <summary>
        /// Called when the timer ticks.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="eventArgs">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OnGameloopTimerTick(object sender, EventArgs eventArgs)
        {
            // load when needed
            if (this.Loaded == false)
            {
                this.Initialize();
                this.LoadContent();
                
                this.Loaded = true;
            }

            // get the new time
            this._gameTime.Update(new TimeSpan(this._gameTimeStopWatch.ElapsedTicks));
            var currentMilliseconds = this._gameTime.TotalGameTime.TotalMilliseconds;

            // update the fps
            UpdatePerInterval(currentMilliseconds);
            DrawPerInterval(currentMilliseconds);
            FPSPerInterval(currentMilliseconds);
        }

        private void UpdatePerInterval(double currentMilliSeconds)
        {
            if (currentMilliSeconds - this._lastTimeUpdated > UpdateInterval)
            {
                this.Update(this._gameTime);

                // remember last that we updated
                this._lastTimeUpdated = currentMilliSeconds;
            }
        }

        private void DrawPerInterval(double currentMilliSeconds)
        {
            if (currentMilliSeconds - this._lastTimeDrawn > DrawInterval)
            {
                this.Invalidate();

                // remember last time that we drawn
                this._lastTimeDrawn = currentMilliSeconds;
            }
        }

        private void FPSPerInterval(double currentMilliSeconds)
        {
            if (currentMilliSeconds - this._lastTimeFpsUpdated > FPSTimerInterval)
            {
                //this.Text = $"Updates : {this._updateCount} Draws : {this._drawCount}";
                this.Text =$"FPS {_frameCounter.AverageFramesPerSecond}";

                // remember last that we updated
                this._lastTimeFpsUpdated = currentMilliSeconds;
            }
        }
        #endregion

        #region OnPaint
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            if (Loaded == false) return;

            // do draw stuff
            this.CurrentGraphics = e.Graphics;
            this.CurrentGraphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.Low; // or NearestNeighbour
            this.CurrentGraphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            this.CurrentGraphics.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.None;
            this.CurrentGraphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighSpeed;
            this.CurrentGraphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;

            // remember that we drawn stuff
            _frameCounter.Update(_gameTime.TotalGameTime.TotalMilliseconds / 1000f);

            if (_debugDraw)
            {
                Debug.WriteLine("*** Starting draw *** " + _gameTime.TotalGameTime.TotalMilliseconds);
            }

            this.Draw(this._gameTime);

            if (_debugDraw)
            {
                Debug.WriteLine("*** Ending draw ***");
            }
        }
        #endregion

        #region Properties for the implementor
        /// <summary>
        /// Gets or sets the current graphics device , that draws the stuff on the bitmap.
        /// </summary>
        /// <value>
        /// The current graphics.
        /// </value>
        public Graphics CurrentGraphics
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the current time used.
        /// </summary>
        /// <value>
        /// The current time.
        /// </value>
        public GameTime GameTime
        {
            get
            {
                return this._gameTime;
            }
        }

        /// <summary>
        /// Gets or sets the input manager, that reads the hardware inputs.
        /// </summary>
        /// <value>
        /// The input manager.
        /// </value>
        public WindowsInputManager InputManager
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the mouse is visible.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is mouse visible; otherwise, <c>false</c>.
        /// </value>
        public bool IsMouseVisible
        {
            get;
            set;
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="Game"/> is loaded.
        /// </summary>
        /// <value>
        ///   <c>true</c> if loaded; otherwise, <c>false</c>.
        /// </value>
        public bool Loaded
        {
            get;
            private set;
        }
        #endregion

        #region DO 3D
        /// <summary>
        /// Initializes this instance.
        /// </summary>
        protected virtual void Initialize()
        {
        }

        /// <summary>
        /// Loads the content for this instance.
        /// </summary>
        protected virtual void LoadContent()
        {
        }

        /// <summary>
        /// Unloads the content for this instance.
        /// </summary>
        protected virtual void UnloadContent()
        {
        }

        /// <summary>
        /// Updates the content for this instance.
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        // ReSharper disable once UnusedParameter.Local
        protected virtual void Update(GameTime gameTime)
        {
        }

        /// <summary>
        /// Draws the specified game time.
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        // ReSharper disable once UnusedParameter.Local
        protected virtual void Draw(GameTime gameTime)
        {
        }
        #endregion
    }
}