﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WindowsInputManager.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Defines the WindowsInputManager type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows.Forms;
using Gui4UFramework.Graphics;
using Gui4UFramework.Management;

namespace WinFormAdapter
{
    /// <summary>
    /// The input manager.
    /// </summary>
    public class WindowsInputManager : InputManager
    {
        /// <summary>
        /// The window where we track for input.
        /// </summary>
        private readonly Form _window;

        /// <summary>
        /// Initializes a new instance of the <see cref="WindowsInputManager"/> class.
        /// </summary>
        /// <param name="window">The window.</param>
        public WindowsInputManager(Form window)
        {
            this._window = window;
        }

        /// <summary>
        /// Reads the mouse location relative to the desktop coordinates.
        /// </summary>
        /// <returns>
        /// The mouse location contained in a DVector2.
        /// </returns>
        public override DVector2 ReadMouseLocation()
        {
            var loc = this._window.PointToClient(Cursor.Position);
            var vector = new DVector2(loc.X, loc.Y);

            //// System.Diagnostics.Debug.WriteLine("Mouse location is " + vector);
            return vector;
        }

        /// <summary>
        /// Reads if the left mouse is pressed.
        /// </summary>
        /// <returns>
        /// True if pressed, otherwise false.
        /// </returns>
        public override bool ReadLeftMousePressed()
        {
            if (this._window.Focused == false) return false;

            var buttons = Control.MouseButtons;

            if (buttons == MouseButtons.Left)
            {
                Debug.WriteLine("Mouse is pressed !");
                return true;
            }

            return false;
        }

        /// <summary>
        /// Reads if left mouse is released.
        /// </summary>
        /// <returns>
        /// True if released, otherwise false.
        /// </returns>
        public override bool ReadLeftMouseReleased()
        {
            var buttons = Control.MouseButtons;
            if (buttons != MouseButtons.Left)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Is call that the keyboard state must be read again towards the KeySwitchState.
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        public override void UpdateKeyboardInput(GameTime gameTime)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Reads the state of the keyboard, contained in a KeySwitches class.
        /// </summary>
        /// <returns>
        /// The current key switches.
        /// </returns>
        public override KeySwitches ReadKeySwitchState()
        {
            return new KeySwitches();
        }
    }
}