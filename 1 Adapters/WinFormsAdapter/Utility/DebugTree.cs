﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Gui4UFramework.Management;
using Gui4UFramework.Structural;

namespace WinFormAdapter.Utility
{
    public partial class DebugTree : Form
    {
        #region Declare
        private readonly Timer _timer;

        private static TreeNode _root;

        private ImageCompositor _compositor;
        #endregion

        #region Initialize
        public DebugTree()
        {
            InitializeComponent();

            this._timer = new Timer
            {
                Interval = 500
            };
            this._timer.Tick += OnTimerTick;
            this._timer.Start();

            //this.TopMost = true;
        }

        private void OnTimerTick(object sender, EventArgs e)
        {
            Scan();
        }

        #endregion

        #region Properties
        public Node Node { get; set; }
        #endregion

        public static DebugTree Show(Node node, ImageCompositor compositor)
        {
            var form = new DebugTree
            {
                Node = node,
                _compositor = compositor
            };

            form.Show();

            _root = form.treeView1.Nodes.Add("Root", "Root");

            _root.ExpandAll();

            return form;
        }

        public void Scan()
        {
            // update the tree-view
            this.treeView1.BeginUpdate();
            
            if (Node == null) return;

            this.Scan(Node, _root);

            this.treeView1.EndUpdate();

            // force a update to the property grid
            if (this.propertyGrid1.SelectedObject != null)
            {
                this.propertyGrid1.SelectedObject = this.propertyGrid1.SelectedObject;
            }
        }

        public void Scan(Node parentGameNode, TreeNode parentTreeNode)
        {
            // if to many gameNode-children , then add gameNode-children to tree-view
            foreach (var gameNodeChild in parentGameNode.Children)
            {
                // find it
                var contains = false;
                foreach (TreeNode treeNodeChild in parentTreeNode.Nodes)
                {
                    if (!treeNodeChild.Name.Equals(gameNodeChild.Name))
                    {
                        continue;
                    }

                    contains = true;
                    break;
                }

                // if not presented, then add it
                if (contains != true)
                {
                    var newTreeNode = new TreeNode()
                    {
                        Tag = gameNodeChild,
                        Name = gameNodeChild.Name,
                        Text = CreateText(gameNodeChild),
                    };
                    parentTreeNode.Nodes.Add(newTreeNode);
                    newTreeNode.Expand();
                }
            }

            // if too many gameNode-children , then remove them
            for (int index = parentTreeNode.Nodes.Count -1; index >= 0; index--)
            {
                TreeNode node = parentTreeNode.Nodes[index];

                // find it
                var contains = false;
                foreach (Node gameNodeChild in parentGameNode.Children)
                {
                    if (gameNodeChild.Name.Equals(node.Name))
                    {
                        contains = true;
                        break;
                    }
                }

                if (contains == false)
                {
                    parentTreeNode.Nodes.RemoveAt(index);
                }
            }

            // scan children
            for (int index = 0; index < parentTreeNode.Nodes.Count; index++)
            {
                Node gameNodeChild = parentGameNode.Children[index];
                TreeNode treeNodeChild = parentTreeNode.Nodes[index];

                Scan(gameNodeChild, treeNodeChild);
            }
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            var tag = e.Node.Tag;
            var node = tag as Node;
            this.propertyGrid1.SelectedObject = node;

            var control = node as Gui4UFramework.Structural.Control;
            if (control != null)
            {
                var textureName = "BMP : " + control.State.CurrentTextureName;

                var resource = _compositor.GetResource(textureName);

                // force a update to the picture-box
                var bmp = resource as Bitmap;
                if (bmp != null)
                {
                    this.pictureBox1.BackgroundImage = bmp;
                }
                else
                {
                    this.pictureBox1.BackgroundImage = null;
                }
            }
        }

        private string CreateText(Node node)
        {
            var type = node.GetType().Name;
            var name = node.Name;

            return type + " : '" + name + "'";
        }
    }
}
