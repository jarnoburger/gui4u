﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Gui4UFramework.Management;

namespace WinFormAdapter.Utility
{
    public partial class DebugResources : Form
    {
        #region Declare
        private readonly Timer _timer;

        private ImageCompositor _compositor;
        #endregion

        #region Initialize
        public DebugResources()
        {
            InitializeComponent();

            this._timer = new Timer
            {
                Interval = 500
            };
            this._timer.Tick += OnTimerTick;
            this._timer.Start();
        }

        public static DebugResources Show(ImageCompositor compositor)
        {
            var debug = new DebugResources
            {
                Compositor = compositor
            };

            debug.Show();
            return debug;
        }
        #endregion

        #region Properties
        public ImageCompositor Compositor
        {
            get { return this._compositor; }
            set { this._compositor = value; }
        }
        #endregion

        private void OnTimerTick(object sender, EventArgs e)
        {
            Scan();
        }

        public void Scan()
        {
            // get the stuff that we will sync
            var resources = this._compositor.GetResourceList();
            var items = this.listView1.Items;

            // check if the resource has a item representing it, otherwise add it
            foreach (var resource in resources)
            {
                // check if we already show it
                var contains = false;
                foreach (ListViewItem item in items)
                {
                    if (!item.Text.Equals(resource)) continue;

                    contains = true;
                    break;
                }

                // if not > add it
                if (contains == false)
                {
                    items.Add(resource);
                }
            }

            // check if each item still has a resource behind it, otherwise remove it
            for (int index = items.Count-1; index >=0 ; index--)
            {
                ListViewItem item = items[index];

                // check if item has a resource 
                var contains = false;
                foreach (var resource in resources)
                {
                    if (item.Text.Equals(resource))
                    {
                        contains = true;
                        break;
                    }
                }

                // if we don't have it. remove it
                if (contains == false)
                {
                    items.RemoveAt(index);
                }
            }

            // force a update to the property grid
            if (this.propertyGrid1.SelectedObject != null)
            {
                this.propertyGrid1.SelectedObject = this.propertyGrid1.SelectedObject;
            }

            // force a update to the picture-box
            var bmp = this.propertyGrid1.SelectedObject as Bitmap;
            if (bmp != null)
            {
                this.pictureBox1.BackgroundImage = bmp;
            }
            else
            {
                this.pictureBox1.BackgroundImage = null;
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            // validate
            if (this.listView1.SelectedItems.Count <= 0) return;

            // get the selected item
            var selectedItem = this.listView1.SelectedItems[0];

            // get the resource behind the item
            var resource = this._compositor.GetResource(selectedItem.Text);

            // force a update to the property grid
            this.propertyGrid1.SelectedObject = resource;

            // force a update to the picture-box
            var bmp = this.propertyGrid1.SelectedObject as Bitmap;
            if (bmp != null)
            {
                this.pictureBox1.BackgroundImage = bmp;
            }
            else
            {
                this.pictureBox1.BackgroundImage = null;
            }
        }
    }
}
