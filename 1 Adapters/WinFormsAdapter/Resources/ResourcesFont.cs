﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ResourcesFont.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Defines the ResourcesFont type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Drawing;
using Gui4UFramework.Management;

namespace WinFormAdapter.Resources
{
    /// <summary>
    /// The resource pool containing the fonts.
    /// </summary>
    public class ResourcesFont : ResourcePool<Font>
    {
        /// <summary>
        /// Creates a font using specifed font name.
        /// </summary>
        /// <param name="fontName">Name of the font.</param>
        /// <param name="finalName"></param>
        /// <returns>The resource location name , that is the same as the font name.</returns>
        public bool CreateFromFontName(string fontName, out string finalName)
        {
            if (this.Contains(fontName))
            {
                finalName = fontName;
                return true;
            }

            var font = new Font(fontName, 10);
            var succcess = this.Create(fontName, font, out finalName);
            if (succcess == false) return false;
            return true;
        }
    }
}