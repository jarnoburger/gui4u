﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ResourcesBitmap.cs" company="Jarno Burger">
//   See copyright.txt in the root of this project.
// </copyright>
// <summary>
//   Defines the ResourcesBitmap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using Gui4UFramework.Colors;
using Gui4UFramework.Management;

namespace WinFormAdapter.Resources
{
    /// <summary>
    /// The resource pool containing the bitmaps.
    /// </summary>
    public class ResourcesBitmap : ResourcePool<Bitmap>
    {
        /// <summary>
        /// Creates a bitmap with the specified name and size.
        /// </summary>
        /// <param name="preferredName">The preferred name.</param>
        /// <param name="width">The width of the image.</param>
        /// <param name="height">The height of the image.</param>
        /// <param name="color">The color of the image.</param>
        /// <param name="finalName"></param>
        /// <returns>The resource location name with the created bitmap image.</returns>
        public bool Create(string preferredName, int width, int height, GuiColor color, out string finalName)
        {
            // When creating Bitmap objects, use PixelFormat.Format32bppPArgb. This provides considerable speed increases - something to do with the internal format used by the Bitmap object. This is extremely simple to implement.
            var bitmap = new Bitmap(width, height);
            var g = Graphics.FromImage(bitmap);
            g.Clear(Color.FromArgb(color.R, color.G, color.B));
            g.Dispose();

            var success = this.Create(preferredName, bitmap, out finalName);

            System.Diagnostics.Debug.Assert(success);

            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            if (success == false) return false;
            
            return true;
        }

        /// <summary>
        /// Creates a image from specified URL and places them in the bitmap resources.
        /// </summary>
        /// <param name="preferredName">Name of the preferred.</param>
        /// <param name="imageUrl">The image URL.</param>
        /// <param name="finalName"></param>
        /// <returns>The location where the resource can be found.</returns>
        [SuppressMessage("Microsoft.Design", "CA1054:UriParametersShouldNotBeStrings", MessageId = "1#")]
        public bool CreateFromUrl(string preferredName, string imageUrl, out string finalName)
        {
#if DEBUG
            if (imageUrl == null)
            {
                throw new ArgumentNullException(nameof(imageUrl), @"The path can not be null , if we need to use it to make a bitmap");
            }

            if (File.Exists(imageUrl) == false)
            {
                throw new ArgumentNullException(nameof(imageUrl), @"The path does not point to a file, we need it to point to a file.");
            }
#endif

            var bitmap = new Bitmap(imageUrl);

            var success = this.Create(preferredName, bitmap, out finalName);

            System.Diagnostics.Debug.Assert(success);

            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            if (success == false) return false;

            return true;
        }

        /// <summary>
        /// Updates the image at specified named location.
        /// </summary>
        /// <param name="name">The name for the location in the pool.</param>
        /// <param name="colorData">The color data to update the image.</param>
        public void Update(string name, GuiColor[] colorData)
        {
#if DEBUG
            if (colorData == null)
            {
                throw new ArgumentNullException(nameof(colorData), @"Colordata can not be null, cause we need to use it to update the resource.");
            }
#endif

            // first reverse the data
            var reverseData = new GuiColor[colorData.Length];
            var r = colorData.Length - 1;
            foreach (var guiColor in colorData)
            {
                reverseData[r] = guiColor;
                r--;
            }

            Bitmap bitmap;
            var success = this.Read(name, out bitmap);
            System.Diagnostics.Debug.Assert(success);

            var width = bitmap.Width;
            var height = bitmap.Height;
            var pixelcount = width * height;
            var rectangle = new Rectangle(0, 0, width, height);
            var format = bitmap.PixelFormat;

            var bitmapData = bitmap.LockBits(rectangle, ImageLockMode.ReadWrite, format);
            var pointer = bitmapData.Scan0;

            var pixels = new byte[pixelcount * 4]; // times 4 because R,G,B,A
            var i = 0;
            foreach (var guiColor in colorData)
            {
                pixels[i] = guiColor.B;
                i++;
                pixels[i] = guiColor.G;
                i++;
                pixels[i] = guiColor.R;
                i++;
                pixels[i] = guiColor.A;
                i++;
            }

            //// Copy data from byte array to pointer
            Marshal.Copy(pixels, 0, pointer, pixels.Length);

            // Unlock bitmap data
            bitmap.UnlockBits(bitmapData);
        }
    }
}